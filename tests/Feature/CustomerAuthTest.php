<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Libraries\Site;
use App\Models\Company;
use App\Models\Customer;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Password;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CustomerAuthTest extends TestCase
{
    use RefreshDatabase;    
    protected $company;
    protected $guard = 'customer';

    public function setUp(): void
    {
        parent::setUp();
        $this->company = factory(Company::class)->create();
        Site::setupCompany($this->company);
    }

    public function testRedirectionToLoginPage()
    {
        $this
            ->get('/')
            ->assertStatus(302)
            ->assertRedirect('/auth/login');
    }

    public function testCustomerCanSeeLoginForm()
    {
        $this
            ->get('/auth/login')
            ->assertSuccessful()
            ->assertViewIs('customer.auth.login');
    }

    public function testEnabledCustomerCanLogin()
    {
        $customer = factory(Customer::class)->create(['company_id' => $this->company->id, 'login_enabled' => 1,]);
        $this
            ->followingRedirects()
            ->post('/auth/login', [
                'email' => $customer->email,
                'password' => 'password',
            ])
            ->assertSuccessful()
            ->assertSessionMissing('errors');

        $this->assertAuthenticatedAs($customer, $this->guard);
    }

    public function testCustomerCanLogout()
    {
        $customer = factory(Customer::class)->create(['company_id' => $this->company->id, 'login_enabled' => 1,]);
        $this
            ->followingRedirects()
            ->actingAs($customer, $this->guard)
            ->post('/auth/logout')
            ->assertSuccessful();

        $this->assertGuest($this->guard);
    }

    public function testLogedInCustomerCantSeeLoginPage()
    {
        $customer = factory(Customer::class)->create(['company_id' => $this->company->id, 'login_enabled' => 1,]);
        $this
            ->actingAs($customer, $this->guard)
            ->get('/auth/login')
            ->assertRedirect('/');
    }

    public function testDisabledCustomerCantLogin()
    {
        $customer = factory(Customer::class)->create(['company_id' => $this->company->id]);
        $this
            ->post('/auth/login', [
                'email' => $customer->email,
                'password' => 'password',
            ])
            ->assertSessionHasErrors();
    }

    public function testCustomerCantLoginOnAdminSide()
    {
        $customer = factory(Customer::class)->create(['company_id' => $this->company->id]);
        $this
            ->post('/admin/auth/login', [
                'email' => $customer->email,
                'password' => 'password',
            ])
            ->assertSessionHasErrors();
    }

    public function testCustomerCanSeeResetEmailForm()
    {
        $this
            ->get('auth/password/reset')
            ->assertSuccessful()
            ->assertViewIs('customer.auth.email');
    }

    public function testCustomerCanRequestResetPassword()
    {
        $customer = factory(Customer::class)->create(['company_id' => $this->company->id, 'login_enabled' => 1,]);
        $this
            ->followingRedirects()
            ->post('auth/password/reset', [
                'email' => $customer->email
            ])
            ->assertSuccessful();
    }

    public function testCustomerCanSeeResetPasswordForm()
    {
        $customer = factory(Customer::class)->create(['company_id' => $this->company->id, 'login_enabled' => 1,]);
        $token = Password::broker('customers')->createToken($customer);
        $this
            ->get('auth/password/reset/' . $token)
            ->assertSuccessful()
            ->assertViewIs('customer.auth.reset')
            ->assertSee('Reset your password.')
            ->assertSee('Email')
            ->assertSee('Password')
            ->assertSee('Confirm Password');
    }

    public function testCustomerCanResetPassword()
    {
        $customer = factory(Customer::class)->create(['company_id' => $this->company->id, 'login_enabled' => 1,]);
        $token = Password::broker('customers')->createToken($customer);
        $newPassword = 'hello@112312';
        $this
            ->followingRedirects()
            ->post('auth/password/reset/', [
                'token' => $token,
                'email' => $customer->email,
                'password' => $newPassword,
                'password_confirmation' => $newPassword,
            ])
            ->assertSuccessful();
        $customer->refresh();
        $this->assertTrue(Hash::check($newPassword, $customer->password));
    }
}
