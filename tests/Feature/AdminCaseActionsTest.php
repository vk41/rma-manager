<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Models\User;
use App\Libraries\Site;
use App\Models\Company;
use App\Models\RmaCase;
use App\Models\Customer;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class AdminCaseActionsTest extends TestCase
{
    use RefreshDatabase;
    protected $company;

    public function setUp(): void
    {
        parent::setUp();
        $this->company = factory(Company::class)->create();
        Site::setupCompany($this->company);
    }

    public function testAdminCanSeeAcceptRejectButton()
    {
        $customer = factory(Customer::class)->create(['company_id' => $this->company->id, 'login_enabled' => 1,]);
        $case = factory(RmaCase::class)->create(['customer_id' => $customer->id, 'status' => 'pending']);
        $user = factory(User::class)->create(['enabled' => 1,]);
        $this
            ->actingAs($user)
            ->get(route('admin.case.show', ['case_number' => $case->case_number]))
            ->assertSuccessful()
            ->assertViewIs('admin.case.show')
            ->assertSee('pending')
            ->assertSee('Accept / Reject');
    }

    public function testAdminCanSeeAcceptAndRejectPage()
    {
        $customer = factory(Customer::class)->create(['company_id' => $this->company->id, 'login_enabled' => 1,]);
        $case = factory(RmaCase::class)->create(['customer_id' => $customer->id, 'status' => 'pending']);
        $user = factory(User::class)->create(['enabled' => 1,]);
        $this
            ->actingAs($user)
            ->get(route('admin.case.action-show', ['case_number' => $case->case_number, 'type' => 'accept']))
            ->assertSuccessful()
            ->assertViewIs('admin.case.accept');
    }

    public function testAdminCanDoAccept()
    {
        $customer = factory(Customer::class)->create(['company_id' => $this->company->id, 'login_enabled' => 1,]);
        $case = factory(RmaCase::class)->create(['customer_id' => $customer->id, 'status' => 'pending']);
        $user = factory(User::class)->create(['enabled' => 1,]);
        $caseData = [
            'status' => 'accepted',
            'reason' => null,
            'serial_number' => 'asdfasd',
            'part_number' => '4k7p9v5v6u',
            'manufacturer' => '2w5t9m5q2s',
            'category' => '4j1g0a2o6w',
            'product_description' => '0u2q1p8e6b',
            'rma_number' => 'AQ645JWP1004-9247',
            'invoice_number' => '1j8r3i6p4p',
            'invoice_date' => '2013-06-21',
            'remarks' => null,
        ];
        $this
            ->followingRedirects()
            ->actingAs($user)
            ->post(route('admin.case.accept', ['case_number' => $case->case_number]), $caseData)
            ->assertSuccessful();
        $this->assertDatabaseHas('rma_cases', ['status' => 'accepted', 'rma_number' => 'AQ645JWP1004-9247', 'case_number' => $case->case_number]);
    }

    public function testAdminCanSeeCollectPage()
    {
        $customer = factory(Customer::class)->create(['company_id' => $this->company->id, 'login_enabled' => 1,]);
        $case = factory(RmaCase::class)->create(['customer_id' => $customer->id, 'status' => 'accepted', 'product_received_on' => null]);
        $user = factory(User::class)->create(['enabled' => 1,]);
        $this
            ->actingAs($user)
            ->get(route('admin.case.action-show', ['case_number' => $case->case_number, 'type' => 'accept']))
            ->assertSuccessful()
            ->assertViewIs('admin.case.accept');
    }

    public function testAdminCanCollectItem()
    {
        $customer = factory(Customer::class)->create(['company_id' => $this->company->id, 'login_enabled' => 1,]);
        $case = factory(RmaCase::class)->create(['customer_id' => $customer->id, 'status' => 'accepted', 'product_received_on' => null]);
        $user = factory(User::class)->create(['enabled' => 1,]);
        $caseData = [
            'status' => 'accepted',
            'reason' => null,
            'serial_number' => 'asdfasd',
            'part_number' => '4k7p9v5v6u',
            'manufacturer' => '2w5t9m5q2s',
            'category' => '4j1g0a2o6w',
            'product_description' => '0u2q1p8e6b',
            'rma_number' => 'AQ645JWP1004-9247',
            'invoice_number' => '1j8r3i6p4p',
            'invoice_date' => '2013-06-21',
            'remarks' => null,
        ];
        $this
            ->followingRedirects()
            ->actingAs($user)
            ->post(route('admin.case.accept', ['case_number' => $case->case_number]), $caseData)
            ->assertSuccessful();
        $this->assertDatabaseHas('rma_cases', ['status' => 'accepted', 'rma_number' => 'AQ645JWP1004-9247', 'case_number' => $case->case_number]);
    }
}
