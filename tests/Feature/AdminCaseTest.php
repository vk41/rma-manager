<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Models\User;
use App\Libraries\Site;
use App\Models\Company;
use App\Models\RmaCase;
use App\Models\Customer;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class AdminCaseTest extends TestCase
{
    use RefreshDatabase;
    protected $company;
    protected $user;
    public function setUp(): void
    {
        parent::setUp();
        $this->company = factory(Company::class)->create();
        $this->user = factory(User::class)->create(['enabled' => 1,]);
        Site::setupCompany($this->company);
    }

    public function testAdminCanSeeHomeScreen()
    {
        $user = factory(User::class)->create(['enabled' => 1,]);
        $this
        ->actingAs($user)
        ->get('/admin')
        ->assertSuccessful()
        ->assertViewIs('admin.index')
        ->assertSee('Home')
        ->assertSee('Active RMAs')
        ->assertSee('Pending Cases')
        ->assertSee('Completed RMAs')
        ->assertSee('Pending Cases')
        ->assertSee('Completed (Collection Pending)')
        ->assertSee('Recent Cases');
    }

    public function testAdminCanCaseCreatePage()
    {
        $user = factory(User::class)->create(['enabled' => 1,]);
        $this
        ->actingAs($user)
        ->get('/admin/case/create')
        ->assertSuccessful()
        ->assertViewIs('admin.case.create')
        ->assertSee('Customer')
        ->assertSee('Part Number')
        ->assertSee('Serial Number')
        ->assertSee('Invoice Number')
        ->assertSee('Problem')
        ->assertSee('Problem Description');
    }

    public function testAdminCanGetProductAutocompleteData()
    {
        $user = factory(User::class)->create(['enabled' => 1,]);
        $this
        ->actingAs($user)
        ->postJson('/admin/case/autofill', ['serial_number' => 'test'])
        ->assertSuccessful()
        ->assertJsonStructure([
            'success',
            'message',
            'data'
        ]);
    }

    public function testAdminCanCreateCase()
    {
        $customer = factory(Customer::class)->create(['company_id' => $this->company->id, 'login_enabled' => 1,]);
        $user = factory(User::class)->create(['enabled' => 1,]);
        $caseData = collect([
            'customer' => $customer->id,
            'part_number' => 'hp -envy-x360/1tb',
            'serial_number' => 'CND02827L0',
            'invoice_number' => 'asdfas',
            'problem_summary' => 'Display Problem',
            'problem_description' => 'Test Problem',
            'password_protected' => [ 'value' => ['no']],
            'backup_done' =>[ 'value' => ['no']],
            'accessories' => [
                'value' => ['box', 'adaptor'],
                'remarks' => 'Test remakrs'
            ],
        ]);
        $this
        ->actingAs($user)
        ->followingRedirects()
        ->post('/admin/case', $caseData->toArray())        
        ->assertSuccessful();
        
        $this->assertDatabaseHas('rma_cases', $caseData->only(['part_number', 'serial_number', 'invoice_number', 'problem_summary', 'problem_description'])->toArray());
    }

    public function testAdminCanSeeCasePage()
    {
        $customer = factory(Customer::class)->create(['company_id' => $this->company->id, 'login_enabled' => 1,]);
        $case = factory(RmaCase::class)->create(['customer_id'=>$customer->id]);
        $user = factory(User::class)->create(['enabled' => 1,]);
        $this
        ->actingAs($user)
        ->get(route('admin.case.show', ['case_number' => $case->case_number]))
        ->assertSuccessful()
        ->assertViewIs('admin.case.show')
        ->assertSee($customer->namme)
        ->assertSee($case->case_number)
        ->assertSee($case->part_number)
        ->assertSee($case->serial_number);
    }

    
    public function testAdminCanSeeActiveCasesPage()
    {
        $user = factory(User::class)->create(['enabled' => 1,]);
        $this
        ->actingAs($user)
        ->get('/admin/case/active')
        ->assertSuccessful()
        ->assertViewIs('admin.case.active')
        ->assertSee('Active RMAs');
    }

    public function testAdminCanSeePendingCasesPage()
    {
        $user = factory(User::class)->create(['enabled' => 1,]);
        $this
        ->actingAs($user)
        ->get('/admin/case/pending')
        ->assertSuccessful()
        ->assertViewIs('admin.case.pending')
        ->assertSee('Pending Cases');
    }

    public function testAdminCanSeeCompletedCasesPage()
    {
        $user = factory(User::class)->create(['enabled' => 1,]);
        $this
        ->actingAs($user)
        ->get('/admin/case/completed')
        ->assertSuccessful()
        ->assertViewIs('admin.case.completed')
        ->assertSee('Completed RMAs');
    }

    public function testAdminCanSeeOtherCasesPage()
    {
        $user = factory(User::class)->create(['enabled' => 1,]);
        $this
        ->actingAs($user)
        ->get('/admin/case/others')
        ->assertSuccessful()
        ->assertViewIs('admin.case.others')
        ->assertSee('Other Cases');
    }

    //
    public function testUnauthenticatedCantSeeAdminCreateCasePage()
    {        
        $this->get('/admin/case/create')->assertRedirect('/admin/auth/login');
    }

    public function testUnauthenticatedCantSeeAdminActiveCasesPage()
    {        
        $this->get('/admin/case/active')->assertRedirect('/admin/auth/login');
    }

    public function testUnauthenticatedCantSeeAdminPendingCasesPage()
    {        
        $this->get('/admin/case/pending')->assertRedirect('/admin/auth/login');
    }

    public function testUnauthenticatedCantSeeAdminCompletedCasesPage()
    {        
        $this->get('/admin/case/completed')->assertRedirect('/admin/auth/login');
    }

    public function testUnauthenticatedCantSeeAdminOtherCasesPage()
    {        
        $this->get('/admin/case/others')->assertRedirect('/admin/auth/login');
    }


}
