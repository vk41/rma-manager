<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Libraries\Site;
use App\Models\Company;
use App\Models\RmaCase;
use App\Models\Customer;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CustomerCaseTest extends TestCase
{
    use RefreshDatabase;
    protected $company;
    protected $guard = 'customer';

    public function setUp(): void
    {
        parent::setUp();
        $this->company = factory(Company::class)->create();
        Site::setupCompany($this->company);
    }
    
    public function testCustomerCanSeeHomeScreen()
    {
        $customer = factory(Customer::class)->create(['company_id' => $this->company->id, 'login_enabled' => 1,]);
        $this
        ->actingAs($customer, $this->guard)
        ->get('/')
        ->assertSuccessful()
        ->assertViewIs('customer.index')
        ->assertSee('Home')
        ->assertSee('Active RMAs')
        ->assertSee('Pending Cases')
        ->assertSee('Completed RMAs')
        ->assertSee('Ready to Collect')
        ->assertSee('Recent Cases');
    }

    public function testCustomerCanCaseCreatePage()
    {
        $customer = factory(Customer::class)->create(['company_id' => $this->company->id, 'login_enabled' => 1,]);
        $this
        ->actingAs($customer, $this->guard)
        ->get('/case/create')
        ->assertSuccessful()
        ->assertViewIs('customer.case.create')
        ->assertSee('Part Number')
        ->assertSee('Serial Number')
        ->assertSee('Invoice Number')
        ->assertSee('Problem')
        ->assertSee('Problem Description');
    }

    public function testCustomerCanCreateCase()
    {
        $customer = factory(Customer::class)->create(['company_id' => $this->company->id, 'login_enabled' => 1,]);
        $caseData = collect([
            'part_number' => 'hp -envy-x360/1tb',
            'serial_number' => 'CND02827L0',
            'invoice_number' => 'asdfas',
            'problem_summary' => 'Display Problem',
            'problem_description' => 'Test Problem',
            'password_protected' => [ 'value' => ['no']],
            'backup_done' =>[ 'value' => ['no']],
            'accessories' => [
                'value' => ['box', 'adaptor'],
                'remarks' => 'Test remakrs'
            ],
        ]);
        $this
        ->actingAs($customer, $this->guard)
        ->followingRedirects()
        ->post('/case', $caseData->toArray())        
        ->assertSuccessful();
        
        $this->assertDatabaseHas('rma_cases', $caseData->only(['part_number', 'serial_number', 'invoice_number', 'problem_summary', 'problem_description'])->toArray());
    }

    public function testCustomerCanSeeCasePage()
    {
        $customer = factory(Customer::class)->create(['company_id' => $this->company->id, 'login_enabled' => 1,]);
        $case = factory(RmaCase::class)->create(['customer_id'=>$customer->id]);
        $this
        ->actingAs($customer, $this->guard)
        ->get(route('customer.case.show', ['case_number' => $case->case_number]))
        ->assertSuccessful()
        ->assertViewIs('customer.case.show')
        ->assertSee($case->case_number)
        ->assertSee($case->part_number)
        ->assertSee($case->serial_number);
    }

    public function testCustomerCanSeeActiveCasesPage()
    {
        $customer = factory(Customer::class)->create(['company_id' => $this->company->id, 'login_enabled' => 1,]);
        $this
        ->actingAs($customer, $this->guard)
        ->get('/case/active')
        ->assertSuccessful()
        ->assertViewIs('customer.case.active')
        ->assertSee('Active RMAs');
    }

    public function testCustomerCanSeePendingCasesPage()
    {
        $customer = factory(Customer::class)->create(['company_id' => $this->company->id, 'login_enabled' => 1,]);
        $this
        ->actingAs($customer, $this->guard)
        ->get('/case/pending')
        ->assertSuccessful()
        ->assertViewIs('customer.case.pending')
        ->assertSee('Pending Cases');
    }

    public function testCustomerCanSeeCompletedCasesPage()
    {
        $customer = factory(Customer::class)->create(['company_id' => $this->company->id, 'login_enabled' => 1,]);
        $this
        ->actingAs($customer, $this->guard)
        ->get('/case/completed')
        ->assertSuccessful()
        ->assertViewIs('customer.case.completed')
        ->assertSee('Completed RMAs');
    }

    public function testCustomerCanSeeOtherCasesPage()
    {
        $customer = factory(Customer::class)->create(['company_id' => $this->company->id, 'login_enabled' => 1,]);
        $this
        ->actingAs($customer, $this->guard)
        ->get('/case/others')
        ->assertSuccessful()
        ->assertViewIs('customer.case.others')
        ->assertSee('Other Cases');
    }

    //
    public function testUnauthenticatedCantSeeCreateCasePage()
    {        
        $this->get('/case/create')->assertRedirect('/auth/login');
    }

    public function testUnauthenticatedCantSeeActiveCasesPage()
    {        
        $this->get('/case/active')->assertRedirect('/auth/login');
    }

    public function testUnauthenticatedCantSeePendingCasesPage()
    {        
        $this->get('/case/pending')->assertRedirect('/auth/login');
    }

    public function testUnauthenticatedCantSeeCompletedCasesPage()
    {        
        $this->get('/case/completed')->assertRedirect('/auth/login');
    }

    public function testUnauthenticatedCantSeeOtherCasesPage()
    {        
        $this->get('/case/others')->assertRedirect('/auth/login');
    }

}
