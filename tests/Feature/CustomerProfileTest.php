<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Libraries\Site;
use App\Models\Company;
use App\Models\Customer;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CustomerProfileTest extends TestCase
{
    use RefreshDatabase;
    protected $company;
    protected $guard = 'customer';

    public function setUp(): void
    {
        parent::setUp();
        $this->company = factory(Company::class)->create();
        Site::setupCompany($this->company);
    }

    public function testCustomerCanSeeProfilePage()
    {
        $customer = factory(Customer::class)->create(['company_id' => $this->company->id, 'login_enabled' => 1,]);
        $this
            ->actingAs($customer, $this->guard)
            ->get(route('customer.user.profile'))
            ->assertSuccessful()
            ->assertViewIs('customer.user.profile')
            ->assertSee($customer->contact_name)
            ->assertSee($customer->contact_number)
            ->assertSee($customer->contact_email);
    }

    public function testCustomerCanEditProfile()
    {
        $customer = factory(Customer::class)->create(['company_id' => $this->company->id, 'login_enabled' => 1,]);
        $newData = [
            'contact_name' => 'test-company',
            'contact_number' => '11415451',
            'contact_email' => 'hello@ss.com',
        ];

        $this
            ->actingAs($customer, $this->guard)
            ->followingRedirects()
            ->post(route('customer.user.profile'), $newData)
            ->assertSuccessful();

        $this->assertDatabaseHas('customers', $newData);
    }
}
