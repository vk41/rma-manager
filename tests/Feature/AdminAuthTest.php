<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Models\User;
use App\Libraries\Site;
use App\Models\Company;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class AdminAuthTest extends TestCase
{
    use RefreshDatabase;
    protected $company;
    
    public function setUp(): void
    {
        parent::setUp();
        $this->company = factory(Company::class)->create();
        Site::setupCompany($this->company);
    }

    public function testRedirectionToAdminLoginPage()
    {
        $this
            ->get('/admin')
            ->assertStatus(302)
            ->assertRedirect('/admin/auth/login');
    }

    public function testAdminCanSeeLoginForm()
    {
        $this
            ->get('/admin/auth/login')
            ->assertSuccessful()
            ->assertViewIs('admin.auth.login');
    }

    public function testEnabledAdminCanLogin()
    {
        $user = factory(User::class)->create(['enabled' => 1,]);
        $this
            ->followingRedirects()
            ->post('/admin/auth/login', [
                'email' => $user->email,
                'password' => 'password',
            ])
            ->assertSuccessful()
            ->assertSessionMissing('errors');

        $this->assertAuthenticatedAs($user);
    }

    public function testAdminCanLogout()
    {
        $user = factory(User::class)->create(['enabled' => 1,]);
        $this
            ->followingRedirects()
            ->actingAs($user)
            ->post('/auth/logout')
            ->assertSuccessful();

        $this->assertAuthenticatedAs($user);
    }

    public function testLogedInAdminCantSeeLoginPage()
    {
        $user = factory(User::class)->create(['enabled' => 1,]);
        $this
            ->actingAs($user)
            ->get('/admin/auth/login')
            ->assertRedirect('/admin');
    }

    public function testDisabledAdminCantLogin()
    {
        $user = factory(User::class)->create(['enabled' => 0,]);
        $this
            ->post('/admin/auth/login', [
                'email' => $user->email,
                'password' => 'password',
            ])
            ->assertSessionHasErrors();
    }

}
