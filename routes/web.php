<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::group(['as' => 'customer.', 'namespace'=>'Customer',], function () {
    require 'customerRoutes.php';
});
Route::group(['prefix' => 'admin', 'as' => 'admin.', 'namespace'=>'Admin',], function () {
    require 'adminRoutes.php';
});

Route::group(['prefix' => 'open', 'as' => 'open.', 'namespace'=>'Open',], function () {
    Route::get('/{uid}/print', 'IndexController@print')->name('print');
});


