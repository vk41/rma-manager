<?php

use Illuminate\Support\Facades\Route;



Route::get('/', 'IndexController@index')->name('index')->middleware('auth');
Route::get('/search', 'IndexController@search')->name('search')->middleware('auth');
Route::get('/search/data', 'IndexController@searchdata')->name('searchdata')->middleware('auth');

Route::group(['prefix' => 'case', 'as' => 'case.', 'middleware' => 'auth'], function () {
  Route::get('/', 'CaseController@index')->name('index');
  Route::get('/create', 'CaseController@create')->name('create');
  Route::post('/', 'CaseController@store')->name('store');

  Route::get('/active', 'CaseController@active')->name('active');
  Route::get('/pending', 'CaseController@pending')->name('pending');
  Route::get('/completed', 'CaseController@completed')->name('completed');
  Route::get('/others', 'CaseController@others')->name('others');
  Route::post('/autofill', 'CaseController@autofill')->name('autofill');

  
  Route::get('/data/{status?}', 'CaseController@data')->name('data');
  Route::group(['prefix' => '{case_number}'], function () {
    Route::get('/', 'CaseController@show')->name('show'); 
    Route::get('/print', 'CaseController@print')->name('print'); 

    Route::get('/action/{type}', 'CaseController@actionShow')->name('action-show');
    Route::post('/accept', 'CaseController@acceptOrReject')->name('accept');
    Route::post('/receive', 'CaseController@receive')->name('receive');
    Route::post('/location', 'CaseController@location')->name('location');
    Route::post('/completion', 'CaseController@completion')->name('completion');
    Route::post('/delivery', 'CaseController@delivery')->name('delivery');
    Route::post('/vendor', 'CaseController@vendor')->name('vendor');
    Route::post('/comment', 'CaseController@comment')->name('comment');

  });
});

Route::group(['prefix' => 'report', 'as' => 'report.', 'middleware' => 'auth'], function () {
  Route::get('/month', 'ReportController@month')->name('month'); 
  Route::get('/active', 'ReportController@active')->name('active'); 
});


Route::get('customers/data', 'CustomerController@data')->name('customers.data')->middleware('auth');
Route::get('customers/autocomplete', 'CustomerController@autocomplete')->name('customers.autocomplete')->middleware('auth');
Route::post('customers/{customer}/status', 'CustomerController@status')->name('customers.status')->middleware('auth');
Route::resource('customers', CustomerController::class)->middleware('auth');


Route::get('users/data', 'UserController@data')->name('users.data')->middleware('auth');
Route::post('users/{customer}/status', 'UserController@status')->name('users.status')->middleware('auth');
Route::resource('users', UserController::class)->middleware('auth');


Route::group(['prefix' => 'auth', 'as' => 'auth.'], function () {
  // Route::get('/', 'UserController@index')->name('profile');
  Route::post('/', 'AuthController@update')->name('update');
  Route::get('/login', 'AuthController@showLoginForm')->name('login');
  Route::post('/login', 'AuthController@login');
  Route::post('/logout', 'AuthController@logout')->name('logout');
});
