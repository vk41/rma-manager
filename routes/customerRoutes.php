<?php

use Illuminate\Support\Facades\Route;



Route::get('/', 'IndexController@index')->name('index')->middleware('auth:customer');
Route::get('/search', 'IndexController@search')->name('search')->middleware('auth:customer');
// Route::get('/search/data', 'IndexController@searchdata')->name('searchdata')->middleware('auth:customer');

Route::group(['prefix' => 'case', 'as' => 'case.', 'middleware' => 'auth:customer'], function () {
  Route::get('/', 'CaseController@index')->name('index');
  Route::get('/create', 'CaseController@create')->name('create');
  Route::post('/', 'CaseController@store')->name('store');
  Route::get('/active', 'CaseController@active')->name('active');
  Route::get('/pending', 'CaseController@pending')->name('pending');
  Route::get('/completed', 'CaseController@completed')->name('completed');
  Route::get('/others', 'CaseController@others')->name('others');
  Route::post('/autofill', 'CaseController@autofill')->name('autofill');
  Route::get('/data/{status?}', 'CaseController@data')->name('data');
  Route::get('/{case_number}', 'CaseController@show')->name('show'); // Should be at the bottom
  Route::get('/{case_number}/print', 'CaseController@print')->name('print'); // Should be at the bottom
});

Route::group(['prefix' => 'user', 'as' => 'user.', 'middleware' => 'auth:customer'], function () {
  Route::get('/', 'UserController@index')->name('profile');
  Route::post('/', 'UserController@update')->name('update');
  
});



Route::group(['prefix' => 'auth', 'as' => 'auth.'], function () {
  Route::get('/login', 'AuthController@showLoginForm')->name('login');
  Route::post('/login', 'AuthController@login');
  Route::post('/logout', 'AuthController@logout')->name('logout');
  // Password reset
  Route::get('password/reset', 'AuthController@showLinkRequestForm')->name('password.request');
  Route::post('password/email', 'AuthController@sendResetLinkEmail')->name('password.email');
  Route::get('password/reset/{token}', 'AuthController@showResetForm')->name('password.reset');
  Route::post('password/reset', 'AuthController@reset')->name('password.update');

  // Route::get('/', 'UserController@index')->name('profile');
  // Route::post('/', 'AuthController@update')->name('update');
  // Route::get('/login', 'AuthController@showLoginForm')->name('login');
  // Route::post('/login', 'AuthController@login');
  // Route::post('/logout', 'AuthController@logout')->name('logout');
});
