<?php

namespace App\Models;

use App\Models\CaseEvent;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RmaCase extends Model
{
    
    use SoftDeletes;
    protected $guarded = [];

    protected static function boot()
    {
        parent::boot();
        static::creating(function ($case) {
            $case->uid = (string) Str::uuid();
        });
    }

    public function scopeActive($query)
    {
        return $query->whereIn('status', ['accepted', 'active']);
    }

    public function customer() {
        return $this->belongsTo('App\Models\Customer')->withTrashed();
    }
    public function checkLists() {
        return $this->hasMany('App\Models\CaseChecklist');
    }

    public function events() {
        return $this->hasMany('App\Models\CaseEvent');
    }

    public function vendorRmas() {
        return $this->hasMany('App\Models\VendorRma');
    }

    public function addEvent($name, $remarks = null) {
        $event = new CaseEvent([
            'name' => $name,
            'remarks' => $remarks,
            'created_by' => (auth()->user()->is_customer) ? 0 : auth()->user()->id,
        ]);
        $this->events()->save($event);
    }
}
