<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CaseChecklist extends Model
{
    use SoftDeletes;
    protected $guarded = [];
    protected $casts = [
        'value' => 'array',
    ];

    public function rmaCase()
    {
        return $this->belongsTo('App\Models\RmaCase');
    }
}
