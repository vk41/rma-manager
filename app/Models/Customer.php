<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Customer extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;
    protected $guard = 'customer'; // Auth guard
    protected $guarded = [];
    protected $casts = [
        'login_enabled' => 'boolean',
    ];
    // protected $appends = ['is_customer', 'is_user'];

    // protected $hidden = [
    //     'password', 'remember_token',
    // ];

    public function company() {
        return $this->belongsTo('App\Models\Company');
    }

    public function rmaCases() {
        return $this->hasMany('App\Models\RmaCase');
    }
    
    public function getIsCustomerAttribute()
    {
        return true;
    }
    
    public function getIsUserAttribute()
    {
        return false;
    }

    public function showRatingStars() {
        $rating = $this->rating;
        $solid = floor($rating/2);
        $half = $rating % 2;
        $empty = floor(5 - ($rating/2));
        $stars = array_merge(array_fill(0, $solid, 'fas fa-star'), array_fill(0, $half, 'fas fa-star-half-alt'), array_fill(0, $empty, 'far fa-star'));
        $starResponse = [];
        $title = $rating/2 . ' Star Customer';
        foreach($stars as $star) {
            $starResponse[] = '<i title="' . $title . '" class="' . $star . '"></i>';
        }
        return implode(' ', $starResponse);

    }
}
