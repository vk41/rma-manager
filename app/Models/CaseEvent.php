<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CaseEvent extends Model
{
    use SoftDeletes;
    protected $guarded = [];
    public function rmaCase()
    {
        return $this->belongsTo('App\Models\RmaCase');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'created_by');
    }
}
