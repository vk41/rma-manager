<?php

namespace App\Http\Controllers\Admin;

use App\Models\Company;
use App\Models\Customer;
use App\Libraries\Helper;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Yajra\DataTables\Facades\DataTables;

class CustomerController extends Controller
{
    public function index()
    {
        return view('admin.customers.index');
    }

    public function data()
    {
        $customers = Customer::with('company');
        return DataTables::eloquent($customers)
            ->addColumn('company', function (Customer $customer) {
                return $customer->company->name;
            })
            ->toJson();
    }
    
    public function autocomplete(Request $request)
    {
        $keyword = $request->term;
        $result  = Customer::where('name', 'LIKE', "%{$keyword}%")
            ->orWhere('code', 'LIKE', "%{$keyword}%")
            ->take(20)
            ->get()
            ->map(function ($customer) {
                return ['id' => $customer->id, 'text' => $customer->code . ' - ' . $customer->name];
            });
        return response()->json(['results' => $result]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.customers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'code' => ['required'],
            'name' => ['required'],
            'location' => ['required'],
            'timezone' => ['required'],
            'contact_name' => ['required'],
            'contact_number' => ['required'],
            'contact_email' => ['required', 'email'],
            'rating' => ['required'],
            'email' => ['required', 'email'],
            'password' => ['required'],
            'company' => ['required'],
            'salesman' => ['required'],
            'remarks' => ['max:200'],
        ]);

        
        $customerData = $request->only(['code', 'name', 'location', 'timezone', 'contact_name', 'contact_number', 'contact_email', 'email', 'password', 'salesman', 'remarks']);
        $customerData['password'] = Hash::make($customerData['password']);
        $customerData['company_id'] = $request->company;
        $customer = new Customer($customerData);
        $customer->save();
        if($request->password && $request->send_password) {
            Helper::sendCustomerMail($customer, new \App\Mail\Customer\CredentialMail($customer, $request->password)); 
        }
        return redirect()->route('admin.customers.index')
            ->with(['success-message' => 'Customer created.']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Customer $customer)
    {
        return view('admin.customers.show', ['customer' => $customer]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Customer $customer)
    {
        return view('admin.customers.edit', ['customer' =>$customer]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'code' => ['required'],
            'name' => ['required'],
            'location' => ['required'],
            'timezone' => ['required'],
            'contact_name' => ['required'],
            'contact_number' => ['required'],
            'contact_email' => ['required', 'email'],
            'rating' => ['required'],
            'email' => ['required', 'email'],            
            'company' => ['required'],
            'salesman' => ['required'],
            'remarks' => ['max:200'],
        ]);

        
        $customerData = $request->only(['code', 'name', 'location', 'timezone', 'contact_name', 'contact_number', 'contact_email', 'email', 'salesman', 'remarks']);
        $passwordChanged = '';
        if($request->password) {
            $customerData['password'] = Hash::make($request->password);
            $passwordChanged = 'Password changed.';
        }
        $customerData['company_id'] = $request->company;
        Customer::where('id', $id)->update($customerData);
        if($request->password && $request->send_password) {
            $customer = Customer::find($id);
            Helper::sendCustomerMail($customer, new \App\Mail\Customer\CredentialMail($customer, $request->password, true)); 
        }
        return redirect()->route('admin.customers.index')
            ->with(['success-message' => 'Customer Updated. '. $passwordChanged]); 
    }

    

    public function status(Customer $customer, Request $request)
    {           
        $customer->login_enabled = intval($request->login_enabled);
        $customer->save();
        $message = ($customer->login_enabled) ? 'Customer Enabled': 'Customer Disabled.';
        return redirect()->route('admin.customers.index')
            ->with(['success-message' => $message]); 
    }
}
