<?php

namespace App\Http\Controllers\Admin;

use App\Models\RmaCase;
use Illuminate\Http\Request;
use App\Libraries\CaseLibrary;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class IndexController extends Controller
{
    public function index() {
        $customer = auth()->user();
        $cases = RmaCase::select(['case_number', 'problem_summary','outcome','problem_description','status','rma_number','part_number', 'serial_number' ])
        ->whereDate('created_at','>', now()->subYear())
        ->get();

        $receivedMonthly = RmaCase::select(DB::raw("DATE_FORMAT(product_received_on, '%M') month") , DB::raw('count(*) as total'))
        ->whereNotNull('product_received_on')
        ->whereDate('product_received_on','>', now()->subYear())
        ->groupBy('month')
        ->orderBy('product_received_on')
        ->get()
        ->pluck('total', 'month');

        $completedMonthly = RmaCase::select(DB::raw("DATE_FORMAT(completed_on, '%M') month") , DB::raw('count(*) as total'))
        ->whereNotNull('completed_on')
        ->whereDate('completed_on','>', now()->subYear())
        ->groupBy('month')
        ->where('status', 'completed')
        ->orderBy('completed_on')
        ->get()
        ->pluck('total', 'month');

        $failedMonthly = RmaCase::select(DB::raw("DATE_FORMAT(completed_on, '%M') month") , DB::raw('count(*) as total'))
        ->whereNotNull('completed_on')
        ->whereDate('completed_on','>', now()->subYear())
        ->groupBy('month')
        ->where('status', 'failed')
        ->orderBy('completed_on')
        ->get()
        ->pluck('total', 'month');
        
        $graphMonth = \Carbon\Carbon::now()->subMonths(12);
        $counters = [
            'received' => [],
            'completed' => [],
            'failed' => [],
            'graphLabels' => [],
            'active' => $cases->whereIn('status', ['accepted','active'])->count()
        ];
        for($i = 0; $i <= 11; $i++){
            $curentMonth = $graphMonth->addMonth()->format('F');
            $counters['received'][$curentMonth] = (isset($receivedMonthly[$curentMonth])) ? $receivedMonthly[$curentMonth]: 0;
            $counters['completed'][$curentMonth] = (isset($completedMonthly[$curentMonth])) ? $completedMonthly[$curentMonth]: 0;
            $counters['failed'][$curentMonth] = (isset($failedMonthly[$curentMonth])) ? $failedMonthly[$curentMonth]: 0;
            $counters['graphLabels'][] = $curentMonth;
        }
        
        $longestActiveRmas = RmaCase::where('status', 'active')->orderBy('product_received_on')->whereNotNull('product_received_on')->take(4)->get();
        // return ($longestActiveRmas);

        $recentCases = RmaCase::select(['case_number', 'serial_number','outcome','problem_summary','problem_description','status' ])
        ->latest()
        ->take(5)
        ->get();

        $pendingCases = RmaCase::select(['case_number', 'serial_number','part_number','outcome','problem_summary','problem_description','status' ])
        ->where('status', 'pending')
        ->latest()
        ->take(10)
        ->get();

        $readyToCollect = RmaCase::select(['case_number', 'serial_number','part_number','outcome','problem_summary','problem_description','status' ])
        ->where('status', 'completed')
        ->whereNotNull('completed_on')
        ->whereNull('delivered_on')
        ->get();
        return view('admin.index', [
            'counters' => $counters, 
            'recentCases' =>$recentCases, 
            'pendingCases' => $pendingCases, 
            'readyToCollect'=>$readyToCollect, 
            'longestActiveRmas' =>$longestActiveRmas
            ]);
    }

    public function search(Request $request) {
        $validatedData = $request->validate([
            'keyword' => ['min:4'],
        ]);
        $keyword = $request->keyword;
        $casesQuery = CaseLibrary::search($keyword);
        if($casesQuery->count() == 1) {
            $case = $casesQuery->first();
            return redirect()->route('admin.case.show', ['case_number' => $case->case_number])
            ->with(['info-message' => "There was only one case for the search query : <b>{$keyword}</b>."]);
        }
        // return $cases->count();

        return view('admin.search', ['keyword' => $keyword, 'cases' => $casesQuery->get()]);
    }
}
