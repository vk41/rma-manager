<?php

namespace App\Http\Controllers\Admin;


use App\Libraries\Helper;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Yajra\DataTables\Facades\DataTables;

class UserController extends Controller
{
    public function index()
    {
        return view('admin.users.index');
    }

    public function data()
    {
        $users = User::query();
        return DataTables::eloquent($users)->toJson();
    }
    

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.users.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        $request->validate([            
            'name' => ['required'],
            'email' => ['required', 'email'],
            'password' => ['required'],
            'role' => ['required'],
            'timezone' => ['required'],
        ]);

        
        $userData = $request->only([ 'name',   'email', 'password', 'salesman', 'role', 'timezone']);
        $userData['password'] = Hash::make($userData['password']);
        $user = new User($userData);
        $user->save();
        return redirect()->route('admin.users.index')
            ->with(['success-message' => 'User created.']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        return view('admin.users.show', ['user' => $user]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        return view('admin.users.edit', ['user' =>$user]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([            
            'name' => ['required'],
            'email' => ['required', 'email'],
            'role' => ['required'],
            'timezone' => ['required'],
        ]);

        
        // $customerData = $request->only(['code', 'name', 'location', 'timezone', 'contact_name', 'contact_number', 'contact_email', 'email', 'salesman', 'remarks']);
        $userData = $request->only([ 'name',   'email', 'salesman', 'role', 'timezone']);
        $passwordChanged = '';
        if($request->password) {
            $userData['password'] = Hash::make($request->password);
            $passwordChanged = 'Password changed.';
        }
        
        User::where('id', $id)->update($userData);
        return redirect()->route('admin.users.index')
            ->with(['success-message' => 'User Updated. '. $passwordChanged]); 
    }

    

    public function status($user, Request $request)
    {          
        $user = User::find($user);
        $user->enabled = intval($request->enabled);
        $user->save();
        $message = ($user->enabled) ? 'User Enabled': 'User Disabled.';
        return redirect()->route('admin.users.index')
            ->with(['success-message' => $message]); 
    }
}
