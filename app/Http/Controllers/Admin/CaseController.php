<?php

namespace App\Http\Controllers\Admin;

use Carbon\Carbon;
use App\Models\RmaCase;
use App\Models\Customer;
use App\Libraries\Helper;
use Illuminate\Http\Request;
use App\Libraries\CaseLibrary;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use App\Http\Requests\RmaCaseRequest;
use Yajra\DataTables\Facades\DataTables;

class CaseController extends Controller
{
    public function create()
    {
        return view('admin.case.create');
    }

    public function store(RmaCaseRequest $request)
    {
        $customer = Customer::where('code', $request->customer)->first();
        if(!$customer) {
            return redirect()->back()->withInput()
                ->withErrors(['Customer not found. Please create customer']);
        }
        $isExists = CaseLibrary::checkIfExits($request);
        if ($isExists) {
            $url = route('admin.case.show', ['case_number' => $isExists->case_number]);
            return redirect()->back()->withInput()
                ->withErrors(["A case for this product already exits. Please check and try again. Visit <a class='text-dark' href='$url'>$isExists->case_number</a>."]);
        }
        try {
            
            $case = CaseLibrary::create($request, $customer);
            Helper::sendCustomerMail($customer, new \App\Mail\Customer\CaseCreatedMail($case));
            return redirect()->route('admin.case.show', ['case_number' => $case->case_number])->with(['success-message' => 'Case created. We will review and take action at the earliest.']);
        } catch (\Exception $exception) {
            return redirect()->back()->withInput()->with(['error-message' => $exception->getMessage()]);
        }
    }

    public function show($case_number)
    {
        $case = CaseLibrary::getCaseByCaseNumber($case_number);
        if (!$case) {
            abort(422);
        }
        return view('admin.case.show', ['case' => $case]);
    }

    public function print($case_number)
    {
        $case = CaseLibrary::getCaseByCaseNumber($case_number);
        if (!$case) {
            abort(422);
        }
        $customer = $case->customer;
        $company = $customer->company;
        return view('print.case', ['case' => $case, 'customer' => $customer, 'company' => $company]);
    }

    public function active()
    {
        return view('admin.case.active');
    }

    public function pending()
    {
        return view('admin.case.pending');
    }

    public function completed()
    {
        return view('admin.case.completed');
    }
    public function others()
    {
        return view('admin.case.others');
    }

    public function data($status = null)
    {
        $cases = RmaCase::with(['customer.company']);
        switch ($status) {
            case 'active':
                $cases = $cases->whereIn('status', ['accepted', 'active']);
                break;
            case 'pending':
                $cases = $cases->whereIn('status', ['pending']);
                break;
            case 'completed':
                $cases = $cases->whereIn('status', ['completed'])->whereDate('created_at', '>', now()->subYear());
                break;
            case 'others':
                $cases = $cases->whereNotIn('status', ['accepted', 'active', 'pending', 'completed'])->whereDate('created_at', '>', now()->subYear());
                break;
        }
        return DataTables::eloquent($cases)
            ->addColumn('company_name', function (RmaCase $case) {
                return $case->customer->company->name;
            })
            ->addColumn('customer_code', function (RmaCase $case) {
                return $case->customer->code;
            })
            ->addColumn('rma_issued_at', function (RmaCase $case) {
                $timeZone = auth()->user()->timezone;
                return Carbon::parse($case->rma_issued_at)->timezone($timeZone)->format('Y-m-d');
            })
            ->addColumn('case_created_on', function (RmaCase $case) {
                $timeZone = auth()->user()->timezone;
                return Carbon::parse($case->created_at)->timezone($timeZone)->format('Y-m-d');
            })
            ->toJson();
    }

    public function actionShow($case_number, $type)
    {
        $case = CaseLibrary::getCaseByCaseNumber($case_number);
        if (!$case) {
            abort(422);
        }

        switch ($type) {
            case 'accept':
                if (!in_array($case->status, ['pending', 'accepted', 'rejected'])) {
                    return redirect()->route('admin.case.show', ['case_number' => $case->case_number])
                        ->with(['warning-message' => 'Case is already ' . $case->status]);
                }
                return view('admin.case.accept', ['case' => $case]);
                break;
            case 'collection':
                if (!in_array($case->status, ['accepted'])) {
                    return redirect()->route('admin.case.show', ['case_number' => $case->case_number])
                        ->with(['warning-message' => 'Case is already ' . $case->status]);
                }
                return view('admin.case.collection', ['case' => $case]);
                break;
            case 'completion':
                if (!in_array($case->status, ['active'])) {
                    return redirect()->route('admin.case.show', ['case_number' => $case->case_number])
                        ->with(['warning-message' => 'Case is already ' . $case->status]);
                }
                return view('admin.case.completion', ['case' => $case]);
                break;
            case 'edit':
                return view('admin.case.edit', ['case' => $case]);
                break;

            case 'vendor':
                return view('admin.case.vendor', ['case' => $case]);
                break;
        }
        return abort(404);
    }

    public function acceptOrReject($case_number, Request $request)
    {
        $case = RmaCase::where('case_number', $case_number)->first();
        if (!$case) {
            abort(422);
        }
        $case = CaseLibrary::acceptorReject($case, $request);
        if ($request->status == 'accepted') {
            Helper::sendCustomerMail($case->customer, new \App\Mail\Customer\RMAIssuedMail($case));
        } else {
            Helper::sendCustomerMail($case->customer, new \App\Mail\Customer\RMARejectedMail($case));
        }
        return redirect()->route('admin.case.show', ['case_number' => $case->case_number])
            ->with(['success-message' => 'Case updated.']);
    }

    public function receive($case_number, Request $request)
    {
        // dd($request->all());
        $case = RmaCase::where('case_number', $case_number)->first();
        if (!$case) {
            abort(422);
        }
        $case = CaseLibrary::receiveProduct($case, $request);
        Helper::sendCustomerMail($case->customer, new \App\Mail\Customer\ProductReceived($case));

        return redirect()->route('admin.case.show', ['case_number' => $case->case_number])
            ->with(['success-message' => 'Case updated.']);
    }

    public function location($case_number, Request $request)
    {
        $case = RmaCase::where('case_number', $case_number)->first();
        if (!$case) {
            abort(422);
        }
        if ($request->current_location == 'vendor') {
            return redirect()->route('admin.case.action-show', ['case_number' => $case->case_number, 'type' => 'vendor'])
                ->withInput()
                ->with(['info-message' => 'Provide additiional information.']);
            // ->route('admin.case.show', ['case_number' => $case->case_number])
            // ->with(['success-message' => 'Case updated.']);
        }
        $case = CaseLibrary::changeLocation($case, $request);



        return redirect()->route('admin.case.show', ['case_number' => $case->case_number])
            ->with(['success-message' => 'Case updated.']);
    }


    public function vendor($case_number, Request $request)
    {
        $request->validate([
            'vendor_name' => ['required'],
            'ref_number' => ['required'],
            'end_user' => ['required'],
        ]);

        $case = RmaCase::where('case_number', $case_number)->first();
        if (!$case) {
            abort(422);
        }
        $case = CaseLibrary::changeLocation($case, $request);
        $case->vendorRmas()->create([
            'vendor_name' => $request->vendor_name,
            'ref_number' => $request->ref_number,
            'end_user' => $request->end_user,
            'remarks' => $request->remarks,
            'created_by' => auth()->user()->id,
        ]);
        return redirect()->route('admin.case.show', ['case_number' => $case->case_number])
            ->with(['success-message' => 'Case location updated.']);
    }

    public function completion($case_number, Request $request)
    {
        $case = RmaCase::where('case_number', $case_number)->first();
        if (!$case) {
            abort(422);
        }
        $case = CaseLibrary::completion($case, $request);
        Helper::sendCustomerMail($case->customer, new \App\Mail\Customer\Completion($case));
        return redirect()->route('admin.case.show', ['case_number' => $case->case_number])
            ->with(['success-message' => 'Case updated.']);
    }

    public function comment($case_number, Request $request)
    {
        $case = RmaCase::where('case_number', $case_number)->first();
        if (!$case) {
            abort(422);
        }
        $case = CaseLibrary::comment($case, $request);
        return redirect()->route('admin.case.show', ['case_number' => $case->case_number])
            ->with(['success-message' => 'Comment added.']);
    }

    public function delivery($case_number, Request $request)
    {
        $case = RmaCase::where('case_number', $case_number)->first();
        if (!$case) {
            abort(422);
        }
        $case = CaseLibrary::delivery($case, $request);
        Helper::sendCustomerMail($case->customer, new \App\Mail\Customer\DeliveryMail($case));
        return redirect()->route('admin.case.show', ['case_number' => $case->case_number])
            ->with(['success-message' => 'Case updated.']);
    }

    public function autofill(Request $request)
    {
        $request->validate([
            'field' => 'required',
            'value' => 'required',
        ]);
        $serialNumber = $request->serial_number;
        $response = CaseLibrary::handleAutofill($serialNumber);
        return response()->json(['success' => true, 'message' => 'Completed', 'data' => $response]);
    }
}
