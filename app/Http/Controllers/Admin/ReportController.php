<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\RmaCase;
use Illuminate\Http\Request;

class ReportController extends Controller
{
    public function month() {
        
    }

    public function active() {
        $cases = RmaCase::active()->orderBy('product_received_on')->get();
        // dd($cases->toArray());
        return view('admin.report.active', ['cases' => $cases]);
    }
}
