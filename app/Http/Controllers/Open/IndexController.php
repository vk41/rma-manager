<?php

namespace App\Http\Controllers\Open;

use Illuminate\Http\Request;
use App\Libraries\CaseLibrary;
use App\Http\Controllers\Controller;

class IndexController extends Controller
{
    public function print($uid) 
    {
        $case = CaseLibrary::getCaseByUID($uid);        
        if (!$case || !in_array($case->status, ['accepted','active','completed'])) {
            abort(422);
        }
        $customer = $case->customer;
        $company = $customer->company;
        return view('print.case', ['case' => $case, 'customer' => $customer, 'company' => $company]);
    }
}
