<?php

namespace App\Http\Controllers\Customer;

use App\Libraries\Site;
use App\Models\Customer;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Password;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class AuthController extends Controller
{
    use AuthenticatesUsers;
    protected $redirectTo = '/';

    public function __construct()
    {
        $this->middleware('guest:customer')->except(['logout']);
    }

    public function showLoginForm()
    {
        return view('customer.auth.login');
    }

    public function showLinkRequestForm()
    {
        return view('customer.auth.email');
    }

    public function sendResetLinkEmail(Request $request)
    {
        $request->validate(['email' => 'required|email']);
        $customer = Customer::where('email', $request->email)->first();
        if($customer && $customer->login_enabled && $customer->company_id == Site::getId()) {
            $token = $this->broker()->createToken($customer);
            Mail::to($customer->email)->send(new \App\Mail\Customer\PasswordResetMail($customer, $token));
        }
        return redirect()->route('customer.auth.password.request')->with('success', true);        
    }

    public function showResetForm($token) {
        return view('customer.auth.reset', ['token' => $token]);
    }

    public function reset(Request $request) {
        $request->validate([
            'token' => 'required',
            'email' => 'required|email',
            'password' => 'required|confirmed|min:8',
        ]);

        $credentials =  $request->only( 'email', 'password', 'password_confirmation', 'token');
        $response = $this->broker()->reset($credentials, function ($customer, $password) {
                $this->resetPassword($customer, $password);
            }
        );
        return $response == Password::PASSWORD_RESET
                    ? redirect($this->redirectPath())->with('success-message', 'Password Changed.')
                    : redirect()->back()->withInput($request->only('email'))->withErrors(['email' => trans($response)]);
    }

    protected function resetPassword($customer, $password)
    {
        $customer->password = Hash::make($password);
        $customer->setRememberToken(Str::random(60));
        $customer->save();
        Mail::to($customer->email)->send(new \App\Mail\Customer\PasswordChanged($customer));
        $this->guard()->login($customer);
    }

    /**
     * Get the needed authorization credentials from the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    protected function credentials(Request $request)
    {
        return [
            'email' => $request->email,
            'password' => $request->password,
            'login_enabled' => 1,
            'company_id' => Site::getId(),
        ];
        
    }

    protected function guard()
    {
        return Auth::guard('customer');
    }

    public function broker()
    {
        return Password::broker('customers');
    }

    /**
     * The user has logged out of the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return mixed
     */
    protected function loggedOut(Request $request)
    {
        return redirect($this->redirectPath());
    }
}
