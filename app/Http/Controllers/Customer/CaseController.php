<?php

namespace App\Http\Controllers\Customer;

use Carbon\Carbon;
use App\Models\RmaCase;
use App\Libraries\Helper;
use Illuminate\Http\Request;
use App\Libraries\CaseLibrary;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use Yajra\DataTables\Facades\DataTables;

class CaseController extends Controller
{
    public function create()
    {
        return view('customer.case.create');
    }

    public function store(Request $request)
    {
        // dd($request->all());
        $isExists = CaseLibrary::checkIfExits($request);
        if ($isExists) {
            $url = route('customer.case.show', ['case_number' => $isExists->case_number]);
            return redirect()->back()->withInput()
                ->with(['warning-message' => "A case for this product already exits. Please check and try again. Visit <a class='text-dark' href='$url'>$isExists->case_number</a>."]);
        }
        try {
            $customer = $request->user();
            $case = CaseLibrary::create($request, $customer);
            Helper::sendCustomerMail($customer, new \App\Mail\Customer\CaseCreatedMail($case));
            Helper::sendAdminMail(new \App\Mail\Admin\CaseCreatedMail($case));
            // Mail::to($request->user()->contact_email)->send(new \App\Mail\Customer\CaseCreatedMail($case));
            return redirect()->route('customer.case.show', ['case_number' => $case->case_number])->with(['success-message' => 'Case created. We will review and take action at the earliest.']);
        } catch (\Exception $exception) {
            // dd($exception->getMessage());
            return redirect()->back()->withInput()->with(['error-message' => $exception->getMessage()]);
        }
    }

    public function show($case_number) {
        $case = CaseLibrary::getCaseByCaseNumber($case_number);
        if(!$case) {
            abort(422);
        }
        return view('customer.case.show', ['case' => $case]);
    }

    public function print($case_number) {
        $case = CaseLibrary::getCaseByCaseNumber($case_number);
        if(!$case) {
            abort(422);
        }
        $customer = $case->customer;
        $company = $customer->company;
        return view('print.case', ['case' => $case, 'customer' => $customer, 'company' => $company]);
    }

    public function active() {
        return view('customer.case.active');
    }

    public function pending() {
        return view('customer.case.pending');
    }

    public function completed() {
        return view('customer.case.completed');
    }
    public function others() {
        return view('customer.case.others');
    }

    public function data($status = null) {
        $customer = auth()->user();
        $cases = RmaCase::select(['case_number', 'problem_summary','outcome','problem_description','status','rma_number','part_number', 'serial_number', 'invoice_number', 'invoice_date', 'rma_issued_at', 'created_at', ]);
        switch($status) {
            case 'active':
                $cases = $cases->whereIn('status', ['accepted','active']);                
            break;
            case 'pending':
                $cases = $cases->whereIn('status', ['pending']);
            break;
            case 'completed':                
                $cases = $cases->whereIn('status', ['completed'])->whereDate('created_at','>', now()->subYear());
            break;
            case 'others':                
                $cases = $cases->whereNotIn('status', ['accepted','active', 'pending', 'completed'])->whereDate('created_at','>', now()->subYear());
            break;
        }
        
                
        $cases= $cases->where('customer_id', $customer->id);

        return DataTables::eloquent($cases)
            ->addColumn('rma_issued_at', function (RmaCase $case) {
                $timeZone = auth()->user()->timezone;
                return Carbon::parse($case->rma_issued_at)->timezone($timeZone)->format('Y-m-d');
            })
            ->addColumn('case_created_on', function (RmaCase $case) {
                $timeZone = auth()->user()->timezone;
                return Carbon::parse($case->created_at)->timezone($timeZone)->format('Y-m-d');
            })
            ->toJson();
        // return $cases;
        
        // return DataTables::of($cases)->make(true);
    }

    public function autofill(Request $request)
    {
        $request->validate([
            'serial_number' => 'required|max:100',
        ]);
        $serialNumber = $request->serial_number;
        $response = CaseLibrary::handleAutofill($serialNumber);
        return response()->json(['success' => true, 'message' => 'Completed', 'data' => $response]);
    }

}
