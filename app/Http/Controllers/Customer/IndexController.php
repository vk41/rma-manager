<?php

namespace App\Http\Controllers\Customer;

use App\Models\RmaCase;
use Illuminate\Http\Request;
use App\Libraries\CaseLibrary;
use App\Http\Controllers\Controller;

class IndexController extends Controller
{
    public function index() {
        $customer = auth()->user();
        $cases = RmaCase::select(['case_number', 'problem_summary','outcome','problem_description','status','rma_number','part_number', 'serial_number' ])
        ->where('customer_id', $customer->id)
        ->whereDate('created_at','>', now()->subYear())
        ->get();
        $counters = [
            'pending' => $cases->where('status', 'pending')->count(),
            'completed' => $cases->where('status', 'completed')->count(),
            'others' => $cases->whereNotIn('status', ['accepted','active', 'pending', 'completed'])->count(),
            'active' => $cases->whereIn('status', ['accepted','active'])->count()
        ];

        $recentCases = RmaCase::select(['case_number', 'serial_number','outcome','problem_summary','problem_description','status' ])
        ->where('customer_id', $customer->id)
        ->latest()
        ->take(5)
        ->get();

        $readyToCollect = RmaCase::select(['case_number', 'serial_number','part_number','outcome','problem_summary','problem_description','status' ])
        ->whereNotNull('completed_on')
        ->whereNull('delivered_on')
        ->where('customer_id', $customer->id)
        ->get();
        return view('customer.index', ['counters' => $counters, 'recentCases' =>$recentCases, 'readyToCollect' => $readyToCollect]);
    }

    public function search(Request $request) {
        $validatedData = $request->validate([
            'keyword' => ['min:4'],
        ]);
        $keyword = $request->keyword;
        $casesQuery = CaseLibrary::search($keyword);
        if($casesQuery->count() == 1) {
            $case = $casesQuery->first();
            return redirect()->route('customer.case.show', ['case_number' => $case->case_number])
            ->with(['info-message' => "There was only one case for the search query : <b>{$keyword}</b>."]);
        }
        // return $cases->count();

        return view('customer.search', ['keyword' => $keyword, 'cases' => $casesQuery->get()]);
    }
}
