<?php

namespace App\Http\Controllers\Customer;

use App\Models\Customer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;


class UserController extends Controller
{

    public function index()
    {
        return view('customer.user.profile');
    }

    public function update(Request $request)
    {
        $request->validate([
            'contact_name' => ['required', 'max:200'],
            'contact_number' => ['required'],
            'contact_email' => ['required', 'email'],
            'password' => ['nullable', 'required_with:password_confirmation', 'min:8', 'confirmed'],
        ]);

        $customer = Customer::find(auth()->user()->id);
        $customer->contact_name = $request->contact_name;
        $customer->contact_number = $request->contact_number;
        $customer->contact_email = $request->contact_email;
        if ($request->password) {
            $customer->password = Hash::make($request->password);
        }
        $customer->save();
        return redirect()->route('customer.user.profile')->with('success-message', 'Your profile updated.');
    }
    
    
}
