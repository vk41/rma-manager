<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RmaCaseRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function messages()
    {
        return [
            'problem_summary.required' => 'Problem Summary is required.',
            'problem_description.required' => 'Problem Description is required.',
            'password_protected.value.required' => 'Password protected field is required.',
            'backup_done.value.required' => 'Backup condition field is required.',
            'accessories.remarks.max:100' => 'Max length is 100 chars.',
        ];
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'part_number' => ['required', 'max:50'],
            'serial_number' => ['required', 'max:50'],
            'invoice_number' => ['required', 'max:50'],
            'invoice_date' => ['required'],
            'manufacturer' => ['required'],
            'category' => ['required'],
            'problem_summary' => ['required'],
            'problem_description' => ['required'],
            'password_protected.value' => ['required'],
            'backup_done.value' => ['required'],
            'accessories.remarks' => ['nullable', 'string', 'max:100',],
        ];
    }
}
