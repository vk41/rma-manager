<?php

namespace App\Libraries;

use App\Models\Customer;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class Helper{
    public static function sendCustomerMail(Customer $customer, $mailable) {
        if(!$customer->email) {
            Log::log('Mail not sent, Customer Not enabled '. $customer->email);
            return false;
        }
        Mail::to($customer->email)->send($mailable);
        if($customer->email == $customer->contact_email) {
            Mail::to($customer->contact_email)->send($mailable);
        }
    }
    public static function sendAdminMail($mailable) {
        Mail::to('rma@aquacom.com')->send($mailable);
    }

    public static function unslug($str) {
        return ucwords(str_replace('_', ' ', $str));
    }
}