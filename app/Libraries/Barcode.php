<?php 
namespace App\Libraries;

use Picqer\Barcode\BarcodeGeneratorPNG;
use Picqer\Barcode\BarcodeGeneratorHTML;

class Barcode {
    //namespace App\Libraries;
    public static function getBarcode($barcode) {
        $generator = new BarcodeGeneratorHTML();
        return $generator->getBarcode($barcode, 'C128');
    }
    
    public static function getBarcodePNG($barcode) {
        $generator = new BarcodeGeneratorPNG();
        return $generator->getBarcode($barcode, 'C128');
    }
}