<?php

namespace App\Libraries;

use Exception;
use Carbon\Carbon;
use App\Models\RmaCase;
use App\Models\Customer;
use App\Libraries\Helper;
use App\Models\CaseEvent;
use Illuminate\Http\Request;
use App\Models\CaseChecklist;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\RmaCaseRequest;

class CaseLibrary
{
    public static function getCompletedOutcomes($outcome = null)
    {
        $outcomes =  [
            'ready_to_collect' => 'Completed, Ready to collect.',
            'ready_to_ship' => 'Completed, Ready to ship.',
            'credit_issued' => 'Failed, Credit Issued.',
        ];
        if (!$outcome) {
            return $outcomes;
        }

        return isset($outcomes[$outcome]) ? $outcomes[$outcome] : Helper::unslug($outcome);
    }

    public static function getLocations()
    {
        return [
            'dubai' => 'Dubai',
            'newyork' => 'New York',
            'miami' => 'Miami',
        ];
    }


    public static function create(RmaCaseRequest $request, Customer $customer)
    {

        $caseData = $request->only(['part_number', 'serial_number', 'invoice_number', 'problem_summary', 'problem_description', 'manufacturer', 'category', 'product_description', 'invoice_number', 'invoice_date', 'remarks',]);
        $checklistData = $request->only(['password_protected', 'backup_done', 'accessories']);
        $caseData['customer_id'] = $customer->id;
        $caseData['case_number'] = static::generateCaseNumber($caseData);
        $rmaCase = RmaCase::create($caseData);

        $checklist = [];

        foreach ($checklistData as $name => $value) {
            if ($value == '') {
                continue;
            }
            $checklist[] = new CaseChecklist([
                'name' => $name,
                'value' => isset($value['value']) ? $value['value'] : [],
                'remarks' => isset($value['remarks']) ? $value['remarks'] : null,
                'created_by' => $request->user()->is_user ? $request->user()->id : 0
            ]);
        }
        $rmaCase->checkLists()->saveMany($checklist);
        $rmaCase->events()->save(
            new CaseEvent([
                'name' => 'created',
                'remarks' => 'RMA created',
                'created_by' => $request->user()->is_user ? $request->user()->id : 0
            ])
        );



        return $rmaCase;
    }

    public static function search($keyword)
    {
        $cases = RmaCase::query();
        $cases = $cases->where(function ($query) use ($keyword) {
            $query
                ->orWhere('case_number', 'LIKE', "%$keyword%")
                ->orWhere('rma_number', 'LIKE', "%$keyword%")
                ->orWhere('part_number', 'LIKE', "%$keyword%")
                ->orWhere('serial_number', 'LIKE', "%$keyword%");
        });
        $cases = auth()->user()->is_user ?  $cases : $cases->where('customer_id', auth()->user()->id);
        return $cases;
    }

    public static function checkIfExits(Request $request)
    {
        $rmacase = RmaCase::where('part_number', $request->part_number)
            ->where('serial_number', $request->serial_number)
            ->whereIn('status', ['pending', 'active', 'rejected'])
            ->first();
        return ($rmacase) ? $rmacase : false;
    }

    public static function generateCaseNumber($caseData)
    {

        $customer = Customer::where('id', $caseData['customer_id'])->first();
        $caseNumber = [
            $customer->code . date('mdY'),
            $customer->rmaCases()->whereDate('created_at', Carbon::today())->count() + 1

        ];
        return strtoupper(implode('-', $caseNumber));
    }

    public static function generateRmaNumber($caseData)
    {
        $customer = Customer::where('id', $caseData['customer_id'])->first();
        $caseNumber = [
            $customer->company->code . $customer->code . date('md'),
            $customer->salesman . ($customer->rmaCases()->whereDate('created_at', Carbon::today())->count() + 1)

        ];
        return strtoupper(implode('-', $caseNumber));
    }

    public static function getCountByStatus(array $statuses, Customer $customer = null)
    {
        $cases = RmaCase::select(['case_number', 'status', 'rma_number', 'part_number', 'serial_number', 'invoice_number', 'invoice_date', 'rma_issued_at', 'created_at'])
            ->whereIn('status', $statuses);
        if ($customer) {
            $cases = $cases->where('customer_id', $customer->id);
        }
        return $cases->count();
    }

    public static function statusHtml($status, $onlyColor = false)
    {
        $themColor = 'secondary';
        switch ($status) {
            case 'pending':
                $themColor = 'secondary';
                break;
            case 'accepted':
                $themColor = 'info';
                break;
            case 'active':
                $themColor = 'primary';
            case 'completed':
            case 'delivered':
                $themColor = 'success';
                break;
            case 'rejected':
                $themColor = 'danger';
                break;

            default:
                $themColor = 'secondary';
                break;
        }
        if ($onlyColor) {
            return $themColor;
        }
        return '<span class="badge badge-' . $themColor . '">' . ucfirst($status) . '</span>';
    }


    public static function getCaseByCaseNumber($caseNumber)
    {
        $user = auth()->user();

        if (auth()->user()->is_customer) {

            $case = RmaCase::with([
                'checkLists' => function ($query) {
                    if (!in_array('*', config('customer.visibility.checklists'))) {
                        $query->whereIn('name', config('customer.visibility.checklists'));
                    }
                },
                'events' => function ($query) {
                    if (!in_array('*', config('customer.visibility.events'))) {
                        $query->whereIn('name', config('customer.visibility.events'));
                    }
                },
                'customer'

            ])
                ->where('case_number', $caseNumber)
                ->where('customer_id', auth()->user()->id)
                ->first();
        } else {
            $case = RmaCase::with([
                'checkLists',
                'events.user',
                'customer',
                'vendorRmas' => function ($query) {
                    $query->orderBy('created_at', 'desc');
                }
            ])->where('case_number', $caseNumber)->first();
        }

        if ($case) {
            $case->events = static::formatEvents($case->events);
        }
        // dd($case->toArray());
        return $case;
    }

    public static function formatEvents(Collection $events)
    {
        $events = $events
            ->map(function ($event) {
                $event->date = $event->created_at->format('j M, Y');
                return $event;
            })
            ->sortByDesc(function ($obj, $key) {
                return $obj->created_at->timestamp;
            });
        return collect($events->values()->all());
        // dd($events);

    }

    public static function getChecklistFriendlyName($name)
    {
        $dictionary = [
            'password_protected' => 'Password Protected?',
            'backup_done' => 'Backup Done?',
            'accessories' => 'Original Accessories',
            'device_body' => 'Device Body',
            'screen' => 'Screen/Monitor condition',
            'latch' => 'Does the Lid open, close, and Latch properly?',
            'wifi' => 'Wifi',
            'keyboard_mouse' => 'Keyboard/Mouse',
            'battery' => 'Battery',
            'ram' => 'RAM'
        ];
        return (isset($dictionary[$name])) ? $dictionary[$name] : ucwords(str_replace('_', ' ', $name));
    }




    public static function acceptorReject(RmaCase $case, Request $request)
    {
        $validatedData = $request->validate([
            'status' => ['required'],
            'reason' => ($request->status == 'rejected') ? 'required' : '',
            'serial_number' => ['required', 'max:50'],
            'part_number' => ['required', 'max:50'],
            'manufacturer' => ['required', 'max:50'],
            'category' => ['required', 'max:50'],
            'product_description' => ['required', 'max:250'],
            'invoice_number' => ['required'],
            'invoice_date' => ['required'],
            'remarks' => ['max:250']
        ]);
        if ($request->status == 'accepted') {
            $case->rma_number = $request->rma_number;
            $case->rma_issued_at = Carbon::now();
        }
        $case->status = $request->status;
        $case->current_state = $request->status;
        $case->outcome = $request->reason;
        $case->serial_number = $request->serial_number;
        $case->part_number = $request->part_number;
        $case->manufacturer = $request->manufacturer;
        $case->category = $request->category;
        $case->product_description = $request->product_description;
        $case->invoice_number = $request->invoice_number;
        $case->invoice_date = $request->invoice_date;
        $case->remarks = $request->remarks;
        $case->save();
        ($request->status == 'accepted') ? $case->addEvent('case_accepted', 'RMA# ' . $case->rma_number . ' Issued') : $case->addEvent('case_rejected', 'Case rejected due to ' . $request->reason);

        return $case;
    }

    public static function receiveProduct(RmaCase $case, Request $request)
    {
        // dd($request->all());
        $messages = [
            'device_body.value.required' => 'Device body condition field is required.',
            'screen_condition.value.required' => 'Screen condition field is required.',
            'lid_condition.value.required' => 'Lid condition field is required.',
            'wifi.value.required' => 'Wifi condition field is required.',
            'keyboard.value.required' => 'Keyboard condition field is required.',
            'battery.value.required' => 'battery condition field is required.',
            'backup_done.value.required' => 'Backup condition field is required.',
            'password_protected.value.required' => 'Password protected field is required.',
            'accessories.value.required' => 'Accessories field is required.',
            'current_location.required' => 'Collected At field is required.',
        ];
        $validatedData = $request->validate([
            'device_body.value' => ['required'],
            'screen_condition.value' => ['required'],
            'lid_condition.value' => ['required'],
            'wifi.value' => ['required'],
            'keyboard.value' => ['required'],
            'battery.value' => ['required'],
            'backup_done.value' => ['required'],
            'password_protected.value' => ['required'],
            'accessories.value' => ['required'],
            'current_location' => ['required'],
        ], $messages);

        $case->status = 'active';
        $case->current_location = $request->current_location;
        $case->product_received_on = Carbon::now();
        $case->current_state = 'received';

        $case->save();
        foreach ($request->all() as $name => $value) {
            if (!in_array($name, ['device_body', 'screen_condition', 'lid_condition', 'wifi', 'keyboard', 'battery', 'backup_done', 'password_protected', 'accessories'])) {
                continue;
            }

            $case->checkLists()->updateOrCreate(
                ['name' => $name],
                [
                    'value' => $value['value'],
                    'remarks' => isset($value['remarks']) ? $value['remarks'] : null,
                    'created_by' => $request->user()->id
                ]
            );
        }
        $case->addEvent('product_received', 'Product Received at ' . ucfirst($request->current_location));
        return $case;
    }
    public static function changeLocation(RmaCase $case, Request $request)
    {
        // dd($request->all());
        $messages = [
            'current_location.required' => 'Location field is required.',
        ];
        $validatedData = $request->validate([
            'current_location' => ['required'],
        ], $messages);

        $oldLocation = $case->current_location;
        $case->current_location = $request->current_location;

        $case->save();
        $remarks = $request->input('current_location_remarks', null);
        $eventRemark = 'Product location changed from ' . ucfirst($oldLocation) . ' to ' . ucfirst($request->current_location);
        if ($remarks) {
            $eventRemark = $remarks . '. ' . $eventRemark;
        }
        // $remarks = implode('. ', [$remarks, ]);

        $case->addEvent('location_changed', $eventRemark);
        return $case;
    }

    public static function completion(RmaCase $case, Request $request)
    {
        // dd($request->all());
        $messages = [
            'current_location.required' => 'Location field is required.',
        ];
        $validatedData = $request->validate([
            'current_location' => ['required'],
            'outcome' => ['required'],
            'repair_summary' => ['required'],
        ], $messages);

        $oldLocation = $case->current_location;
        $case->status = 'completed';
        $case->current_location = $request->current_location;
        $case->outcome = $request->outcome;
        $case->repair_summary = $request->repair_summary;
        $case->completed_on = Carbon::now();

        $case->save();
        if ($oldLocation != $request->current_location) {
            $case->addEvent('location_changed', 'Product location changed from ' . ucfirst($oldLocation) . ' to ' . ucfirst($request->current_location));
        }
        $case->addEvent('completed', $request->repair_summary);

        return $case;
    }
    public static function delivery(RmaCase $case, Request $request)
    {
        // dd($request->all());       
        $validatedData = $request->validate([
            'remarks' => ['required']
        ]);

        $case->status = 'completed';
        $case->delivered_on = Carbon::now();
        $case->current_state = 'delivered';


        $case->save();
        $case->addEvent('delivered', $request->repair_summary);

        return $case;
    }
    public static function comment(RmaCase $case, Request $request)
    {
        // dd($request->all());       
        $validatedData = $request->validate([
            'comment' => ['required']
        ]);

        // $case->status = 'completed';   
        // $case->delivered_on = Carbon::now();   
        // $case->current_state = 'de/livered';   


        $case->save();
        $case->addEvent('comment', $request->comment);

        return $case;
    }

    public static function handleAutofill($serialNumber)
    {
        $faker = \Faker\Factory::create();
        $response = [
            'part_number' => $faker->bothify('#?#?#?#?#?'),
            'manufacturer' => $faker->bothify('#?#?#?#?#?'),
            'category' => $faker->bothify('#?#?#?#?#?'),
            'product_description' => $faker->sentence,
            'invoice_number' => $faker->bothify('#?#?#?#?#?'),
            'invoice_date' => $faker->date,
        ];
        // if(Auth::user()->is_user) {
        //     $response['customer'] = Customer::inRandomOrder()->first()->code;
        // }
        return $response;
    }

    public static function getCaseByUID($uid)
    {
        return  RmaCase::with(['customer'])
            ->where('uid', $uid)
            ->first();
    }
}
