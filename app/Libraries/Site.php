<?php

namespace App\Libraries;

use App\Models\Company;


class Site {
    public static $company = null;

    public static function getCompany() {
        if(static::$company == null) {
            $domain = parse_url(request()->root())['host'];
            if($domain == 'localhost') {
                $company = Company::first();
            }else {
                $company = Company::where('domain', 'LIKE', "%{$domain}%")->first();
            }
            static::setupCompany($company);
        }        
        return static::$company;
    }

    public static function setupCompany(Company $company) {
        static::$company = $company;
        return static::$company;
    }

    public static function getLogo() {
        return optional(static::getCompany())->logo;
    }

    public static function getId() {
        return optional(static::getCompany())->id;
    }

    public static function getCompanyName() {
        return optional(static::getCompany())->name;
    }

    public static function getCompanyWebsite() {
        return optional(static::getCompany())->website;
    }

    // public static function getCompanyTheme() {
    //     return static::getCompany()->theme;
    // }

    public static function isAdminPage() {
        // dd(request()->url());
        $url = parse_url(request()->url());
        if(!isset($url['path'])) {
            return false;
        }
        $path = explode('/', $url['path']);
        if($path[1] == 'admin') {
            return true;
        }
    }
}