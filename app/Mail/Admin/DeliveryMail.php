<?php

namespace App\Mail\Customer;

use App\Models\RmaCase;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class DeliveryMail extends Mailable
{
    use Queueable, SerializesModels;
    public $case;
    public $customer;
    protected $company;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(RmaCase $case)
    {
        $this->case = $case;
        $this->customer = $case->customer; 
        $this->company = $this->customer->company;         
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject("RMA# {$this->case->case_number} : Product delivered")
        ->markdown('emails.customer.delivery')
        ->with([
            'company' => $this->company
        ]);
    }
}
