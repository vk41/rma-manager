<?php

namespace App\Mail\Customer;

use App\Models\Customer;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class CredentialMail extends Mailable
{
    use Queueable, SerializesModels;
    public $customer;
    public $company;
    public $password;
    public $isAccountUpdate;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Customer $customer, $password, $isAccountUpdate = false)
    {
        $this->customer = $customer;
        $this->company = $customer->company;
        $this->password = $password;
        $this->isAccountUpdate = $isAccountUpdate;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('RMA manager credentials')
        ->markdown('emails.customer.credentials');
    }
}
