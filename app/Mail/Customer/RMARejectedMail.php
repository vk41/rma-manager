<?php

namespace App\Mail\Customer;

use App\Models\RmaCase;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class RMARejectedMail extends Mailable
{
    use Queueable, SerializesModels;
    public $case;
    public $customer;
    protected $company;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(RmaCase $case)
    {
        $this->case = $case;
        $this->customer = $case->customer; 
        $this->company = $this->customer->company;         
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject("Case# {$this->case->case_number} Rejected")
        ->markdown('emails.customer.rma_rejected')
        ->with([
            'company' => $this->company
        ]);
    }
}
