<?php

namespace App\Mail\Customer;

use App\Models\RmaCase;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class Completion extends Mailable
{
    use Queueable, SerializesModels;
    public $case;
    public $customer;
    protected $company;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(RmaCase $case)
    {
        $this->case = $case;
        $this->customer = $case->customer; 
        $this->company = $this->customer->company;         
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject("RMA# {$this->case->case_number} : RMA Completed")
        ->markdown('emails.customer.completion')
        ->with([
            'company' => $this->company
        ]);
    }
}
