<?php

namespace App\Mail\Customer;

use App\Models\Customer;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class PasswordResetMail extends Mailable
{
    use Queueable, SerializesModels;
    public $customer;
    public $token;
    public $company;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Customer $customer, $token)
    {
        $this->customer = $customer;
        $this->token = $token;
        $this->company = $customer->company;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Password reset request')
        ->markdown('emails.customer.passwordreset');
    }
}
