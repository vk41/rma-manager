<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customers', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('code');
            $table->string('email')->unique()->comment('Login Email');
            $table->string('password');
            $table->boolean('login_enabled')->default(false);
            $table->boolean('email_enabled')->default(false);
            $table->rememberToken();
            $table->string('location');
            $table->integer('company_id');
            $table->string('salesman');
            $table->string('contact_name');
            $table->string('contact_number')->nullable();
            $table->string('contact_email');
            $table->integer('rating')->default(1)->comment('1 to 10');
            $table->string('remarks')->nullable();
            $table->string('timezone')->default('America/New_York');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customers');
    }
}
