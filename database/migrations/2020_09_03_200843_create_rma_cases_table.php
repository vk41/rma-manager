<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRmaCasesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rma_cases', function (Blueprint $table) {
            $table->id();
            $table->uuid('uid');
            $table->string('case_number');
            $table->string('rma_number')->nullable();
            $table->integer('customer_id');
            $table->string('part_number');
            $table->string('manufacturer')->nullable();
            $table->string('category')->nullable();
            $table->string('product_description')->nullable();
            $table->string('serial_number')->nullable();
            $table->string('invoice_number');
            $table->date('invoice_date')->nullable();
            $table->text('problem_summary');
            $table->text('problem_description');
            $table->integer('item_qty')->default(1);
            $table->enum('status',['pending', 'accepted','active', 'completed', 'rejected', 'failed'])->default('pending');
            $table->string('current_state')->nullable();
            $table->string('outcome')->nullable();
            $table->boolean('serialized')->default(true);
            $table->string('repair_summary')->nullable();
            $table->string('remarks')->nullable();
            $table->string('current_location')->nullable();
            $table->dateTime('rma_issued_at')->nullable();
            $table->dateTime('product_received_on')->nullable();
            $table->dateTime('completed_on')->nullable();
            $table->string('completed_by')->nullable();
            $table->dateTime('delivered_on')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rma_cases');
    }
}
