<?php

use App\Models\Company;
use App\Models\Customer;
use Illuminate\Database\Seeder;

class CompanySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        $companies = [];
        $companies[] = ['name'=> 'Aquacom', 'code'=> 'AQ', 'email'=> 'rma@aquacom.com', 'logo'=>'aquacom.png', 'email_layout'=>'aquacom', 'domain'=>'http://rma.aquacom.com', 'website'=> 'www.aquacom.com'];
        $companies[] = ['name'=> 'Compusouk', 'code'=> 'CS','email'=> 'rma@compusouk.com', 'logo'=>'compusouk.png', 'email_layout'=>'compusouk', 'domain'=>'http://rma.compusouk.com', 'website'=> 'www.compusouk.com'];

        foreach($companies as $key => $company) {
            $faker = Faker\Factory::create();
            $company['address1'] = $faker->address;
            $company['address2'] = $faker->address;
            $company['phone'] = $faker->phoneNumber;
            $company['fax'] = $faker->phoneNumber;


            Company::create($company)
            ->each(function ($company) {
                $randCompany = rand(1, 3);
                $company->customers()->createMany(factory(Customer::class, $randCompany)->make()->toArray());
            });
        }
        
    }
}
