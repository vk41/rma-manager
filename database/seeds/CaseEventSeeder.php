<?php

use Illuminate\Database\Seeder;

class CaseEventSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Models\CaseEvent::class, 100)->create();
    }
}
