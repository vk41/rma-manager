<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Company;
use App\Models\Customer;
use Faker\Generator as Faker;
use Illuminate\Support\Facades\Hash;

$factory->define(Customer::class, function (Faker $faker) {
    return [
        'name' => $faker->company,
        'code' => strtoupper($faker->bothify('###???')),
        'email' => $faker->email,
        'password' => Hash::make('password'),
        'location' => $faker->city,
        'salesman' => strtoupper($faker->bothify('##')),
        'contact_name' => $faker->name,
        'contact_number' => $faker->phoneNumber,
        'contact_email' => $faker->email,
        'rating' => $faker->randomDigit,
        'remarks' => $faker->text(),
        'timezone' => $faker->timezone,
    ];
});
