<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\CaseEvent;
use Faker\Generator as Faker;

$factory->define(CaseEvent::class, function (Faker $faker) {
    return [
        'rma_case_id' => App\Models\RmaCase::inRandomOrder()->first()->id,
        'name' => $faker->randomElement(['created', 'accepted', 'product_received', 'completed', 'work_in_progress','comment', 'to_ship', 'shipped']),
        'remarks' => $faker->boolean(50) ? $faker->sentence() : null,
        'created_by' => App\Models\User::inRandomOrder()->first()->id,
        'created_at' => $faker->dateTimeBetween('-2 years')

    ];
});
