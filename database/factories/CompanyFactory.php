<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Company;
use Faker\Generator as Faker;

$factory->define(Company::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->email,
        'logo' => $faker->image(),
        'code' => strtoupper($faker->bothify('##')),
        'email_layout' => 'aquacom',
        'address1' => $faker->streetName,
        'address2' => $faker->streetAddress,
        'phone' => $faker->phoneNumber,
        'fax' => $faker->phoneNumber,
        'website' => 'www.'.$faker->domainName,
        'domain' => 'http://rma.'.$faker->domainName,
    ];
});
