<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\RmaCase;
use Faker\Generator as Faker;

$factory->define(RmaCase::class, function (Faker $faker) {
    return [
        'case_number' => strtoupper($faker->bothify('##???????##')),
        'rma_number' => $faker->boolean(50) ? strtoupper($faker->bothify('##???????##')) : null,
        'customer_id' => App\Models\Customer::inRandomOrder()->first()->id,
        'part_number' => $faker->bothify('#?#?#?#?#?'),
        'manufacturer' => $faker->company,
        'category' => $faker->company,
        'serial_number' => $faker->bothify('#?#?#?#?#?'),
        'invoice_number' => $faker->bothify('?????'),
        'invoice_date' => $faker->boolean(50) ? $faker->date : null,
        'product_received_on' => $faker->boolean(50) ? $faker->dateTimeBetween('-2 years') : null,
        'completed_on' => $faker->boolean(50) ? $faker->dateTimeBetween('-2 years') : null,
        'problem_summary' => $faker->randomElement(['DOA', 'Display Problem', 'System Slow', 'Others']),
        'problem_description' => $faker->sentence(),
        // 'item_qty' => ,
        'status' => function (array $case) use ($faker) {
            if($case['rma_number']) {
                return $faker->randomElement(['accepted','active', 'completed', 'failed' ]);
            }
            return $faker->randomElement(['pending', 'rejected']);
        },
        'current_state' => $faker->randomElement(['pending', 'accepted','active', 'completed', 'rejected', null]),
        'outcome' => $faker->boolean(50) ? $faker->sentence() : null,
        // 'serialized' => ,
        'repair_summary' => $faker->boolean(50) ? $faker->sentence() : null,
        'remarks' => $faker->boolean(50) ? $faker->sentence() : null,
        'current_location' => $faker->randomElement(['customer', 'dubai','vendor',  null]) ,
    ];
});
