window.ucfirst = function(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
};

window.statusHtml = function(status) {
    var themColor = "secondary";
    switch (status) {
        case "pending":
            themColor = "secondary";
            break;
        case "accepted":
            themColor = "info";
            break;
        case "active":
            themColor = "primary";
        case "completed":
        case "delivered":
            themColor = "success";
            break;
        case "rejected":
            themColor = "danger";
            break;

        default:
            themColor = "secondary";
            break;
    }
    status = status.charAt(0).toUpperCase() + status.slice(1);
    return `<span class="badge badge-${themColor}">${status}</span>`;
};

$(function() {
    $('[data-toggle="tooltip"]').tooltip();
});
