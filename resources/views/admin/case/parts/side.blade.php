 <!-- Profile Image -->
 <div class="card card-{{ App\Libraries\CaseLibrary::statusHtml($case->status, true) }} card-outline">
    <div class="ribbon-wrapper ribbon-lg" title="{{$case->status}}">
        <div class="ribbon bg-{{App\Libraries\CaseLibrary::statusHtml($case->status, true)}}">
            {{$case->status}}
        </div>
      </div>
    <div class="card-body box-profile">

        @if ($case->rma_number)
        <h3 class="profile-username text-center">RMA#: <a href="{{route('admin.case.show', ['case_number' => $case->case_number])}}">{{ $case->rma_number }}</a></h3>
        <p class="text-muted text-center">Case#: <a href="{{route('admin.case.show', ['case_number' => $case->case_number])}}">{{ $case->case_number }}</a></p>
        @else
        <h3 class="profile-username text-center">Case#: <a href="{{route('admin.case.show', ['case_number' => $case->case_number])}}">{{ $case->case_number }}</a></h3>
        @endif
        <p class="text-muted text-center">
            @if ($case->outcome)
                <div class="text-center"><b>Outcome:</b> {{ App\Libraries\CaseLibrary::getCompletedOutcomes($case->outcome) }}</div>
            @endif
        </p>
        <p class="text-muted text-center">
            <div class="text-center"><b>Customer:</b> {{ $case->customer->code }} - {{ $case->customer->name }}
                <br/><span class="text-warning">{!!$case->customer->showRatingStars()!!}</span>
            </div>
        </p>

        <ul class="list-group list-group-unbordered mb-3">
            <li class="list-group-item">
                <b>Part Number</b> <span class="float-right">{{ $case->part_number }}</span>
            </li>
            <li class="list-group-item">
                <b>Serial Number</b> <span class="float-right">{{ $case->serial_number }}</span>
            </li>
            <li class="list-group-item">
                <b>Brand</b> <span class="float-right">{{ $case->manufacturer }}</span>
            </li>
            @if($case->current_location)
            <li class="list-group-item">
                <b>Location</b> <span class="float-right">{{ $case->current_location }}</span>
            </li>
            @endif
        </ul>

        {{-- <a href="#"
            class="btn btn-primary btn-block"><b>Follow</b></a> --}}
    </div>
    <!-- /.card-body -->
</div>
<!-- /.card -->

<!-- About Me Box -->
<div class="card card-{{ App\Libraries\CaseLibrary::statusHtml($case->status, true) }}">
    <div class="card-header">
        <h3 class="card-title">Detail</h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
        <strong><i class="fas fa-book mr-1"></i> Problem</strong>

        <p class="text-muted">
            {{ $case->problem_summary }}
            @if ($case->problem_description) -
                {{ $case->problem_description }} @endif
        </p>

        <hr>

        <strong><i class="fas fa-map-marker-alt mr-1"></i> Invoice</strong>
        <p class="text-muted">
            {{ $case->invoice_number }}
            @if ($case->invoice_date) - Date: {{ $case->invoice_date }}
            @endif
        </p>

        <hr>
        <strong><i class="fas fa-pencil-alt mr-1"></i>Original Accessories
            @if ($case->checkLists->firstWhere('name', 'accessories'))
                @if (count($case->checkLists->firstWhere('name', 'accessories')->value) > 0)
                    <div class="float-right">
                        @foreach ($case->checkLists->firstWhere('name', 'accessories')->value as $item)
                            <span class="badge badge-primary">{{ strtoupper($item) }}</span>
                        @endforeach
                    </div>
                @else
                    @if (!$case->checkLists->firstWhere('name', 'accessories')->remarks)
                        <div class="float-right">
                            <span class="badge badge-danger">No Accessories</span>
                        </div>
                    @endif
                @endif
            @endif


        </strong>

        <p class="text-muted">
            @if ($case->checkLists->firstWhere('name', 'accessories'))
                <div>
                    {{ $case->checkLists->firstWhere('name', 'accessories')->remarks }}
                </div>
            @endif
        </p>


        <hr>

        <strong><i class="far fa-file-alt mr-1"></i> Condition</strong>

        <p class="text-muted">

            @if ($case->checkLists->firstWhere('name', 'password_protected'))
                <div>Password Protected? <span
                        class="float-right">{{ strtoupper($case->checkLists->firstWhere('name', 'password_protected')->value[0]) }}</span>
                </div>
            @endif
            @if ($case->checkLists->firstWhere('name', 'backup_done'))
                <div>Data Backup is done? <span
                        class="float-right">{{ strtoupper($case->checkLists->firstWhere('name', 'backup_done')->value[0]) }}</span>
                </div>
            @endif
        </p>
    </div>
    <!-- /.card-body -->
</div>
<!-- /.card -->