@extends('layouts.admin')
@section('title', 'Case# ' . $case->case_number . ' Collection')
@section('content')
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-3">
                    @include('admin.case.parts.side')                   
                </div>
                <!-- /.col -->
                <div class="col-md-9" id="app">
                    <form method="POST" action="{{route('admin.case.receive', ['case_number'=>$case->case_number])}}">
                        @csrf
                        <div class="card card-secondary">
                            <div class="card-header">
                                <h3 class="card-title">Checklist</h3>
                            </div>
                            <div class="card-body">
                                <div class="row border-bottom ">
                                    <label class="col-md-3 col-form-label">Device Body</label>
                                    <div class="col-md-3">
                                        <div class="icheck-primary ">
                                            <input type="checkbox" id="device-body-scratches" value="scratches" name="device_body[value][]" @if( in_array('scratches', old('device_body.value', [])) ) checked @endif/>
                                            <label for="device-body-scratches">Scratches</label>
                                        </div>
                                        <div class="icheck-primary ">
                                            <input type="checkbox" id="device-body-dent" value="dent" name="device_body[value][]" @if( in_array('dent', old('device_body.value', [])) ) checked @endif/>
                                            <label for="device-body-dent">Dent</label>
                                        </div>
                                        <div class="icheck-primary ">
                                            <input type="checkbox" id="device-body-broken" value="broken" name="device_body[value][]" @if( in_array('broken', old('device_body.value', [])) ) checked @endif/>
                                            <label for="device-body-broken">Broken</label>
                                        </div>
                                        <div class="icheck-primary ">
                                            <input type="checkbox" id="device-body-na" value="broken" name="device_body[value][]" @if( in_array('na', old('device_body.value', [])) ) checked @endif/>
                                            <label for="device-body-na">NA</label>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <input type="text" class="form-control" name="device_body[remarks]" id="device-body-remarks" placeholder="Remarks" value="{{ old('device_body.remarks') }}">
                                        </div>
                                    </div>
                                    <!-- /.card-body -->
                                </div>
                                <!-- /.row -->

                                <div class="row border-bottom pt-3">
                                    <label class="col-md-3 col-form-label">Screen/Monitor Condition</label>
                                    <div class="col-md-3">
                                        <div class="icheck-primary ">
                                            <input type="checkbox" id="screen-condition-scratches" value="scratches" name="screen_condition[value][]" @if( in_array('scratches', old('screen_condition.value', [])) ) checked @endif/>
                                            <label for="screen-condition-scratches">Scratches</label>
                                        </div>
                                        
                                        <div class="icheck-primary ">
                                            <input type="checkbox" id="screen-condition-broken" value="broken" name="screen_condition[value][]" @if( in_array('broken', old('screen_condition.value', [])) ) checked @endif/>
                                            <label for="screen-condition-broken">Broken</label>
                                        </div>
                                        <div class="icheck-primary ">
                                            <input type="checkbox" id="screen-condition-not-working" value="not_working" name="screen_condition[value][]" @if( in_array('not_working', old('screen_condition.value', [])) ) checked @endif/>
                                            <label for="screen-condition-not-working">Not Working</label>
                                        </div>
                                        <div class="icheck-primary ">
                                            <input type="checkbox" id="screen-condition-na" value="na" name="screen_condition[value][]" @if( in_array('na', old('screen_condition.value', [])) ) checked @endif/>
                                            <label for="screen-condition-na">NA</label>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <input type="text" class="form-control" name="screen_condition[remarks]" id="screen-condition-remarks" placeholder="Remarks" value="{{ old('screen_condition.remarks') }}">
                                        </div>
                                    </div>
                                    <!-- /.card-body -->
                                </div>
                                <!-- /.row -->

                                <div class="row border-bottom pt-3">
                                    <label class="col-md-3 col-form-label">Does the Lid work properly?</label>
                                    <div class="col-md-3">
                                        <div class="icheck-primary icheck-inline">
                                            <input type="radio" id="lid-condition-yes" value="yes" name="lid_condition[value][]" @if( in_array('yes', old('lid_condition.value', [])) ) checked @endif/>
                                            <label for="lid-condition-yes">Yes</label>
                                        </div>
                                        
                                        <div class="icheck-primary icheck-inline">
                                            <input type="radio" id="lid-condition-no" value="no" name="lid_condition[value][]" @if( in_array('no', old('lid_condition.value', [])) ) checked @endif/>
                                            <label for="lid-condition-no">No</label>
                                        </div>
                                        <div class="icheck-primary icheck-inline">
                                            <input type="radio" id="lid-condition-na" value="na" name="lid_condition[value][]" @if( in_array('na', old('lid_condition.value', [])) ) checked @endif/>
                                            <label for="lid-condition-na">NA</label>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <input type="text" class="form-control" name="lid_condition[remarks]" id="lid-condition-remarks" placeholder="Remarks" value="{{ old('lid_condition.remarks') }}">
                                        </div>
                                    </div>
                                    <!-- /.card-body -->
                                </div>
                                <!-- /.row -->

                                <div class="row border-bottom pt-3">
                                    <label class="col-md-3 col-form-label">WiFi Working?</label>
                                    <div class="col-md-3">
                                        <div class="icheck-primary icheck-inline">
                                            <input type="radio" id="wifi-yes" value="yes" name="wifi[value][]" @if( in_array('yes', old('wifi.value', [])) ) checked @endif/>
                                            <label for="wifi-yes">Yes</label>
                                        </div>
                                        
                                        <div class="icheck-primary icheck-inline">
                                            <input type="radio" id="wifi-no" value="no" name="wifi[value][]" @if( in_array('no', old('wifi.value', [])) ) checked @endif/>
                                            <label for="wifi-no">No</label>
                                        </div>
                                        <div class="icheck-primary icheck-inline">
                                            <input type="radio" id="wifi-na" value="na" name="wifi[value][]" @if( in_array('na', old('wifi.value', [])) ) checked @endif/>
                                            <label for="wifi-na">NA</label>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <input type="text" class="form-control" name="wifi[remarks]" id="wifi-remarks" placeholder="Remarks" value="{{ old('wifi.remarks') }}">
                                        </div>
                                    </div>
                                    <!-- /.card-body -->
                                </div>
                                <!-- /.row -->

                                <div class="row border-bottom pt-3">
                                    <label class="col-md-3 col-form-label">Keyboard Working?</label>
                                    <div class="col-md-3">
                                        <div class="icheck-primary icheck-inline">
                                            <input type="radio" id="keyboard-yes" value="yes" name="keyboard[value][]" @if( in_array('yes', old('keyboard.value', [])) ) checked @endif/>
                                            <label for="keyboard-yes">Yes</label>
                                        </div>
                                        
                                        <div class="icheck-primary icheck-inline">
                                            <input type="radio" id="keyboard-no" value="no" name="keyboard[value][]" @if( in_array('no', old('keyboard.value', [])) ) checked @endif/>
                                            <label for="keyboard-no">No</label>
                                        </div>
                                        <div class="icheck-primary icheck-inline">
                                            <input type="radio" id="keyboard-na" value="na" name="keyboard[value][]" @if( in_array('na', old('keyboard.value', [])) ) checked @endif/>
                                            <label for="keyboard-na">NA</label>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <input type="text" class="form-control" name="keyboard[remarks]" id="keyboard-remarks" placeholder="Remarks" value="{{ old('keyboard.remarks') }}">
                                        </div>
                                    </div>
                                    <!-- /.card-body -->
                                </div>
                                <!-- /.row -->

                                <div class="row border-bottom pt-3">
                                    <label class="col-md-3 col-form-label">Battery Working?</label>
                                    <div class="col-md-3">
                                        <div class="icheck-primary icheck-inline">
                                            <input type="radio" id="battery-yes" value="yes" name="battery[value][]" @if( in_array('yes', old('battery.value', [])) ) checked @endif/>
                                            <label for="battery-yes">Yes</label>
                                        </div>
                                        
                                        <div class="icheck-primary icheck-inline">
                                            <input type="radio" id="battery-no" value="no" name="battery[value][]" @if( in_array('no', old('battery.value', [])) ) checked @endif/>
                                            <label for="battery-no">No</label>
                                        </div>
                                        <div class="icheck-primary icheck-inline">
                                            <input type="radio" id="battery-na" value="na" name="battery[value][]" @if( in_array('na', old('battery.value', [])) ) checked @endif/>
                                            <label for="battery-na">NA</label>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <input type="text" class="form-control" name="battery[remarks]" id="battery-remarks" placeholder="Remarks" value="{{ old('battery.remarks') }}">
                                        </div>
                                    </div>
                                    <!-- /.card-body -->
                                </div>
                                <!-- /.row -->

                                <div class="row border-bottom pt-3">
                                    <label class="col-md-3 col-form-label">Data Backup is done?</label>
                                    <div class="col-md-3">
                                        <div class="icheck-primary icheck-inline">
                                            <input type="radio" id="backup-done-yes" value="yes" name="backup_done[value][]" @if( in_array('yes', old('backup_done.value', [])) ) checked @endif/>
                                            <label for="backup-done-yes">Yes</label>
                                        </div>
                                        
                                        <div class="icheck-primary icheck-inline">
                                            <input type="radio" id="backup-done-no" value="no" name="backup_done[value][]" @if( in_array('no', old('backup_done.value', [])) ) checked @endif/>
                                            <label for="backup-done-no">No</label>
                                        </div>
                                        <div class="icheck-primary icheck-inline">
                                            <input type="radio" id="backup-done-na" value="na" name="backup_done[value][]" @if( in_array('na', old('backup_done.value', [])) ) checked @endif/>
                                            <label for="backup-done-na">NA</label>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <input type="text" class="form-control" name="backup_done[remarks]" id="backup-done-remarks" placeholder="Remarks" value="{{ old('backup_done.remarks') }}">
                                        </div>
                                    </div>
                                    <!-- /.card-body -->
                                </div>
                                <!-- /.row -->
                                
                                <div class="row border-bottom pt-3">
                                    <label class="col-md-3 col-form-label">Password Protected?</label>
                                    <div class="col-md-3">
                                        <div class="icheck-primary icheck-inline">
                                            <input type="radio" id="password-protected-yes" value="yes" name="password_protected[value][]" @if( in_array('yes', old('password_protected.value', [])) ) checked @endif/>
                                            <label for="password-protected-yes">Yes</label>
                                        </div>
                                        
                                        <div class="icheck-primary icheck-inline">
                                            <input type="radio" id="password-protected-no" value="no" name="password_protected[value][]" @if( in_array('no', old('password_protected.value', [])) ) checked @endif/>
                                            <label for="password-protected-no">No</label>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <input type="text" class="form-control" name="password_protected[remarks]" id="password-protected-remarks" placeholder="Remarks" value="{{ old('password_protected.remarks') }}">
                                        </div>
                                    </div>
                                    <!-- /.card-body -->
                                </div>
                                <!-- /.row -->
                                
                                <div class="row border-bottom pt-3">
                                    <label class="col-md-3 col-form-label">Accessories</label>
                                    <div class="col-md-3">
                                        <div class="icheck-primary">
                                            <input type="checkbox" id="accessories-box" value="box" name="accessories[value][]"  @if( in_array('box', old('accessories.value', [])) ) checked @endif />
                                            <label for="accessories-box">Original Box</label>
                                          </div>
                                          <div class="icheck-primary">
                                            <input type="checkbox" id="accessories-adaptor" value="adaptor" name="accessories[value][]"  @if( in_array('adaptor', old('accessories.value', []))  ) checked @endif />
                                            <label for="accessories-adaptor">Original Power supply</label>
                                          </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <input type="text" class="form-control" name="accessories[remarks]" id="accessories-remarks" placeholder="Remarks" value="{{ old('accessories.remarks') }}">
                                        </div>
                                    </div>
                                    <!-- /.card-body -->
                                </div>
                                <!-- /.row -->
                                
                                <div class="row border-bottom py-3">
                                    <label class="col-md-3 col-form-label">Collected At</label>                                    
                                    <div class="col-md-9">
                                        @foreach (App\Libraries\CaseLibrary::getLocations() as $key=> $item)
                                        <div class="icheck-primary icheck-inline">
                                        <input type="radio" id="collected-{{$key}}" value="{{$key}}" name="current_location" @if( old('current_location') == $key) checked @endif/>
                                            <label for="collected-{{$key}}">{{$item}}</label>
                                        </div>
                                        @endforeach
                                        
                                        
                                        
                                    </div>
                                    <!-- /.card-body -->
                                </div>
                                <!-- /.row -->

                            </div>

                            <div class="overlay d-none">
                                <i class="fas fa-2x fa-sync-alt fa-spin"></i>
                            </div>

                        </div> <!-- /.row -->

                        <div class="row mb-3">
                            <div class="col-12">
                                <input type="submit" value="Receive" class="btn btn-success float-right">
                            </div>
                        </div>
                    </form>
                    
                    <!-- /.nav-tabs-custom -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->

    </section>
@endsection
@push('styles')
    <link rel="stylesheet" href="{{ asset('plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}">
@endpush