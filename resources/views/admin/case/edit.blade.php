@extends('layouts.admin')
@section('title', 'Accept Or Reject Case# '. $case->case_number)
@section('content')
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-3">
                    @include('admin.case.parts.side')                   
                </div>
                <!-- /.col -->
                <div class="col-md-9" id="app">
                    <form method="POST" action="{{route('admin.case.accept', ['case_number'=>$case->case_number])}}">
                        @csrf
                        <input type="hidden" name="status" value="accepted"/>
                        <input type="hidden" name="reason" value=""/>
                        <div class="card card-secondary">
                            <div class="card-header">
                                <h3 class="card-title">Case</h3>
                            </div>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="rma_number">Serial Number</label>
                                            <div class="input-group mb-3">
                                                <input type="text" class="form-control" name="serial_number" id="serial_number" placeholder="RMA Number" value="{{ old('serial_number', $case->serial_number) }}" required>
                                                <div class="input-group-append">
                                                    <button id="autofill_button" class="btn btn-outline-primary" type="button">Auto Fill</button>
                                                  </div>
                                              </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="part_number">Part Number</label>
                                            <input type="text" class="form-control" name="part_number" id="part_number" placeholder="Part Number" value="{{ old('part_number', $case->part_number) }}" required>
                                            <small class="form-text text-muted original-value"><b>Original: </b>{{$case->part_number}}</small>
                                        </div>
                                        <div class="form-group">
                                            <label for="manufacturer">Manufacturer</label>
                                            <input type="text" class="form-control" name="manufacturer" id="manufacturer" placeholder="Manufacturer" value="{{ old('manufacturer', $case->manufacturer) }}" required>
                                            <small class="form-text text-muted original-value"><b>Original: </b>{{$case->manufacturer}}</small>
                                        </div>
                                        <div class="form-group">
                                            <label for="category">Category</label>
                                            <input type="text" class="form-control" name="category" id="category" placeholder="Category" value="{{ old('category', $case->category) }}" required>
                                            <small class="form-text text-muted original-value"><b>Original: </b>{{$case->category}}</small>
                                        </div>
                                        <div class="form-group">
                                            <label for="product_description">Product Description</label>
                                            <input type="text" class="form-control" name="product_description" id="product_description" placeholder="Product Description" value="{{ old('product_description', $case->product_description) }}" required>
                                            <small class="form-text text-muted original-value"><b>Original: </b>{{$case->product_description}}</small>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="rma_number">RMA Number</label>
                                            <input type="text" class="form-control" name="rma_number" id="rma_number" placeholder="RMA Number" value="{{ old('rma_number', App\Libraries\CaseLibrary::generateRmaNumber($case)) }}">
                                        </div>
                                        <div class="form-group">
                                            <label for="invoice_number">Invoice Number</label>
                                            <input type="text" class="form-control" name="invoice_number" id="invoice_number" placeholder="Invoice Number" value="{{ old('invoice_number', $case->invoice_number) }}" required>
                                            <small class="form-text text-muted original-value"><b>Original: </b>{{$case->invoice_number}}</small>
                                        </div>
                                        <div class="form-group">
                                            <label for="invoice_date">Invoice Date</label>
                                            <input type="text" class="form-control" name="invoice_date" id="invoice_date" placeholder="Invoice Date" value="{{ old('invoice_date', $case->invoice_date) }}" required>
                                            <small class="form-text text-muted original-value"><b>Original: </b>{{$case->invoice_date}}</small>
                                        </div>
                                        <div class="form-group">
                                            <label for="remarks">Remarks</label>
                                            <textarea class="form-control" id="remarks" rows="2" name="remarks">{{ old('remarks', $case->remarks) }}</textarea>
                                            {{-- <input type="text" class="form-control" name="invoice_date" id="invoice_date" placeholder="Invoice Date" value="{{ old('invoice_date', $case->invoice_date) }}" required> --}}
                                        </div>
                                        
                                    </div>
                                    <!-- /.card-body -->
                                </div>
                            </div>

                            <div class="overlay d-none">
                                <i class="fas fa-2x fa-sync-alt fa-spin"></i>
                            </div>

                        </div> <!-- /.row -->

                        <div class="row">
                            <div class="col-12">
                                <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#reject-model">Reject</button>
                                <input type="submit" value="Accept" class="btn btn-success float-right">
                            </div>
                        </div>
                    </form>
                    
                    <!-- /.nav-tabs-custom -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->


          <div class="modal fade" id="reject-model" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <form id="rejection-form">
                    <div class="modal-header">
                        <h4 class="modal-title" id="myModalLabel">Rejection Reason</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="rejection-reason">Reason</label>
                            <textarea class="form-control" id="rejection-reason" rows="2" name="rejection_reason" required></textarea>
                        </div>
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                      <button type="subject" class="btn btn-danger">Reject</button>
                    </div>
                </form>
                
              </div>
            </div>
          </div>

    </section>
@endsection
@push('styles')
    <link rel="stylesheet" href="{{ asset('plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}">
@endpush
@push('scripts')
<script src="{{ asset('plugins/vue/vue.runtime.min.js') }}"></script>
    <script>

// var app = new Vue({
//           el: '#app'
// });

        $(function() {

            $('#rejection-form').on('submit', function(event) {
                event.preventDefault();
                $('#app').find('input[name="reason"]').val($('#rejection-reason').val());
                $('#app').find('input[name="status"]').val('rejected');                
                $('#app').find('form').submit();                                
            });

            $('#autofill_button').on('click', function(event) {                
                event.preventDefault();
                handleLoader();
                axios.post('{{route("admin.case.autofill")}}',  {
                    serial_number: $('#serial_number').val()
                })
                .then(function(response) {
                    handleAutoFill(response.data.data);
                    handleLoader(false);
                    toastr.success('Data filled automatically.');
                })
                .catch(function(err) {
                    handleLoader(false);
                    console.log(err, err.response);
                    toastr.error('Autofill failed.');
                });
            });
        });

        function handleAutoFill(data) {
                var fields = Object.keys(data);
                Object.keys(data).forEach(function(fieldName) {
                    var $element = $('#app').find('#'+fieldName);
                    var newValue = data[fieldName];
                    // console.log(newValue);
                    var originalValue = $element.val();
                    // console.log('originalValue', originalValue);
                    $element.val(newValue);
                    $element.closest('.form-group').addClass('value-changed');
                    // console.log($element);
                });
            }
        function handleLoader(show = true) {
            if(show) {
                $('#app').find('.overlay').removeClass('d-none');
            }else {
                $('#app').find('.overlay').addClass('d-none');
            }
        }
    </script>
@endpush
