@extends('layouts.admin')
@section('title', 'New Case')
@section('content')
    <!-- Main content -->
    <section class="content mb-2">
        <form method="POST" action="{{ route('admin.case.store') }}">
            @csrf
            {{-- <div class="row">
                <div class="col-md-12">
                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title">Customer</h3>
                        </div>
                        <div class="card-body">
                            <div class="form-group">
                                <select type="text" id="customer_selector" name="customer" class="form-control"
                                    value="{{ old('customer') }}" required>
                                  </select>
                            </div>
                            <!-- /.card-body -->
                        </div>
                    </div>
                </div>
            </div> --}}
            <div class="row">
                <div class="col-md-6">
                    <div class="card card-primary" id="app">
                        <div class="card-header">
                            <h3 class="card-title">Product</h3>
                        </div>
                        <div class="card-body">
                            {{-- <div class="form-group">
                                <label for="serial_number">Serial Number</label>
                                <div class="input-group">
                                  <input type="text" autocomplete="off" class="form-control" id="serial_number" name="serial_number" value="{{ old('serial_number') }}">
                                  <div class="input-group-append">
                                    <button id="autofill_button" class="btn btn-outline-primary" type="button">Auto Fill</button>
                                  </div>
                                </div>
                              </div>

                            <div class="form-group">
                                <label for="part_number">Part Number</label>
                                <input type="text" id="part_number" autocomplete="off" name="part_number" class="form-control"
                                    value="{{ old('part_number') }}" required>
                            </div> --}}

                            
                            
                            <div class="form-group">
                                <div class="form-row">
                                    <div class="col">
                                        <label for="serial_number">Serial Number</label>
                                        <div class="input-group">
                                            <input type="text" autocomplete="off" class="form-control" id="serial_number" name="serial_number" value="{{ old('serial_number') }}">
                                            <div class="input-group-append" required>
                                            <button id="autofill_button" class="btn btn-outline-primary autofill-button" type="button">Auto Fill</button>
                                            </div>
                                      </div>
                                  </div>
                                  <div class="col">
                                    <label for="part_number">Part Number</label>
                                    <div class="input-group">
                                        <input type="text" autocomplete="off" class="form-control" id="part_number" name="part_number" value="{{ old('part_number') }}">
                                        <div class="input-group-append" required>
                                        <button id="autofill_button" class="btn btn-outline-primary autofill-button" type="button">Auto Fill</button>
                                        </div>
                                  </div>
                                  </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="form-row">
                                    <div class="col">
                                    <label for="invoice_number">Invoice</label>
                                    <input type="text" id="invoice_number" autocomplete="off" name="invoice_number" class="form-control"
                                        value="{{ old('invoice_number') }}" placeholder="Invoice Number" required>
                                  </div>
                                  <div class="col">
                                    <label for="invoice_date">Invoice Date</label>
                                    <input type="text" id="invoice_date" placeholder="Invoice Date" name="invoice_date" class="form-control"
                                      value="{{ old('invoice_date') }}" required>
                                  </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="form-row">
                                    <div class="col">
                                        <label for="manufacturer">Manufacturer</label>
                                        <input type="text" id="manufacturer" placeholder="Manufacturer" name="manufacturer" class="form-control"
                                          value="{{ old('manufacturer') }}" required>
                                      </div>
                                    <div class="col">
                                        <label for="category">Category</label>
                                        <input type="text" id="category" autocomplete="off" name="category" class="form-control"
                                        value="{{ old('category') }}" placeholder="Cateogory" required>
                                  </div>
                                  
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="category">Product Description</label>
                                <input type="text" id="product_description" placeholder="Product Description" name="product_description" class="form-control"
                                          value="{{ old('product_description') }}" required>
                            </div>
                            <div class="form-group">
                                <label for="category">Customer</label>
                                <input type="text" id="customer" placeholder="Customer" name="customer" class="form-control" autocomplete="off"
                                          value="{{ old('customer') }}" required>
                            </div>

                            <div class="form-group">
                                <label for="inputStatus">Problem</label>
                                <select class="form-control" name="problem_summary" required>
                                    <option selected disabled>Select one</option>
                                    <option @if (old('problem_summary') == 'DOA') selected
                                        @endif >DOA</option>
                                    <option @if (old('problem_summary') == 'Display Problem')
                                        selected @endif>Display Problem</option>
                                    <option @if (old('problem_summary') == 'System Slow')
                                        selected @endif>System Slow</option>
                                    <option @if (old('problem_summary') == 'Others') selected
                                        @endif>Others</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="inputDescription">Problem Description</label>
                                <textarea id="inputDescription" name="problem_description" class="form-control"
                                    rows="2">{{ old('problem_description') }}</textarea>
                            </div>
                        </div>
                        <!-- /.card-body -->
                        <div class="overlay d-none">
                            <i class="fas fa-2x fa-sync-alt fa-spin"></i>
                        </div>
                    </div>
                    <!-- /.card -->
                </div>
                <div class="col-md-6">
                    <div class="card card-secondary">
                        <div class="card-header">
                            <h3 class="card-title">Condition</h3>
                        </div>
                        <div class="card-body">
                            <div class="form-group">
                                <legend class="col-form-label  pt-0"><b>Password Protected?</b></legend>
                                <div class="icheck-primary icheck-inline">
                                    <input type="radio" id="password-protected-yes" value="yes"
                                        name="password_protected[value][]" @if (old('password_protected.value.0') == 'yes') checked @endif required/>
                                    <label for="password-protected-yes">Yes</label>
                                </div>
                                <div class="icheck-primary icheck-inline">
                                    <input type="radio" id="password-protected-no" value="no"
                                        name="password_protected[value][]" @if (old('password_protected.value.0') == 'no') checked @endif required/>
                                    <label for="password-protected-no">No</label>
                                </div>
                                <small id="passwordHelpBlock" class="form-text text-danger">
                                    Please provide password while droping the product, delays will occur otherwise.
                                </small>
                            </div>
                            <div class="form-group">
                                <legend class="col-form-label  pt-0"><b>Data Backup is done?</b></legend>
                                <div class="icheck-primary icheck-inline">
                                    <input type="radio" id="backup-done-yes" value="yes" name="backup_done[value][]"
                                        @if (old('backup_done.value.0') == 'yes') checked
                                    @endif required/>
                                    <label for="backup-done-yes">Yes</label>
                                </div>
                                <div class="icheck-primary icheck-inline">
                                    <input type="radio" id="backup-done-no" value="no" name="backup_done[value][]" @if (old('backup_done.value.0') == 'no') checked  @endif required/>
                                    <label for="backup-done-no">No</label>
                                </div>
                                <div class="icheck-primary icheck-inline">
                                    <input type="radio" id="backup-done-na" value="na" name="backup_done[value][]" @if (old('backup_done.value.0') == 'na') checked @endif required/>
                                    <label for="backup-done-na">NA</label>
                                </div>
                                <small id="passwordHelpBlock" class="form-text text-danger">
                                    Data is not covered under warranty.
                                </small>
                            </div>
                            <div class="form-group">
                                <legend class="col-form-label  pt-0"><b>Accessories</b></legend>
                                <div class="icheck-primary">
                                    <input type="checkbox" id="acc-box" value="box" name="accessories[value][]" @if (in_array('box', old('accessories.value', []))) checked
                                    @endif />
                                    <label for="acc-box">Original Box</label>
                                </div>
                                <div class="icheck-primary">
                                    <input type="checkbox" id="acc-adaptor" value="adaptor" name="accessories[value][]" @if (in_array('adaptor', old('accessories.value', []))) checked  @endif />
                                    <label for="acc-adaptor">Original Power supply</label>
                                </div>
                                <div class="">
                                    <label for="acc-adaptor">Other Accessories</label>
                                    <input type="text" autocomplete="off" id="inputName" name="accessories[remarks]" class="form-control"
                                        value="{{ old('accessories.remarks') }}">
                                </div>
                                <small id="passwordHelpBlock" class="form-text text-danger">
                                    Original accessories are mandatory.
                                </small>
                            </div>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <a href="{{ url('/') }}" class="btn btn-secondary">Cancel</a>
                    <input type="submit" value="Create Case" class="btn btn-success float-right">
                </div>
            </div>
        </form>
    </section>
@endsection
@push('styles')
    <link rel="stylesheet" href="{{ asset('plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/select2/css/select2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css') }}">
@endpush
@push('scripts')
    <script src="{{ asset('plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('plugins/inputmask/min/jquery.inputmask.bundle.min.js') }}"></script>
    <script src="{{ asset('plugins/select2/js/select2.min.js') }}"></script>
    <script>
        $(function() {
          $('#invoice_date').datepicker({
              format: 'yyyy-mm-dd',
              endDate: '+0d'
          });

        //   $('#customer').select2({
        //       theme: 'bootstrap4',
        //       placeholder: "Select a company",
        //       ajax: {
        //           url: '{{ route('admin.customers.autocomplete') }}',
        //           dataType: 'json'
        //       }
        //   });

          $('.autofill-button').on('click', function(event) {                
                event.preventDefault();
                var inputField = $(event.currentTarget).closest('.input-group').find('input');
                $('.autofill-button').closest('.input-group').find('input').removeClass('is-valid is-invalid')
                if(inputField.val() == '') {
                    inputField.addClass('is-invalid');
                    toastr.error('Value cannot be empty.');
                    return ;
                }else{
                    inputField.addClass('is-valid');
                }

                var searchParams = {
                    field: inputField.attr('name'),
                    value: inputField.val()
                };
                handleLoader();
                axios.post('{{route("admin.case.autofill")}}',  searchParams)
                .then(function(response) {
                    handleAutoFill(response.data.data);
                    handleLoader(false);
                    toastr.success('Data filled automatically.');
                })
                .catch(function(err) {
                    handleLoader(false);
                    console.log(err, err.response);
                    toastr.error('Autofill failed.');
                });
            });
            
        });

        function handleAutoFill(data, requestField) {
                var fields = Object.keys(data);
                Object.keys(data).forEach(function(fieldName) {
                    if(requestField != fieldName) {
                        var $element = $('#app').find('#'+fieldName);
                        var newValue = data[fieldName];
                        $element.val(newValue);
                    }
                    

                });
            }
        function handleLoader(show = true) {
            if(show) {
                $('#app').find('.overlay').removeClass('d-none');
            }else {
                $('#app').find('.overlay').addClass('d-none');
            }
        }

    </script>
@endpush
