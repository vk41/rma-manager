@extends('layouts.admin')
@section('title', 'Case# '.$case->case_number.' completion')
@section('content')
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-3">
                    @include('admin.case.parts.side')                   
                </div>
                <!-- /.col -->
                <div class="col-md-9" id="app">
                    <form method="POST" action="{{route('admin.case.vendor', ['case_number'=>$case->case_number])}}">
                        @csrf
                        <input name="current_location" value="vendor" type="hidden"/>
                        <div class="card card-secondary">
                            <div class="card-header">
                                <h3 class="card-title">Vendor Information</h3>
                            </div>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="part_number">Vendor Name</label>
                                            <input class="form-control" name="vendor_name" id="vendor_name" placeholder="Vendor Name" value="{{ old('vendor_name') }}" required />
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="part_number">Vendor Ref No</label>
                                            <input class="form-control" name="ref_number" id="ref_number" placeholder="Vendor Ref No" value="{{ old('ref_number') }}" required />
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="part_number">Email/Username Used</label>
                                            <input class="form-control" name="end_user" id="end_user" placeholder="Email/Username Used" value="{{ old('end_user') }}" required />
                                        </div>
                                    </div>  
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="repair_summary">Remarks <small class="text-muted">Such as chat transcript.</small></label>
                                            <textarea id="remarks" name="remarks" class="form-control" rows="5">{{ old('remarks') }}</textarea>
                                            
                                        </div>
                                    </div>
                                </div>
                                
                            </div>

                            <div class="overlay d-none">
                                <i class="fas fa-2x fa-sync-alt fa-spin"></i>
                            </div>

                        </div> <!-- /.row -->

                        <div class="row">
                            <div class="col-12">
                                {{-- <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#reject-model">Reject</button> --}}
                                <input type="submit" value="Save" class="btn btn-success float-right">
                            </div>
                        </div>
                    </form>
                    
                    <!-- /.nav-tabs-custom -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->



    </section>
@endsection
@push('styles')
    {{-- <link rel="stylesheet" href="{{ asset('plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}"> --}}
    <link rel="stylesheet" href="{{ asset('plugins/summernote/summernote-bs4.css') }}">
@endpush
@push('scripts')
<script src="{{ asset('plugins/summernote/summernote-bs4.min.js') }}"></script>
    <script>

// var app = new Vue({
//           el: '#app'
// });

        $(function() {
            $('#remarks').summernote({
                height: 200
            })
            
        });

        
    </script>
@endpush
