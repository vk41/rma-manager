@extends('layouts.admin')
@section('title', 'Case# ' . $case->case_number . ' Detail')
@section('content')
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-3">
                    @include('admin.case.parts.side')                   
                </div>
                <!-- /.col -->
                <div class="col-md-9">
                    <div class="card">
                        <div class="card-header p-2">
                            <ul class="nav nav-pills">
                                <li class="nav-item"><a class="nav-link active" href="#timeline"
                                        data-toggle="tab">Timeline</a></li>
                                <li class="nav-item"><a class="nav-link" href="#diagnosis" data-toggle="tab">Diagnosis</a>
                                <li class="nav-item"><a class="nav-link" href="#product" data-toggle="tab">Product</a>
                                <li class="nav-item ml-2">
                                    <a href="{{ route('admin.case.print', ['case_number' => $case->case_number]) }}"
                                        target="_blank" class="btn btn-default"><i class="nav-icon fas fa-print"></i>
                                        Print</a>
                                </li>
                                @if($case->status != 'completed' )
                                {{-- <li class="nav-item ml-2">
                                    <a href="{{ route('admin.case.action-show', ['case_number' => $case->case_number, 'type' => 'edit']) }}"
                                         class="btn btn-warning"><i class="nav-icon fas fa-edit"></i>
                                        Edit</a>
                                </li> --}}
                                @endif
                                @if($case->status == 'pending' || $case->status == 'rejected' )
                                <li class="nav-item ml-2">
                                    <a href="{{ route('admin.case.action-show', ['case_number' => $case->case_number, 'type' => 'accept']) }}" class="btn btn-info"><i class="nav-icon fas fa-check"></i>
                                        Accept / Reject</a>
                                </li>
                                @endif
                                @if($case->status == 'accepted' && !$case->product_received_on)
                                <li class="nav-item ml-2">
                                    <a href="{{ route('admin.case.action-show', ['case_number' => $case->case_number, 'type' => 'collection']) }}"
                                         class="btn btn-info"><i class="nav-icon fas fa-dolly"></i>
                                        Collection</a>
                                </li>
                                @endif
                                @if($case->status == 'active' && !$case->completed_on)
                                <li class="nav-item ml-2">
                                    <a href="{{ route('admin.case.action-show', ['case_number' => $case->case_number, 'type' => 'completion']) }}"
                                         class="btn btn-success"><i class="nav-icon fas fa-check"></i>
                                        Completed</a>
                                </li>
                                <li class="nav-item ml-2">
                                    <a href="#" data-toggle="modal" data-target="#locationChangeModel" 
                                         class="btn btn-secondary"><i class="nav-icon fas fa-map"></i>
                                        Change Location</a>
                                </li>
                                @endif
                                @if($case->status == 'completed' && !$case->delivered_on)
                                <li class="nav-item ml-2">
                                    <a href="#" data-toggle="modal" data-target="#deliveryModal"
                                         class="btn btn-info"><i class="nav-icon fas fa-shipping-fast"></i>
                                        Delivery</a>
                                </li>
                                @endif
                                @if($case->vendorRmas->count())
                                <li class="nav-item ml-2">
                                    <a href="#" data-toggle="modal" data-target="#vendorHistoryModal"
                                         class="btn btn-info"><i class="nav-icon fas fa-building"></i>
                                        Vendor History</a>
                                </li>
                                @endif
                            </ul>
                        </div><!-- /.card-header -->
                        <div class="card-body">
                            <div class="tab-content">
                                <!-- /.tab-pane -->
                                <div class="tab-pane active" id="timeline">

                                    <form method="POST" action="{{route('admin.case.comment', ['case_number'=>$case->case_number])}}">
                                        @csrf
                                        <div class="input-group mb-3">
                                            <input type="text" placeholder="Add Comment" name="comment" value="{{old('comment')}}" class="form-control" placeholder="" aria-label="" >
                                            <div class="input-group-append">
                                                <button class="btn btn-outline-primary" type="submit">Save</button>
                                              </div>
                                          </div>
                                        </form>

                                    <!-- The timeline -->
                                    <div class="timeline timeline-inverse">
                                        <!-- timeline time label -->
                                        <div class="time-label">
                                            <span class="bg-info">
                                                {{ Carbon\Carbon::now()->format('j M, Y') }}
                                            </span>
                                        </div>
                                        <!-- /.timeline-label -->
                                        @foreach ($case->events->groupBy('date') as $date => $events)
                                            @foreach ($events as $event)
                                                <!-- timeline item -->
                                                {{-- {{$event->name}} --}}
                                                <div>
                                                    @switch($event->name)
                                                        @case('completed')
                                                            <i class="fas fa-check bg-success"></i>
                                                            @break

                                                        @case('location_changed')
                                                            <i class="fas fa-truck bg-info"></i>
                                                            @break

                                                        @case('comment')
                                                            <i class="fas fa-comments bg-warning"></i>
                                                            @break
                                                        @default
                                                        <i class="fas fa-calendar-check bg-primary"></i>
                                                    @endswitch
                                                    

                                                    <div class="timeline-item">
                                                        <span class="time">
                                                            <label data-toggle="tooltip" data-placement="top" title="Tooltip on top">({{\Carbon\Carbon::parse($case->product_received_on)->diffInDays(\Carbon\Carbon::now())}} Days)</label>
                                                            <i class="far fa-clock"></i>
                                                            {{ $event->created_at->format('g:i a') }}
                                                        </span>

                                                        <h3 class="timeline-header">
                                                            <a href="#">
                                                                @if ($event->created_by == 0)
                                                                    {{ $case->customer->name }}
                                                                @else
                                                                {{$event->user->name}}
                                                                @endif
                                                            </a>
                                                            {{ App\Libraries\Helper::unslug($event->name) }}
                                                        </h3>
                                                        @if ($event->remarks)
                                                            <div class="timeline-body">{{ $event->remarks }}</div>
                                                        @endif
                                                    </div>
                                                </div>
                                                <!-- END timeline item -->
                                            @endforeach
                                            <div class="time-label">
                                                <span class="bg-secondary">
                                                    {{ $date }}
                                                </span>
                                            </div>

                                        @endforeach


                                        <div>
                                            <i class="far fa-clock bg-gray"></i>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.tab-pane -->

                                <div class="tab-pane" id="diagnosis">
                                    {{-- <div class="text-right mb-3">
                                        
                                    </div> --}}
                                    <div class="card">
                                        <div class="card-header">
                                            Checklist
                                        </div>
                                        <div class="card-body">
                                            <ul class="list-group list-group-unbordered mb-3">
                                                @foreach ($case->checklists as $checklist)
                                                    <li class="list-group-item">
                                                        <b>{{ App\Libraries\CaseLibrary::getChecklistFriendlyName($checklist->name) }}</b>
                                                        @if ($checklist->remarks)
                                                            <small>{{ $checklist->remarks }}</small>
                                                        @endif
                                                        <span class="float-right">
                                                            @if (count($checklist->value) <= 0)
                                                                <span class="badge badge-warning">NA</span>
                                                            @endif
                                                            @foreach ($checklist->value as $item)
                                                                <span
                                                                    class="badge badge-secondary">{{ strtoupper(App\Libraries\Helper::unslug($item)) }}</span>
                                                            @endforeach

                                                        </span>
                                                    </li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    </div>

                                    <div class="card">
                                        <div class="card-header">
                                            Repair Summary
                                        </div>
                                        <div class="card-body">
                                            {{ $case->repair_summary }}
                                        </div>
                                    </div>
                                </div>
                                <!-- /.tab-pane -->
                                <div class="tab-pane" id="product">

                                    <div class="card">
                                        <div class="card-header">
                                            Checklist
                                        </div>
                                        <div class="card-body">
                                            <!-- Table row -->
                                            <div class="row">
                                                <div class="col-12 table-responsive">
                                                    <table class="table table-bordered table-sm">
                                                        <tbody>
                                                            <tr>
                                                                <th width="30%">Part Number</th>
                                                                <td>{{ $case->part_number }}</td>
                                                                <th width="30%">Serial Number</th>
                                                                <td>{{ $case->serial_number }}</td>
                                                            </tr>
                                                            <tr>
                                                                <th>Invoice Number</th>
                                                                <td>{{ $case->invoice_number }}</td>
                                                                <th>Invoice Date</th>
                                                                <td>{{ $case->invoice_date }}</td>
                                                            </tr>
                                                            <tr>
                                                                <th>Manufacturer</th>
                                                                <td>{{ $case->manufacturer }}</td>
                                                                <th>Category</th>
                                                                <td>{{ $case->category }}</td>
                                                            </tr>
                                                            <tr>
                                                                <th>Spec</th>
                                                                <td colspan="3">{{ $case->product_description }}</td>
                                                               
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <!-- /.col -->
                                            </div>
                                            <!-- /.row -->
                                        </div>
                                    </div>
                                </div>
                                <!-- /.tab-pane -->
                            </div>
                            <!-- /.tab-content -->
                        </div><!-- /.card-body -->
                    </div>
                    <!-- /.nav-tabs-custom -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </section>
@if($case->vendorRmas->count())
<div class="modal fade" id="vendorHistoryModal" tabindex="-1" role="dialog" aria-labelledby="vendorHistoryModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="deliveryModalLabel">Vendor RMAs</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            @foreach ($case->vendorRmas as $vendorRma)
            <div class="card" >
                <div class="card-body">
                   <h5 >{{$vendorRma->vendor_name}}</h5>
                  <h5> Ref#: {{$vendorRma->ref_number}}</h5>
                  <h6 class="card-subtitle mb-2 text-muted">{{$vendorRma->end_user}} <small class="float-right">{{$vendorRma->created_at}}</small></h6>
                  {{-- <h6 class="card-subtitle mb-2 text-muted float-right"></h6> --}}
                  @if($vendorRma->remarks) 
                  <hr/>
                  <p class="card-text">{!!$vendorRma->remarks!!}</p>
                  @endif
                </div>
              </div>
            @endforeach
        </div>
    </div>
  </div>
</div>
@endif

@if($case->status == 'completed' && !$case->delivered_on)
<div class="modal fade" id="deliveryModal" tabindex="-1" role="dialog" aria-labelledby="deliveryModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
    <form method="POST" action="{{route('admin.case.delivery', ['case_number'=>$case->case_number])}}">
        @csrf
      <div class="modal-header">
        <h5 class="modal-title" id="deliveryModalLabel">Product Delivery</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row border-bottom py-3">
            <label class="col-md-3 col-form-label">Remarks :</label>                                    
            <div class="col-md-9">
                <div class="form-group">
                    <input type="text" class="form-control" name="remarks" id="delivery-remarks" placeholder="Remarks" value="{{ old('remarks') }}">
                </div>
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.row -->
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Change</button>
      </div>
    </form>
    </div>
  </div>
</div>
@endif

@if($case->status == 'active' && !$case->completed_on)
<div class="modal fade" id="locationChangeModel" tabindex="-1" role="dialog" aria-labelledby="locationChangeModelLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
    <form method="POST" action="{{route('admin.case.location', ['case_number'=>$case->case_number])}}">
        @csrf
      <div class="modal-header">
        <h5 class="modal-title" id="locationChangeModelLabel">Change Location</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row border-bottom py-3">
            <label class="col-md-3 col-form-label">Location :</label>                                    
            <div class="col-md-9">
                @foreach (App\Libraries\CaseLibrary::getLocations() as $key=> $item)
                <div class="icheck-primary icheck-inline">
                <input type="radio" id="collected-{{$key}}" value="{{$key}}" name="current_location" @if( old('current_location') == $key) checked @endif/>
                    <label for="collected-{{$key}}">{{$item}}</label>
                </div>
                @endforeach
                <div class="icheck-primary icheck-inline">
                    <input type="radio" id="collected-vendor" value="vendor" name="current_location" @if( old('current_location', $case->current_location) == 'vendor' ) checked @endif/>
                    <label for="collected-vendor">Vendor/Mfr</label>
                </div>
            </div>
            <!-- /.card-body -->
        </div>
        <div class="row border-bottom py-3">
            <label class="col-md-3 col-form-label">Remarks :</label>                                    
            <div class="col-md-9">
                <div class="form-group">
                    <input type="text" class="form-control" name="current_location_remarks" id="current-location-remarks" placeholder="Remarks" value="{{ old('current_location_remarks') }}">
                </div>
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.row -->
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Change</button>
      </div>
    </form>
    </div>
  </div>
</div>
@endif
@endsection
@push('styles')
    <link rel="stylesheet" href="{{ asset('plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}">
@endpush
@push('scripts')

@endpush
