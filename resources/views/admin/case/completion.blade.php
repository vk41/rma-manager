@extends('layouts.admin')
@section('title', 'Case# '.$case->case_number.' completion')
@section('content')
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-3">
                    @include('admin.case.parts.side')                   
                </div>
                <!-- /.col -->
                <div class="col-md-9" id="app">
                    <form method="POST" action="{{route('admin.case.completion', ['case_number'=>$case->case_number])}}">
                        @csrf
                        
                        <div class="card card-secondary">
                            <div class="card-header">
                                <h3 class="card-title">Case</h3>
                            </div>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="part_number">Outcome</label>


                                            <select class="form-control" name="outcome" required>
                                                <option selected disabled>Select one</option>
                                                @foreach (App\Libraries\CaseLibrary::getCompletedOutcomes() as $key=> $item)
                                                <option value="{{$key}}" @if (old('outcome') == $key) selected @endif >{{$item}}</option>
                                                @endforeach
                                                {{-- <option @if (old('outcome', $case->current_location) == 'newyork') selected @endif >New York</option>
                                                <option @if (old('outcome', $case->current_location) == 'miami') selected @endif >Miami</option> --}}
                                            </select>
                                            <small class="form-text text-muted original-value"><b>Original: </b>{{$case->part_number}}</small>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="inputStatus">Location</label>
                                            <select class="form-control" name="current_location" required>
                                                <option selected disabled>Select one</option>
                                                @foreach (App\Libraries\CaseLibrary::getLocations() as $key=> $item)
                                                <option value="{{$key}}" @if (old('current_location', $case->current_location) == $key) selected @endif >{{$item}}</option>
                                                @endforeach
                                                {{-- <option @if (old('current_location', $case->current_location) == 'newyork') selected @endif >New York</option>
                                                <option @if (old('current_location', $case->current_location) == 'miami') selected @endif >Miami</option> --}}
                                            </select>
                                        </div>
                                        
                                    </div>
                                    <!-- /.card-body -->
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="repair_summary">Repair Summary</label>
                                            <textarea id="repair_summary" name="repair_summary" class="form-control" rows="3">{{ old('repair_summary') }}</textarea>
                                        </div>
                                    </div>
                                </div>
                                
                            </div>

                            <div class="overlay d-none">
                                <i class="fas fa-2x fa-sync-alt fa-spin"></i>
                            </div>

                        </div> <!-- /.row -->

                        <div class="row">
                            <div class="col-12">
                                {{-- <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#reject-model">Reject</button> --}}
                                <input type="submit" value="Accept" class="btn btn-success float-right">
                            </div>
                        </div>
                    </form>
                    
                    <!-- /.nav-tabs-custom -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->


          <div class="modal fade" id="reject-model" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <form id="rejection-form">
                    <div class="modal-header">
                        <h4 class="modal-title" id="myModalLabel">Rejection Reason</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="rejection-reason">Reason</label>
                            <textarea class="form-control" id="rejection-reason" rows="2" name="rejection_reason" required></textarea>
                        </div>
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                      <button type="subject" class="btn btn-danger">Reject</button>
                    </div>
                </form>
                
              </div>
            </div>
          </div>

    </section>
@endsection
@push('styles')
    <link rel="stylesheet" href="{{ asset('plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}">
@endpush
@push('scripts')
<script src="{{ asset('plugins/vue/vue.runtime.min.js') }}"></script>
    <script>

// var app = new Vue({
//           el: '#app'
// });

        $(function() {

            $('#rejection-form').on('submit', function(event) {
                event.preventDefault();
                $('#app').find('input[name="reason"]').val($('#rejection-reason').val());
                $('#app').find('input[name="status"]').val('rejected');                
                $('#app').find('form').submit();                                
            });

            $('#autofill_button').on('click', function(event) {                
                event.preventDefault();
                handleLoader();
                axios.post('{{route("admin.case.autofill")}}',  {
                    serial_number: $('#serial_number').val()
                })
                .then(function(response) {
                    handleAutoFill(response.data.data);
                    handleLoader(false);
                    toastr.success('Data filled automatically.');
                })
                .catch(function(err) {
                    handleLoader(false);
                    console.log(err, err.response);
                    toastr.error('Autofill failed.');
                });
            });
        });

        function handleAutoFill(data) {
                var fields = Object.keys(data);
                Object.keys(data).forEach(function(fieldName) {
                    var $element = $('#app').find('#'+fieldName);
                    var newValue = data[fieldName];
                    // console.log(newValue);
                    var originalValue = $element.val();
                    // console.log('originalValue', originalValue);
                    $element.val(newValue);
                    $element.closest('.form-group').addClass('value-changed');
                    // console.log($element);
                });
            }
        function handleLoader(show = true) {
            if(show) {
                $('#app').find('.overlay').removeClass('d-none');
            }else {
                $('#app').find('.overlay').addClass('d-none');
            }
        }
    </script>
@endpush
