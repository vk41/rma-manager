@extends('layouts.admin')
@section('title', 'Home')
@section('content')
    <!-- Main content -->
    <section class="content">
        <!-- Info boxes -->
        
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title">Monthly Recap Report</h5>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-8">
                                <p class="text-center">
                                    <strong>RMAs : {{\Carbon\Carbon::now()->subMonths(11)->format('F-Y')}} - {{\Carbon\Carbon::now()->format('F-Y')}}</strong>
                                </p>

                                <div class="chart">
                                    <!-- Sales Chart Canvas -->
                                    <canvas id="salesChart" height="180" style="height: 180px;"></canvas>
                                </div>
                                <!-- /.chart-responsive -->
                            </div>
                            <!-- /.col -->
                            <div class="col-md-4">
                                <p class="text-center">
                                    <strong>4 Longest Active</strong>
                                </p>
                                @foreach ($longestActiveRmas as $case)
                                    <div class="progress-group">
                                    <a href="{{ route('admin.case.show', ['case_number' => $case->case_number]) }}">{{$case->rma_number}}</a>
                                        <span class="float-right"><b>{{\Carbon\Carbon::parse( $case->product_received_on)->diffInDays(\Carbon\Carbon::now())}} days</b>/ 60 days</span>
                                        <div class="progress progress-sm">
                                            @php
                                                $percentage = (\Carbon\Carbon::parse( $case->product_received_on)->diffInDays(\Carbon\Carbon::now())/ 60) * 100;
                                                $percentage = ($percentage > 100) ? 100 : $percentage;
                                                $theme = 'bg-success';
                                                if($percentage > 90) {
                                                    $theme = 'bg-danger';
                                                }else if($percentage > 60 && $percentage <= 90) {
                                                    $theme = 'bg-warning';
                                                }
                                            @endphp
                                            {{-- {{$percentage}} --}}
                                            <div class="progress-bar {{$theme}}" style="width: {{$percentage}}%"></div>
                                        </div>
                                    </div>
                                @endforeach
                                
                                <!-- /.progress-group -->

                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- ./card-body -->
                    <div class="card-footer">
                        <div class="row">
                            <div class="col-sm-3 col-6">
                                <div class="description-block border-right">
                                    <span class="description-percentage ">Total</span>
                                    <h5 class="description-header">{{$counters['active']}}</h5>
                                    <span class="description-text text-info">ACTIVE</span>
                                </div>
                                <!-- /.description-block -->
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-3 col-6">
                                <div class="description-block border-right">
                                    <span class="description-percentage ">{{\Carbon\Carbon::now()->format('F-Y')}}</span>
                                    <h5 class="description-header">{{array_sum($counters['received'])}}</h5>
                                    <span class="description-text text-primary">Received</span>
                                </div>
                                <!-- /.description-block -->
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-3 col-6">
                                <div class="description-block border-right">
                                    <span class="description-percentage">{{\Carbon\Carbon::now()->format('F-Y')}}</span>
                                    <h5 class="description-header">{{array_sum($counters['completed'])}}</h5>
                                    <span class="description-text text-success">Completed</span>
                                </div>
                                <!-- /.description-block -->
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-3 col-6">
                                <div class="description-block">
                                    <span class="description-percentage">{{\Carbon\Carbon::now()->format('F-Y')}}</span>
                                    <h5 class="description-header">{{array_sum($counters['failed'])}}</h5>
                                    <span class="description-text text-danger">Failed</span>
                                </div>
                                <!-- /.description-block -->
                            </div>
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /.card-footer -->
                </div>
                <!-- /.card -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->


        <div class="row">
            <div class="col-md-4">
                <!-- PRODUCT LIST -->
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title"><a href="{{ route('admin.case.pending') }}">Pending Cases</a></h3>

                        <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                <i class="fas fa-minus"></i>
                            </button>
                            <button type="button" class="btn btn-tool" data-card-widget="remove">
                                <i class="fas fa-times"></i>
                            </button>
                        </div>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body p-0">
                        <ul class="products-list product-list-in-card pl-2 pr-2">
                            @if (count($pendingCases) > 0)
                                @foreach ($pendingCases as $item)
                                    <li class="item">
                                        <div class="">
                                            <a href="{{ route('admin.case.show', ['case_number' => $item->case_number]) }}"
                                                class="product-title">{{ $item->case_number }}
                                                <span
                                                    class="badge badge-secondary float-right">{{ Carbon\Carbon::parse($item->completed_on)->timezone(auth()->user()->timezone)->format('Y-m-d') }}</span></a>
                                            <span class="product-description">
                                                @if ($item->rma_number)
                                                    <div>RMA: {{ $item->rma_number }}</div>
                                                @endif
                                                S/N: <b>{{ $item->serial_number }}</b> PN: <b>{{ $item->part_number }}</b>
                                                @if($item->current_location) 
                                                Loc: <b>{{ $item->current_location }}</b>
                                                @endif
                                            </span>
                                        </div>
                                    </li>
                                @endforeach

                            @else
                                <li class="item">
                                    <div class="">

                                        <span class="product-description text-center">
                                            No Active cases.
                                        </span>
                                    </div>
                                </li>
                            @endif
                        </ul>
                    </div>
                </div>
                <!-- /.card -->
            </div>
            <div class="col-md-8">
                <!-- TABLE: LATEST ORDERS -->
                <div class="card">
                    <div class="card-header border-transparent">
                        <h3 class="card-title">Recent Cases</h3>

                        <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                <i class="fas fa-minus"></i>
                            </button>
                            <button type="button" class="btn btn-tool" data-card-widget="remove">
                                <i class="fas fa-times"></i>
                            </button>
                        </div>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body p-0">
                        <div class="table-responsive">
                            <table class="table m-0">
                                <thead>
                                    <tr>
                                        <th>Case Number</th>
                                        <th>Serial#</th>
                                        <th>Status</th>
                                        <th>Problem</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if (count($recentCases) > 0)
                                        @foreach ($recentCases as $case)
                                            <tr>
                                                <td><a
                                                        href="{{ route('admin.case.show', ['case_number' => $case->case_number]) }}">{{ $case->case_number }}</a>
                                                </td>
                                                <td>{{ $case->serial_number }}</td>
                                                <td>{!! App\Libraries\CaseLibrary::statusHtml($case->status) !!}</td>
                                                <td>{{ $case->problem_summary }} - {{ $case->problem_description }}</td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <td colspan="4" class="text-center">Nothing to display.</td>
                                        </tr>
                                    @endif

                                </tbody>
                            </table>
                        </div>
                        <!-- /.table-responsive -->
                    </div>
                    <!-- /.card-body -->
                    <div class="card-footer clearfix">
                        <a href="{{ route('admin.case.create') }}" class="btn btn-sm btn-info float-left">Create New
                            Case</a>
                        <a href="{{ route('admin.case.active') }}" class="btn btn-sm btn-secondary float-right">View Active
                            Cases</a>
                    </div>
                    <!-- /.card-footer -->
                </div>

                <!-- TABLE: LATEST ORDERS -->
                <div class="card">
                    <div class="card-header border-transparent">
                        <h3 class="card-title"><a href="{{ route('admin.case.completed') }}">Completed (Collection Pending)</a></h3>

                        <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                <i class="fas fa-minus"></i>
                            </button>
                            <button type="button" class="btn btn-tool" data-card-widget="remove">
                                <i class="fas fa-times"></i>
                            </button>
                        </div>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body p-0">
                        <div class="table-responsive">
                            <table class="table m-0">
                                <thead>
                                    <tr>
                                        <th>Case Number</th>
                                        <th>Serial#</th>
                                        <th>Status</th>
                                        <th>Problem</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if (count($readyToCollect) > 0)
                                        @foreach ($readyToCollect as $case)
                                            <tr>
                                                <td><a
                                                        href="{{ route('admin.case.show', ['case_number' => $case->case_number]) }}">{{ $case->case_number }}</a>
                                                </td>
                                                <td>{{ $case->serial_number }}</td>
                                                <td>{!! App\Libraries\CaseLibrary::statusHtml($case->status) !!}</td>
                                                <td>{{ $case->problem_summary }} - {{ $case->problem_description }}</td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <td colspan="4" class="text-center">Nothing to display.</td>
                                        </tr>
                                    @endif

                                </tbody>
                            </table>
                        </div>
                        <!-- /.table-responsive -->
                    </div>
                    <!-- /.card-body -->
                    <div class="card-footer clearfix">
                        {{-- <a href="{{ route('admin.case.create') }}"
                            class="btn btn-sm btn-info float-left">Create New Case</a> --}}
                        <a href="{{ route('admin.case.completed') }}" class="btn btn-sm btn-secondary float-right">Completed
                            Cases</a>
                    </div>
                    <!-- /.card-footer -->
                </div>
            </div>

        </div>
    </section>
@endsection
@push('styles')
    <link href="{{ asset('plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}" rel="stylesheet">
    <link href="{{ asset('plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}" rel="stylesheet">
@endpush

@push('scripts')
    <script src="{{ asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('plugins/chart.js/Chart.min.js') }}"></script>
    {{-- <script src="plugins/chart.js/Chart.min.js"></script>
    --}}

    <script>
        $(document).ready(function() {
            var packagesTable = $('#cases-table').DataTable();
        });
        var salesChartOptions = {
            maintainAspectRatio: false,
            responsive: true,
            legend: {
                display: true
            },
            hover: {
                mode: 'index'
            },
            tooltips: {
                mode: 'index',
            },
            scales: {
                xAxes: [{
                    gridLines: {
                        display: true
                    }
                }],
                yAxes: [{
                    gridLines: {
                        display: true
                    }
                }]
            }
        }
        var salesChartCanvas = $('#salesChart').get(0).getContext('2d')
        
        var salesChartData = {
            labels: ['{!!implode("', '", $counters['graphLabels'])!!}'],
            datasets: [{
                    label: 'Received',
                    borderColor: '#007bff',
                    fill: false,
                    data: [{!!implode(", ", $counters['received'])!!}],
                },
                {
                    label: 'Completed',
                    borderColor: '#28a745',
                    
                    fill: false,
                    data: [{!!implode(", ", $counters['completed'])!!}],
                },
                {
                    label: 'Failed',                    
                    borderColor: '#dc3545',
                    fill: false,
                    data: [{!!implode(", ", $counters['failed'])!!}],
                }
            ]
        }

        var salesChart = new Chart(salesChartCanvas, {
            type: 'line',
            data: salesChartData,
            options: salesChartOptions
        });

    </script>

@endpush
