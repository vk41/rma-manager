@extends('layouts.admin')
@section('title', 'Active RMAs')
@section('content')
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-12">
                <div class="card">

                    <!-- /.card-header -->
                    <div class="card-body">
                        <table id="cases-table" class="table table-bordered table-striped table-hover">
                            <thead>
                                <tr>
                                    <th>RMA#</th>
                                    
                                    <th>Current Loc</th>
                                    <th>Customer</th>
                                    <th>Catergory</th>
                                    <th>Part#</th>
                                    <th>Serial#</th>
                                    <th>Problem</th>                                    
                                    <th>Status</th>
                                    <th>Received On</th>
                                    <th>No Days</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>    
                                @foreach ($cases as $case)
                                <td>{{$case->rma_number}}</td>
                                
                                <td>{{$case->current_location}}</td>
                                <td>{{$case->customer->code}}</td>
                                <td>{{$case->category}}</td>
                                <td>{{$case->part_number}}</td>
                                <td>{{$case->serial_number}}</td>
                                <td>{{$case->problem_description}}</td>
                                <td>{!!App\Libraries\CaseLibrary::statusHtml($case->status)!!}</td>
                                <td>{{$case->product_received_on}}</td>
                                <td>{{($case->product_received_on) ? \Carbon\Carbon::parse($case->product_received_on)->diffInDays(\Carbon\Carbon::now()) : 'NA '}}</td>
                                </tr>
                                @endforeach
                                
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection


@push('styles')
    <link href="{{ asset('plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}" rel="stylesheet">
    <link href="{{ asset('plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}" rel="stylesheet">
    <link href="{{ asset('plugins/datatables-buttons/css/buttons.bootstrap4.min.css') }}" rel="stylesheet">
@endpush

@push('scripts')
<script src="{{ asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-buttons/js/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-buttons/js/buttons.bootstrap4.min.js') }}"></script>
<script src="{{ asset('plugins/jszip/jszip.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-buttons/js/buttons.html5.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-buttons/js/buttons.print.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-buttons/js/buttons.colVis.min.js') }}"></script>

<script>
    $(document).ready(function() {
        var packagesTable = $('#cases-table').DataTable({
                processing: true,                
                autoWidth: false,
                responsive: true,
                pageLength: 20,
                dom: 'Bfrtip',
                buttons: [
                    'copy',  'excel', 
                ],
                order: [
                    [1, 'desc']
                ],                
                lengthMenu: [
                    [10, 20, 50, -1],
                    [10, 20, 50, "All"]
                ],
        });
    });
</script>
@endpush