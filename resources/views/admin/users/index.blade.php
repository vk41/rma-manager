@extends('layouts.admin')
@section('title', 'Users')
@section('content')
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header p-2">
                        <ul class="nav nav-pills">
                            <li class="nav-item"><a class="btn btn-primary" href="{{ route('admin.users.create') }}">Create User</a></li>
                        </ul>
                    </div><!-- /.card-header -->
                    <!-- /.card-header -->
                    <div class="card-body">
                        <table id="main-table" class="table table-bordered table-hover">
                            <thead>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <form class="d-none" id="status-form" method="POST">
            @csrf
            <input type="hidden" name="enabled" value=""/>
    </form>
@endsection


@push('styles')
    <link href="{{ asset('plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}" rel="stylesheet">
    <link href="{{ asset('plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}" rel="stylesheet">
@endpush

@push('scripts')
    <script src="{{ asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>

    <script>
        $(document).ready(function() {
            var url = "{{ route('admin.users.data') }}";
            var packagesTable = $('#main-table').DataTable({
                    processing: true,
                    serverSide: true,
                    autoWidth: false,
                    responsive: true,
                    pageLength: 20,
                    order: [
                        [0, 'asc']
                    ],
                    ajax: url,
                    lengthMenu: [
                        [10, 20, 50, -1],
                        [10, 20, 50, "All"]
                    ],
                    columns: [
                        {
                            data: 'name',
                            width: '150px',
                            title: 'Name',
                            render: function(data, type, row) {
                                return `<a class="case-link" data-id="${row.id}" href="#">${data}</a>`
                            }
                        },
                        {
                            data: 'email',
                            width: '150px',
                            title: 'Email',
                        },
                        {
                            data: 'role',
                            width: '100px',
                            title: 'Roles'
                        },
                        {
                            data: 'enabled',
                            width: '100px',
                            title: 'Enabled',
                            render: function(data, type, row) {
                                return (row.enabled == 1) ? '<span class="badge badge-success">Enabled</span>': '<span class="badge badge-warning">Disabled</span>';
                            }
                        },
                        
                        {                            
                            width: '100px',
                            title: 'Action',
                            render: function(data, type, row) {
                                // return row.login_enabled;
                                var actions = [`<a class="action-link btn btn-info btn-sm" data-action="edit" data-id="${row.id}" href="#"><i class="far fa-edit"></i></a>`];
                                if(row.enabled == 1) {
                                    actions.push(`<a class="action-link btn btn-warning btn-sm" data-action="disable" data-id="${row.id}" href="#"><i class="fas fa-ban"></i></a>`)
                                }else {
                                    actions.push(`<a class="action-link btn btn-success btn-sm" data-action="enable" data-id="${row.id}" href="#"><i class="fas fa-check"></i></a>`)
                                }
                                        
                                        return actions.join(' ');
                            }
                        },

                    ],
                    drawCallback: function(settings) {
                        $(settings.nTable).find('.case-link').off().on('click', function(event) {
                          event.preventDefault();
                          var caseUrl = "{{route('admin.users.show',  ':custid')}}";
                          var custID = $(event.currentTarget).data('id');
                          var targetUrl = caseUrl.replace(':custid', custID);
                          window.location.href = targetUrl;
                        });
                        $(settings.nTable).find('.action-link').off().on('click', actionCallback);
                    }
                })
                .on('processing.dt', function(e, settings, processing) {
                    if (processing) {
                        $('#table-overlay').removeClass("d-none");
                    } else {
                        $('#table-overlay').addClass("d-none", true);
                    }
                });
        });


        function actionCallback(event) {
            event.preventDefault();
            var action =  $(event.currentTarget).data('action');
            var custID = $(event.currentTarget).data('id');

            if(action == 'enable' || action == 'disable') {
                loginStatusCallback(custID, action);
            } else if(action == 'edit') {
                var caseUrl = "{{route('admin.users.edit', ':custid')}}";
                var targetUrl = caseUrl.replace(':custid', custID);
                window.location.href = targetUrl;
            }
            
        }

        function loginStatusCallback(custID, action) {
            var response = confirm(`Are you sure to ${action} the user?`);
            if (response== true) {
                var caseUrl = "{{route('admin.users.status', ':custid')}}";
                var targetUrl = caseUrl.replace(':custid', custID);
                var actionVal = (action == 'enable') ? 1:0;
                $('#status-form').find('input[name="enabled"]').val(actionVal);
                $('#status-form').attr('action', targetUrl).submit();
                
            }            
        }

    </script>

@endpush
