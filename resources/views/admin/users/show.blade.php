@extends('layouts.admin')
@section('title', 'View Customer: ' . $user->name)
@section('content')
    <!-- Main content -->
    <section class="content mb-2">
        
            <div class="row">
                <div class="col-md-6 offset-md-3">
                    <div class="card card-primary" >
                        <div class="card-header">
                            <h3 class="card-title">User</h3>
                        </div>
                        <div class="card-body">
                            <table class="table">
                                <tr>
                                    <th>Name</th>
                                    <td>{{$user->name}}<td>
                                </tr>
                                <tr>
                                    <th>Email</th>
                                    <td>{{$user->email}}<td>
                                </tr>
                                <tr>
                                    <th>Role</th>
                                    <td>{{ucfirst($user->role)}}<td>
                                </tr>
                                <tr>
                                    <th>Timezone</th>
                                    <td>{{$user->timezone}}<td>
                                </tr>
                                <tr>
                                    <th>Status</th>
                                    <td>
                                        @if($user->enabled)
                                        <span class="badge badge-success">Enabled</span>
                                        @else
                                        <span class="badge badge-warning">Disabled</span>
                                        @endif
                                        <td>
                                </tr>
                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->

                    {{-- <form method="POST" id="delete-form" action="{{route('admin.users.destroy', $user->id)}}">
                        @csrf
                        @method('DELETE')
                    </form> --}}
                    {{-- <input type="button" id="delete-button" value="Delete" class="btn btn-danger"> --}}
                    <a href="{{route('admin.users.edit', ['user' => $user->id])}}" class="btn btn-warning float-right">Edit</a>
                </div>
                
                
                
            </div>
            
            <div class="row mb-2">
                <div class="col-12">
                    
                </div>
            </div>
    </section>
@endsection
@push('styles')
    <link rel="stylesheet" href="{{ asset('plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/select2/css/select2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css') }}">
@endpush
@push('scripts')
    <script>
        $(function() {
            $('#delete-button').on('click', function(event) {
                event.preventDefault();
                var response = confirm("Are you sure to delete the customer?");
                if (response== true) {
                               
                    $('#delete-form').submit();
                    
                }
            })
        });
    </script>
    
@endpush
