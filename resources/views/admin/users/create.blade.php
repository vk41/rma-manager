@extends('layouts.admin')
@section('title', 'Add New User')
@section('content')
    <!-- Main content -->
    <section class="content mb-2">
        <form method="POST" action="{{ route('admin.users.store') }}">
            @csrf
            @method('POST')
            <div class="row">
                <div class="col-md-6 offset-md-3">
                    <div class="card card-primary" >
                        <div class="card-header">
                            <h3 class="card-title">User</h3>
                        </div>
                        <div class="card-body">
                            <div class="form-group">
                                <label for="name">Name</label>
                                <input type="text" id="name" autocomplete="off" name="name" class="form-control"
                                    value="{{ old('name') }}" required>
                            </div>
                            <div class="form-group">
                                <label for="email">Login Email</label>
                                <input type="email" id="email" autocomplete="off" name="email" class="form-control"
                                    value="{{ old('email') }}" required>
                            </div>
                            <div class="form-group">
                                <label for="password">Password</label>
                                <div class="input-group">
                                  <input type="text" autocomplete="off" class="form-control" id="password" name="password">
                                  <div class="input-group-append">
                                    <button id="password-generate-button" class="btn btn-outline-primary" type="button">Generate</button>
                                  </div>
                                </div>
                              </div>
                            
                            
                        
                    
                            <div class="form-group">
                                <legend class="col-form-label  pt-0"><b>Role</b></legend>
                                <div class="icheck-primary icheck-inline">
                                    <input type="radio" id="role_user" value="user" name="role"   @if (old('role') == 'user') checked @endif required/>
                                    <label for="role_user">User</label>
                                </div>
                                <div class="icheck-primary icheck-inline">
                                    <input type="radio" id="role_manager" value="manager" name="role"   @if (old('role') == 'manager') checked @endif required/>
                                    <label for="role_manager">Manager</label>
                                </div>
                                <div class="icheck-primary icheck-inline">
                                    <input type="radio" id="role_admin" value="admin" name="role"   @if (old('role') == 'admin') checked @endif required/>
                                    <label for="role_admin">Admin</label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputTimezone">Timezone</label>
                                <select class="form-control" name="timezone" required>
                                    <option selected disabled>Select one</option>
                                    @foreach (timezone_identifiers_list() as $timezone)
                                        <option @if (old('timezone') == $timezone) selected @endif >{{$timezone}}</option>
                                    @endforeach
                                </select>
                            </div>
                            
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                    <a href="{{ url('/') }}" class="btn btn-secondary">Cancel</a>
                    <input type="submit" value="Create User" class="btn btn-success float-right">
                </div>
            </div>
        </form>
    </section>
@endsection
@push('styles')
    <link rel="stylesheet" href="{{ asset('plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/select2/css/select2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css') }}">
@endpush
@push('scripts')
    <script>
        function generatePassword(length=8, charset="abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789") {
            return new Array(length)
            .fill(null)
            .map(()=> charset.charAt(Math.floor(Math.random() * charset.length)))
            .join('');
        }
        $(function() {
            $('#password-generate-button').on('click', function(event) {
                event.preventDefault();
                $('#password').val(generatePassword(8));
            });
        });
    </script>
    
@endpush
