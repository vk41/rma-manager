@extends('layouts.admin')
@section('title', 'Add New Customer')
@section('content')
    <!-- Main content -->
    <section class="content mb-2">
        <form method="POST" action="{{ route('admin.customers.store') }}">
            @csrf
            @method('POST')
            <div class="row">
                <div class="col-md-4">
                    <div class="card card-primary" >
                        <div class="card-header">
                            <h3 class="card-title">Customer</h3>
                        </div>
                        <div class="card-body">
                            <div class="form-group">
                                <label for="code">Code</label>
                                <input type="text" id="code" autocomplete="off" name="code" class="form-control"
                                    value="{{ old('code') }}" required>
                            </div>
                            <div class="form-group">
                                <label for="name">Name</label>
                                <input type="text" id="name" autocomplete="off" name="name" class="form-control"
                                    value="{{ old('name') }}" required>
                            </div>
                            <div class="form-group">
                                <label for="location">Location</label>
                                <input type="text" id="location" autocomplete="off" name="location" class="form-control"
                                    value="{{ old('location') }}" required>
                            </div>
                            <div class="form-group">
                                <label for="inputTimezone">Timezone</label>
                                <select class="form-control" name="timezone" required>
                                    <option selected disabled>Select one</option>
                                    @foreach (timezone_identifiers_list() as $timezone)
                                        <option @if (old('timezone') == $timezone) selected @endif >{{$timezone}}</option>
                                    @endforeach
                                </select>
                            </div>
                            
                        </div>
                        <!-- /.card-body -->
                        <div class="overlay d-none">
                            <i class="fas fa-2x fa-sync-alt fa-spin"></i>
                        </div>
                    </div>
                    <!-- /.card -->
                </div>
                <div class="col-md-4">
                    <div class="card card-primary" >
                        <div class="card-header">
                            <h3 class="card-title">Cotnact</h3>
                        </div>
                        <div class="card-body">
                            <div class="form-group">
                                <label for="contact_name">Contact Name</label>
                                <input type="text" id="contact_name" autocomplete="off" name="contact_name" class="form-control"
                                    value="{{ old('contact_name') }}" required>
                            </div>
                            <div class="form-group">
                                <label for="contact_number">Contact Number</label>
                                <input type="text" id="contact_number" autocomplete="off" name="contact_number" class="form-control"
                                    value="{{ old('contact_number') }}" required>
                            </div>
                            <div class="form-group">
                                <label for="contact_email">Contact Email</label>
                                <input type="email" id="contact_email" autocomplete="off" name="contact_email" class="form-control"
                                    value="{{ old('contact_email') }}" required>
                            </div>
                            <div class="form-group">
                                <label for="inputRating">Rating</label>
                                <select class="form-control" name="rating" required>
                                    <option selected disabled>Select one</option>
                                    @for ($i = 1; $i < 10; $i++)    
                                        <option @if (old('rating') == $i) selected @endif >{{$i}}</option>                                    
                                    @endfor
                                </select>
                            </div>
                            
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <div class="col-md-4">
                    <div class="card card-primary" >
                        <div class="card-header">
                            <h3 class="card-title">Credential</h3>
                        </div>
                        <div class="card-body">
                            
                            <div class="form-group">
                                <label for="email">Login Email</label>
                                <input type="email" id="email" autocomplete="off" name="email" class="form-control"
                                    value="{{ old('email') }}" required>
                            </div>
                            <div class="form-group">
                                <label for="password">Password</label>
                                <div class="input-group">
                                  <input type="text" autocomplete="off" class="form-control" id="password" name="password">
                                  <div class="input-group-append">
                                    <button id="password-generate-button" class="btn btn-outline-primary" type="button">Generate</button>
                                  </div>
                                </div>
                              </div>
                              <div class="form-group">
                                <label for="inputStatus">Company</label>
                                <select class="form-control" name="company" required>
                                    <option selected disabled>Select one</option>
                                    @foreach (App\Models\Company::all() as $company)
                                        <option value="{{$company->id}}" @if (old('company') == $company->id) selected @endif >{{$company->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="salesman">Sales Man</label>
                                <input type="text" id="salesman" autocomplete="off" name="salesman" class="form-control"
                                    value="{{ old('salesman') }}" required>
                            </div>
                            <div class="form-group">
                                <div class="icheck-primary icheck-inline">
                                    <input type="checkbox" id="send_password" value="yes" name="send_password" @if (old('send_password', true) == 'yes') checked  @endif/>
                                    <label for="send_password">Send login information to customer</label>
                                </div>
                            </div>

                            
                        </div>
                        <!-- /.card-body -->                        
                    </div>
                    <!-- /.card -->
                </div>
                
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card card-primary" >
                        <div class="card-header">
                            <h3 class="card-title">Remarks</h3>
                        </div>
                        <div class="card-body">
                            <div class="form-group">
                                <textarea id="remarks" autocomplete="off" name="remarks" class="form-control">{{ old('remarks') }}</textarea>
                            </div>
                            
                        </div>
                        <!-- /.card-body -->
                        <div class="overlay d-none">
                            <i class="fas fa-2x fa-sync-alt fa-spin"></i>
                        </div>
                    </div>
                    <!-- /.card -->
                </div>
            </div>
            <div class="row mb-2">
                <div class="col-12">
                    <a href="{{ url('/') }}" class="btn btn-secondary">Cancel</a>
                    <input type="submit" value="Create Customer" class="btn btn-success float-right">
                </div>
            </div>
        </form>
    </section>
@endsection
@push('styles')
    <link rel="stylesheet" href="{{ asset('plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/select2/css/select2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css') }}">
@endpush
@push('scripts')
    <script>
        function generatePassword(length=8, charset="abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789") {
            return new Array(length)
            .fill(null)
            .map(()=> charset.charAt(Math.floor(Math.random() * charset.length)))
            .join('');
        }
        $(function() {
            $('#password-generate-button').on('click', function(event) {
                event.preventDefault();
                $('#password').val(generatePassword(8));
            });
        });
    </script>
    
@endpush
