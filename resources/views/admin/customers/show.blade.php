@extends('layouts.admin')
@section('title', 'View Customer: ' . $customer->name)
@section('content')
    <!-- Main content -->
    <section class="content mb-2">
        
            <div class="row">
                <div class="col-md-4">
                    <div class="card card-primary" >
                        <div class="card-header">
                            <h3 class="card-title">Customer</h3>
                        </div>
                        <div class="card-body">
                            <table class="table">
                                <tr>
                                    <th>Code</th>
                                    <td>{{$customer->code}}<td>
                                </tr>
                                <tr>
                                    <th>Name</th>
                                    <td>{{$customer->name}}<td>
                                </tr>
                                <tr>
                                    <th>Location</th>
                                    <td>{{$customer->location}}<td>
                                </tr>
                                <tr>
                                    <th>Timezone</th>
                                    <td>{{$customer->timezone}}<td>
                                </tr>
                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <div class="col-md-4">
                    <div class="card card-primary" >
                        <div class="card-header">
                            <h3 class="card-title">Cotnact</h3>
                        </div>
                        <div class="card-body">
                            <table class="table">
                                <tr>
                                    <th>Contact Name</th>
                                    <td>{{$customer->contact_name}}<td>
                                </tr>
                                <tr>
                                    <th>Contact Number</th>
                                    <td>{{$customer->contact_number}}<td>
                                </tr>
                                <tr>
                                    <th>Contact Email</th>
                                    <td>{{$customer->contact_email}}<td>
                                </tr>
                                <tr>
                                    <th>Rating</th>
                                    <td><span class="text-warning">{!!$customer->showRatingStars()!!}</span><td>
                                </tr>
                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <div class="col-md-4">
                    <div class="card card-primary" >
                        <div class="card-header">
                            <h3 class="card-title">Credential</h3>
                        </div>
                        <div class="card-body">
                            <table class="table">
                                <tr>
                                    <th>Login Email</th>
                                    <td>{{$customer->email}}<td>
                                </tr>
                                <tr>
                                    <th>Company</th>
                                    <td>{{$customer->company->name}}<td>
                                </tr>
                                <tr>
                                    <th>Sales Man</th>
                                    <td>{{$customer->salesman}}<td>
                                </tr>
                            </table>
                            
                        </div>
                        <!-- /.card-body -->
                        <div class="overlay d-none">
                            <i class="fas fa-2x fa-sync-alt fa-spin"></i>
                        </div>
                    </div>
                    <!-- /.card -->
                </div>
                
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card card-primary" >
                        <div class="card-header">
                            <h3 class="card-title">Remarks</h3>
                        </div>
                        <div class="card-body">
                            <p>{{$customer->remarks}}</p>
                            
                        </div>
                        <!-- /.card-body -->
                        <div class="overlay d-none">
                            <i class="fas fa-2x fa-sync-alt fa-spin"></i>
                        </div>
                    </div>
                    <!-- /.card -->
                </div>
            </div>
            <div class="row mb-2">
                <div class="col-12">
                    <a href="{{route('admin.customers.edit', ['customer' => $customer->id])}}" class="btn btn-warning float-right">Edit</a>
                </div>
            </div>
    </section>
@endsection
@push('styles')
    <link rel="stylesheet" href="{{ asset('plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/select2/css/select2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css') }}">
@endpush
@push('scripts')
    <script>
        $(function() {
            $('#delete-button').on('click', function(event) {
                event.preventDefault();
                var response = confirm("Are you sure to delete the customer?");
                if (response== true) {
                               
                    $('#delete-form').submit();
                    
                }
            })
        });
    </script>
    
@endpush
