@extends('layouts.admin')
@section('title', 'Search for ' . $keyword)
@section('content')
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-12">
                <div class="card">

                    <!-- /.card-header -->
                    <div class="card-body">
                        @if(isset($cases))
                        @if($cases->count()>0)
                        <table id="cases-table" class="table table-bordered table-striped table-hover">
                            <thead>
                                <tr>
                                    <th>Case Number</th>
                                    <th>Status</th>
                                    <th>RMA Number</th>
                                    <th>Part Number</th>
                                    <th>Serial Number</th>
                                    <th>Customer</th>
                                    <th>Problem</th>
                                </tr>   
                            </thead>
                            <tbody>
                                @foreach ($cases as $case)
                                <tr>
                                    <td>{{$case->case_number}}</td>
                                    <td>{!!App\Libraries\CaseLibrary::statusHtml($case->status)!!}</td>
                                    <td>{{$case->rma_number}}</td>
                                    <td>{{$case->part_number}}</td>
                                    <td>{{$case->serial_number}}</td>
                                    <td>{{$case->customer->code}}</td>
                                    <td>{{$case->problem_summary}} - {{$case->problem_description}}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        @else
                        <div class="row justify-content-md-center">
                            <div class="col-md-6 col-md-offset-3 text-center">
                                <h3>No results found for the search query.</h3>
                                <form class=" ml-3" action="{{route('admin.search')}}">
                                    <div class="input-group input-group-lg">
                                        <input class="form-control form-control-navbar"  type="search" name="keyword" @if(request()->input('keyword')) value="{{request()->input('keyword')}}" @endif placeholder="Search for RMA" aria-label="Search">
                                        <div class="input-group-append">
                                          <button class="btn btn-primary" type="submit">
                                            <i class="fas fa-search"></i>
                                          </button>
                                        </div>
                                      </div>
                                    </form>
                            </div>
                        </div>
                        
                        @endif
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection


@push('styles')
    <link href="{{ asset('plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}" rel="stylesheet">
    <link href="{{ asset('plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}" rel="stylesheet">
@endpush

@push('scripts')
    <script src="{{ asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>

    <script>
        $(document).ready(function() {
            var url = "{{ route('admin.case.data', ['status' => 'active']) }}";
            var packagesTable = $('#cases-table').DataTable();
            var packagesTable = $('#casess-table').DataTable({
                    processing: true,
                    serverSide: true,
                    autoWidth: false,
                    responsive: true,
                    pageLength: 20,
                    order: [
                        [1, 'desc']
                    ],
                    ajax: url,
                    lengthMenu: [
                        [10, 20, 50, -1],
                        [10, 20, 50, "All"]
                    ],
                    columns: [{
                            data: 'rma_number',
                            width: '100px',
                            title: 'RMA Number',
                            render: function(data, type, row) {
                                return `<a class="case-link" data-case="${row.case_number}" href="#">${data}</a>`
                            }
                        },
                        {
                            data: 'part_number',
                            width: '100px',
                            title: 'Part Number'
                        },
                        {
                            data: 'serial_number',
                            width: '100px',
                            title: 'Serial Number'
                        },
                        {
                            data: 'customer_code',
                            width: '75px',
                            title: 'Customer'
                        },
                        {
                            data: 'problem_summary',
                            width: '150px',
                            title: 'Problem Summary'
                        },
                        {
                            data: 'problem_description',
                            title: 'Problem Description'
                        },
                        {
                            data: 'rma_issued_at',
                            width: '150px',
                            title: 'RMA Issue Date'
                        },
                        {
                            data: 'status',
                            width: '75px',
                            title: 'Status',
                            render: function(data, type, row) {
                                return statusHtml(data);
                            }
                        },
                        {
                            data: 'company_name',
                            width: '100px',
                            title: 'Company'
                        },
                        {
                            data: 'case_created_on',
                            width: '150px',
                            title: 'Case Created On'
                        },
                    ],
                    drawCallback: function(settings) {
                        $(settings.nTable).find('.case-link').off().on('click', function(event) {
                          event.preventDefault();
                          var caseUrl = "{{route('admin.case.show', ['case_number' => ':caseno'])}}";
                          var caseNumber = $(event.currentTarget).data('case');
                          var targetUrl = caseUrl.replace(':caseno', caseNumber);
                          window.open(targetUrl, '_blank');
                        });
                    }
                })
                .on('processing.dt', function(e, settings, processing) {
                    if (processing) {
                        $('#table-overlay').removeClass("d-none");
                    } else {
                        $('#table-overlay').addClass("d-none", true);
                    }
                });
        });

    </script>

@endpush
