<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="{{url('/')}}" class="brand-link">
      <img src="{{asset('images/logo/icon/'.\App\Libraries\Site::getLogo())}}"
          alt="{{\App\Libraries\Site::getCompanyName()}}"
           class="brand-image"> 
    <span class="brand-text font-weight-light">{{\App\Libraries\Site::getCompanyName()}}</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          {{-- {{request()->segment(0)}}
          {{Route::currentRouteName()}} --}}
          <li class="nav-item">
            <a href="{{ route('customer.index') }}" class="nav-link {{ Route::is('customer.index') ? 'active' : '' }}">
              <i class="nav-icon fas fa-home"></i>
              <p>
                Home
                {{-- <span class="right badge badge-danger">New</span> --}}
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ route('customer.case.create') }}" class="nav-link {{ Route::is('customer.case.create') ? 'active' : '' }}">
              <i class="nav-icon fas fa-edit"></i>
              <p>
                Create Case
                {{-- <span class="right badge badge-danger">New</span> --}}
              </p>
            </a>
          </li>
          
          <li class="nav-item has-treeview {{ Route::is('customer.case.*') ? 'menu-open' : '' }}">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-th"></i>
              <p>
                Cases
                <i class="fas fa-angle-left right"></i>
                {{-- <span class="badge badge-info right">6</span> --}}
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{ route('customer.case.active') }}" class="nav-link {{ Route::is('customer.case.active') ? 'active' : '' }}">
                  <i class="fas fa-chart-line nav-icon"></i>
                  <p>Active RMAs
                  <span class="badge badge-info right">{{App\Libraries\CaseLibrary::getCountByStatus( ['accepted','active'], auth()->user())}}</span>
                  </p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{ route('customer.case.pending') }}" class="nav-link {{ Route::is('customer.case.pending')  ? 'active' : '' }}">
                  <i class="fas fa-question nav-icon"></i>
                  <p>Pending
                    <span class="badge badge-info right">{{App\Libraries\CaseLibrary::getCountByStatus( ['pending'], auth()->user())}}</span>
                  </p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{ route('customer.case.completed') }}" class="nav-link {{ Route::is('customer.case.completed')  ? 'active' : '' }}">
                  <i class="fas fa-check nav-icon"></i>
                  <p>Completed</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{ route('customer.case.others') }}" class="nav-link {{ Route::is('customer.case.others') ? 'active' : '' }}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Others</p>
                </a>
              </li>
            </ul>
          </li>
          
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>