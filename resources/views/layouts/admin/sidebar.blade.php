<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="{{url('/admin')}}" class="brand-link">
      <img src="{{asset('images/logo/icon/'.\App\Libraries\Site::getLogo())}}"
          alt="{{\App\Libraries\Site::getCompanyName()}}"
           class="brand-image"> 
    <span class="brand-text font-weight-light">{{\App\Libraries\Site::getCompanyName()}}</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          {{-- {{request()->segment(0)}}
          {{Route::currentRouteName()}} --}}
          <li class="nav-item">
            <a href="{{ route('admin.index') }}" class="nav-link {{ Route::is('admin.index') ? 'active' : '' }}">
              <i class="nav-icon fas fa-home"></i>
              <p>
                Home
                {{-- <span class="right badge badge-danger">New</span> --}}
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ route('admin.case.create') }}" class="nav-link {{ Route::is('admin.case.create') ? 'active' : '' }}">
              <i class="nav-icon fas fa-edit"></i>
              <p>
                Create Case
                {{-- <span class="right badge badge-danger">New</span> --}}
              </p>
            </a>
          </li>
          
          <li class="nav-item has-treeview {{ Route::is('admin.case.*') ? 'menu-open' : '' }}">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-th"></i>
              <p>
                Cases
                <i class="fas fa-angle-left right"></i>
                {{-- <span class="badge badge-info right">6</span> --}}
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{ route('admin.case.active') }}" class="nav-link {{ Route::is('admin.case.active') ? 'active' : '' }}">
                  <i class="fas fa-chart-line nav-icon"></i>
                  <p>Active RMAs
                  <span class="badge badge-info right">{{App\Libraries\CaseLibrary::getCountByStatus( ['accepted','active'])}}</span>
                  </p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{ route('admin.case.pending') }}" class="nav-link {{ Route::is('admin.case.pending') ? 'active' : '' }}">
                  <i class="fas fa-question nav-icon"></i>
                  <p>Pending
                    <span class="badge badge-info right">{{App\Libraries\CaseLibrary::getCountByStatus( ['pending'])}}</span>
                  </p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{ route('admin.case.completed') }}" class="nav-link {{ Route::is('admin.case.completed') ? 'active' : '' }}">
                  <i class="fas fa-check nav-icon"></i>
                  <p>Completed</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{ route('admin.case.others') }}" class="nav-link {{ Route::is('admin.case.others') ? 'active' : '' }}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Others</p>
                </a>
              </li>
            </ul>
          </li>

          <li class="nav-item has-treeview {{ Route::is('admin.report.*') ? 'menu-open' : '' }}">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-chart-bar"></i>
              <p>
                Reports
                <i class="fas fa-angle-left right"></i>
                
                {{-- <span class="badge badge-info right">6</span> --}}
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{ route('admin.report.month') }}" class="nav-link {{ Route::is('admin.report.month') ? 'active' : '' }}">
                  <i class="fas fa-chart-line nav-icon"></i>
                  <p>This Month</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{ route('admin.report.active') }}" class="nav-link {{ Route::is('admin.report.active') ? 'active' : '' }}">
                  <i class="fas fa-check nav-icon"></i>
                  <p>Active</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{ route('admin.case.others') }}" class="nav-link {{ Route::is('admin.case.others') ? 'active' : '' }}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Others</p>
                </a>
              </li>
            </ul>
          </li>

          <li class="nav-item">
            <a href="{{ route('admin.customers.index') }}" class="nav-link {{ Route::is('admin.customers.*') ? 'active' : '' }}">
              <i class="nav-icon fas fa-users"></i>
              <p>Customers</p>
            </a>
          </li>
          {{-- <li class="nav-item">
            <a href="{{ route('admin.companies.index') }}" class="nav-link {{ Route::is('admin.companies.*') ? 'active' : '' }}">
              <i class="nav-icon fas fa-building"></i>
              <p>Companies</p>
            </a>
          </li> --}}
          <li class="nav-item">
            <a href="{{ route('admin.users.index') }}" class="nav-link {{ Route::is('admin.users.*')  ? 'active' : '' }}">
              <i class="nav-icon fas fa-user-tie"></i>
              <p>Users</p>
            </a>
          </li>
          
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>