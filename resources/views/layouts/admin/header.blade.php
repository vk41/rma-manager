<nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>
    </ul>

    <!-- SEARCH FORM -->
    <form class="form-inline ml-3 position-relative" action="{{route('admin.search')}}">
    <div class="input-group input-group-sm">
        <input class="form-control form-control-navbar" id="main-search" type="search" name="keyword" @if(request()->input('keyword')) value="{{request()->input('keyword')}}" @endif placeholder="Search for RMA" aria-label="Search">
        <div class="input-group-append">
          <button class="btn btn-navbar" type="submit">
            <i class="fas fa-search"></i>
          </button>
        </div>
      </div>
    </form>

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
      <li class="nav-item dropdown user-menu">
        <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">
          {{-- <img src="../../dist/img/user2-160x160.jpg" class="user-image img-circle elevation-2" alt="User Image"> --}}
        <span class="d-none d-md-inline"><i class="fas fa-user"></i> {{auth()->user()->name}}</span>
        </a>
        <ul class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
          <!-- User image -->
          <li class="text-center p-3 bg-primary">
            <p>
              <b>  </i>{{auth()->user()->name}}</b>
            </p>
          </li>
          <!-- Menu Footer-->
          <li class="user-footer">
            {{-- <a href="{{route('customer.user.profile')}}" class="btn btn-default btn-flat">Profile</a> --}}
            <a href="#" class="btn btn-default btn-flat float-right" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Sign out</a>
            <form id="logout-form" action="{{ route('admin.auth.logout') }}" method="POST" class="d-none">
              @csrf
          </form>
          </li>
        </ul>
      </li>
    </ul>
  </nav>