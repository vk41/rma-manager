<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>
    @yield('title')
    
    </title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Font Awesome -->
  <link href="{{ asset('plugins/fontawesome-free/css/all.min.css') }}" rel="stylesheet">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- overlayScrollbars -->
  {{-- <link rel="stylesheet" href="../../dist/css/adminlte.min.css"> --}}
  <link href="{{ asset('css/app.css') }}" rel="stylesheet">
  {{-- <link href="{{ asset('plugins/overlayScrollbars/css/OverlayScrollbars.min.css') }}" rel="stylesheet"> --}}
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  @stack('styles')
</head>
<body class="hold-transition sidebar-mini layout-fixed">
<!-- Site wrapper -->
<div class="wrapper">
  <!-- Navbar -->
  @include('layouts.admin.header')
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  
@include('layouts.admin.sidebar')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>@yield('title')</h1>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
    @if (\Session::has('success-message'))
    <div class="alert alert-success alert-dismissible mx-2" bis_skin_checked="1">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
      <h5><i class="icon fas fa-check"></i> Success</h5>
      {!!Session::get('success-message')!!}
    </div>
    @endif

    @if (\Session::has('error-message'))
    <div class="alert alert-danger alert-dismissible mx-2" bis_skin_checked="1">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
      <h5><i class="icon fas fa-ban"></i> Error</h5>
      {!!Session::get('error-message')!!}
    </div>
    @endif
    @if (\Session::has('warning-message'))
    <div class="alert alert-warning alert-dismissible mx-2" bis_skin_checked="1">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
      <h5><i class="icon fas fa-exclamation-triangle"></i> Alert</h5>
      {!!Session::get('warning-message')!!}
    </div>
    @endif
    @if (\Session::has('info-message'))
    <div class="alert alert-info alert-dismissible mx-2" bis_skin_checked="1">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
      <h5><i class="icon fas fa-info"></i> Info</h5>
      {!!Session::get('info-message')!!}
    </div>
    @endif

    @if(count( $errors ) > 0)
    <div class="alert alert-danger alert-dismissible mx-2" bis_skin_checked="1">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
      <h5><i class="icon fas fa-ban"></i> Error</h5>
      @foreach ($errors->all() as $error)
      <div>{!! $error !!}</div>
      @endforeach
    </div>
    @endif    

    @yield('content')
  </div>
  <!-- /.content-wrapper -->

  <footer class="main-footer">
    {{-- <div class="float-right d-none d-sm-block">
      <b>Version</b> 1.0.0
    </div> --}}
    <strong>Copyright &copy; {{date('Y')}} <a target="_blank" href="http://{{\App\Libraries\Site::getCompanyWebsite()}}">{{\App\Libraries\Site::getCompanyName()}}</a>.</strong> All rights
    reserved.
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->
<script src="{{ asset('js/app.js') }}"></script>
{{-- <script src="{{ asset('plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js')}}"></script> --}}
<!-- jQuery -->
{{-- <script src="../../plugins/jquery/jquery.min.js"></script> --}}
<!-- Bootstrap 4 -->
{{-- <script src="../../plugins/bootstrap/js/bootstrap.bundle.min.js"></script> --}}
<!-- AdminLTE App -->
{{-- <script src="../../dist/js/adminlte.min.js"></script> --}}
<!-- AdminLTE for demo purposes -->
{{-- <script src="../../dist/js/demo.js"></script> --}}
@stack('scripts')
</body>
</html>
