@component('mail::layout')
{{-- Header --}}
@slot('header')
@component('mail::header', ['company' => $company])
{{ config('app.name') }}
@endcomponent
@endslot

{{-- Body --}}
{{ $slot }}

{{-- Subcopy --}}
@isset($subcopy)
@slot('subcopy')
@component('mail::subcopy')
{{ $subcopy }}
@endcomponent
@endslot
@endisset

{{-- Footer --}}
@slot('footer')
@component('mail::footer', ['company' => $company])
© {{ date('Y') }} {{ $company->name }}. @lang('All rights reserved.')
@endcomponent
@endslot
@endcomponent
