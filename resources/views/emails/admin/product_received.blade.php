@component('mail::message', ['company' => $company])
# Product Received.

Hello, {{$customer->name}}

We have received product for the RMA# <b>{{$case->rma_number}}</b>. 

<table class="table w-100 bordered" cellpadding="0" cellspacing="0" >
<tbody>
<tr>
<th>Part Number</th>
<td>{{$case->part_number}}</td>
</tr>
<tr>
<th>Serial Number</th>
<td>{{$case->serial_number}}</td>
</tr>
<tr>
<th>Invoice Number</th>
<td>{{$case->invoice_number}}</td>
</tr>
<tr>
<th>Problem</th>
<td>{{$case->problem_summary}}</td>
</tr>
<tr>
<td colspan="2">{{$case->problem_description}}</td>
</tr>
</tbody>
</table>



Thanks,<br>
{{ $company->name }}
@endcomponent