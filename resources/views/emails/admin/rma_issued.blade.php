@component('mail::message', ['company' => $company])
# RMA Issued.

Hello, {{$customer->name}}

We have issued your RMA# <b>{{$case->rma_number}}</b> for the Case# <b>{{$case->case_number}}</b> the below item. Please send the product along with all the original accessories.

<table class="table w-100 bordered" cellpadding="0" cellspacing="0" >
<tbody>
<tr>
<th>Part Number</th>
<td>{{$case->part_number}}</td>
</tr>
<tr>
<th>Serial Number</th>
<td>{{$case->serial_number}}</td>
</tr>
<tr>
<th>Invoice Number</th>
<td>{{$case->invoice_number}}</td>
</tr>
<tr>
<th>Problem</th>
<td>{{$case->problem_summary}}</td>
</tr>
<tr>
<td colspan="2">{{$case->problem_description}}</td>
</tr>
</tbody>
</table>

 

This is not a confirmation of RMA. You can print the RMA form from the below link.

<a href="{{$company->domain}}/case/{{$case->case_number}}">{{$company->domain}}/case/{{$case->case_number}}</a>


Thanks,<br>
{{ $company->name }}
@endcomponent