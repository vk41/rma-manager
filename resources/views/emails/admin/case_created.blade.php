@component('mail::message', ['company' => $company])
# RMA Request recieved.

Hello

We have recieved new RMA request for the below item. We have created a case <b><a href="{{route('admin.case.show', ['case_number' => $case->case_number])}}">{{$case->case_number}}</a></b> for the item.

<table class="table w-100 bordered" cellpadding="0" cellspacing="0" >
<tbody>
<tr>
<th>Part Number</th>
<td>{{$case->part_number}}</td>
</tr>
<tr>
<th>Serial Number</th>
<td>{{$case->serial_number}}</td>
</tr>
<tr>
<th>Invoice Number</th>
<td>{{$case->invoice_number}}</td>
</tr>
<tr>
<th>Customer</th>
<td>{{$case->customer->name}}</td>
</tr>
<tr>
<th>Contact name</th>
<td>{{$case->customer->contact_name}}</td>
</tr>
<tr>
<th>Contact Number</th>
<td>{{$case->customer->contact_number}}</td>
</tr>
<tr>
<th>Contact email</th>
<td>{{$case->customer->contact_email}}</td>
</tr>
<tr>
<th>Problem</th>
<td>{{$case->problem_summary}}</td>
</tr>
<tr>
<td colspan="2"><p>{{$case->problem_description}}</p></td>
</tr>
</tbody>
</table>


Thanks,<br>
@endcomponent