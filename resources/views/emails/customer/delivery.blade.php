@component('mail::message', ['company' => $company])
# Product Delivered.

Hello, {{$customer->name}}

We have delivered your product.

<table class="table w-100 bordered" cellpadding="0" cellspacing="0" >
<tbody>
<tr>
<th>Part Number</th>
<td>{{$case->part_number}}</td>
</tr>
<tr>
<th>Serial Number</th>
<td>{{$case->serial_number}}</td>
</tr>
<tr>
<th>Invoice Number</th>
<td>{{$case->invoice_number}}</td>
</tr>
<tr>
<th>Problem</th>
<td>{{$case->problem_summary}}</td>
</tr>
<tr>
<td colspan="2">{{$case->problem_description}}</td>
</tr>
<tr>
<td colspan="2"><b>Repair Summary: </b>{{$case->repair_summary}}</td>
</tr>
</tbody>
</table>



Thanks,<br>
{{ $company->name }}
@endcomponent