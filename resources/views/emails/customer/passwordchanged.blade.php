@component('mail::message', ['company' => $company])
# Hello

Your password has been changed.

Thanks,<br>
{{ $company->name }}
@endcomponent
