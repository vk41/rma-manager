@component('mail::message', ['company' => $company])
@if($isAccountUpdate) 
# Account Updated.
@else
# Account Created.
@endif

Hello, {{$customer->name}}
{{-- {{$isAccountUpdate}} --}}
@if($isAccountUpdate) 
We have updated your login credentials for our RMA Manager. Please use below credentials to login to our service.
@else
We have created account for our RMA Manager. Please use below credentials to login to our service.
@endif

Password is case sensitive.

<table class="table w-100 bordered" cellpadding="0" cellspacing="0" >
<tbody>
<tr>
<th>Email</th>
<td>{{$customer->email}}</td>
</tr>
<tr>
<th>Password</th>
<td>{{$password}}</td>
</tr>
<tr>
<th>URL</th>
<td><a href="{{$company->domain}}">{{$company->domain}}</a></td>
</tr>
</tbody>
</table>



Thanks,<br>
{{ $company->name }}
@endcomponent