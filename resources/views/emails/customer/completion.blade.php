@component('mail::message', ['company' => $company])
# RMA completed.

Hello, {{$customer->name}}

@if($case->outcome == 'ready_to_collect')
We have completed the RMA# <b>{{$case->rma_number}}</b>. The product is ready for collection from our {{ucfirst($case->current_location)}} office.
@elseif($case->outcome == 'ready_to_ship')
We have completed the RMA# <b>{{$case->rma_number}}</b>. The product is ready to ship out from our {{ucfirst($case->current_location)}} office.
@elseif($case->outcome == 'credit_issued')
We have closed the RMA# <b>{{$case->rma_number}}</b>. We have issued credit note.
@endif
<table class="table w-100 bordered" cellpadding="0" cellspacing="0" >
<tbody>
<tr>
<th>Part Number</th>
<td>{{$case->part_number}}</td>
</tr>
<tr>
<th>Serial Number</th>
<td>{{$case->serial_number}}</td>
</tr>
<tr>
<th>Invoice Number</th>
<td>{{$case->invoice_number}}</td>
</tr>
<tr>
<th>Problem</th>
<td>{{$case->problem_summary}}</td>
</tr>
<tr>
<td colspan="2">{{$case->problem_description}}</td>
</tr>
<tr>
<td colspan="2"><b>Repair Summary: </b>{{$case->repair_summary}}</td>
</tr>
</tbody>
</table>



Thanks,<br>
{{ $company->name }}
@endcomponent