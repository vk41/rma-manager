@component('mail::message', ['company' => $company])
# RMA Rejected.

Hello, {{$customer->name}}

Your RMA request for the Case# <b>{{$case->case_number}}</b> was rejected due to <b>{{$case->outcome}}</b>. 

<table class="table w-100 bordered" cellpadding="0" cellspacing="0" >
<tbody>
<tr>
<th>Part Number</th>
<td>{{$case->part_number}}</td>
</tr>
<tr>
<th>Serial Number</th>
<td>{{$case->serial_number}}</td>
</tr>
<tr>
<th>Invoice Number</th>
<td>{{$case->invoice_number}}</td>
</tr>
<tr>
<th>Problem</th>
<td>{{$case->problem_summary}}</td>
</tr>
<tr>
<td colspan="2">{{$case->problem_description}}</td>
</tr>
</tbody>
</table>

 

This is not a confirmation of RMA. You can print the RMA form from the below link.


Thanks,<br>
{{ $company->name }}
@endcomponent