@component('mail::message', ['company' => $company])
# RMA Request recieved.

Hello, {{$customer->name}}

We have recieved your RMA request for the below item. We have created a case <b>{{$case->case_number}}</b> for the item. We will review and issue RMA number if eligible.

<table class="table w-100 bordered" cellpadding="0" cellspacing="0" >
<tbody>
<tr>
<th>Part Number</th>
<td>{{$case->part_number}}</td>
</tr>
<tr>
<th>Serial Number</th>
<td>{{$case->serial_number}}</td>
</tr>
<tr>
<th>Invoice Number</th>
<td>{{$case->invoice_number}}</td>
</tr>
<tr>
<th>Problem</th>
<td>{{$case->problem_summary}}</td>
</tr>
<tr>
<td colspan="2">{{$case->problem_description}}</td>
</tr>
</tbody>
</table>


This is not a confirmation of RMA. You dont have to print this.

Thanks,<br>
{{ $company->name }}
@endcomponent