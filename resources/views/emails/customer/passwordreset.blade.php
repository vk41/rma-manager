@component('mail::message', ['company' => $company])
# Password request

You have requested for password change. Please ignore this email if you haven't.

@component('mail::button', ['url' => route('customer.auth.password.reset', ['token' => $token])])
Reset Password
@endcomponent

Thanks,<br>
{{ $company->name }}
@endcomponent