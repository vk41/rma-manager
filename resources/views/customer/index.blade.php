@extends('layouts.customer')
@section('title', 'Home')
@section('content')
    <!-- Main content -->
    <section class="content">
        <!-- Info boxes -->
        <div class="row">
            
            <div class="col-12 col-sm-6 col-md-3">
                <div class="info-box mb-3">
                    <span class="info-box-icon bg-info elevation-1"><i class="fas fa-chart-line"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text"><a href="{{ route('customer.case.active') }}">Active RMAs</a></span>
                    <span class="info-box-number">{{$counters['active']}}</span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->
            <div class="col-12 col-sm-6 col-md-3">
                <div class="info-box">
                    <span class="info-box-icon bg-secondary elevation-1"><i class="fas fa-question"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text"><a href="{{ route('customer.case.pending') }}">Pending Cases</a></span>
                        <span class="info-box-number">{{$counters['pending']}}</span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->

            <!-- fix for small devices only -->
            <div class="clearfix hidden-md-up"></div>

            <div class="col-12 col-sm-6 col-md-3">
                <div class="info-box mb-3">
                    <span class="info-box-icon bg-success elevation-1"><i class="fas fa-check"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text"><a href="{{ route('customer.case.completed') }}">Completed RMAs</a></span>
                        <span class="info-box-number">{{$counters['completed']}}</span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->
            <div class="col-12 col-sm-6 col-md-3">
                <div class="info-box mb-3">
                    <span class="info-box-icon bg-danger elevation-1"><i class="fas fa-circle"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text"><a href="{{ route('customer.case.others') }}">Others</a></span>
                        <span class="info-box-number">{{$counters['others']}}</span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
        <div class="row">
          <div class="col-md-4">
            <!-- PRODUCT LIST -->
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Ready to Collect</h3>

                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-card-widget="collapse">
                            <i class="fas fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-tool" data-card-widget="remove">
                            <i class="fas fa-times"></i>
                        </button>
                    </div>
                </div>
                <!-- /.card-header -->
                <div class="card-body p-0">
                    <ul class="products-list product-list-in-card pl-2 pr-2">
                      @if(count($readyToCollect) > 0)
                      @foreach ($readyToCollect as $item)
                        <li class="item">
                          <div class="">
                            <a href="{{route('customer.case.show', ['case_number' => $item->case_number])}}" class="product-title">{{$item->case_number}}
                                <span class="badge badge-secondary float-right">{{Carbon\Carbon::parse($item->completed_on)->timezone(auth()->user()->timezone)->format('Y-m-d')}}</span></a>
                              <span class="product-description">
                                @if($item->rma_number)
                                  <div>RMA: {{$item->rma_number}}</div>
                                @endif
                                S/N: <b>{{$item->serial_number}}</b> PN: <b>{{$item->part_number}}</b>
                              </span>
                          </div>
                      </li>
                      @endforeach
                      
                      @else
                      <li class="item">
                        <div class="">
                            <span class="product-description text-center">
                                No Active cases.
                            </span>
                        </div>
                    </li>
                      @endif
                    </ul>
                </div>
            </div>
            <!-- /.card -->
        </div>
            <div class="col-md-8">
                <!-- TABLE: LATEST ORDERS -->
                <div class="card">
                    <div class="card-header border-transparent">
                        <h3 class="card-title">Recent Cases</h3>

                        <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                <i class="fas fa-minus"></i>
                            </button>
                            <button type="button" class="btn btn-tool" data-card-widget="remove">
                                <i class="fas fa-times"></i>
                            </button>
                        </div>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body p-0">
                        <div class="table-responsive">
                            <table class="table m-0">
                                <thead>
                                    <tr>
                                        <th>Case Number</th>
                                        <th>Serial#</th>
                                        <th>Status</th>
                                        <th>Problem</th>
                                    </tr>
                                </thead>
                                <tbody>
                                  @if(count($recentCases) > 0)
                                  @foreach ($recentCases as $case)
                                  <tr>
                                  <td><a href="{{route('customer.case.show', ['case_number' => $case->case_number])}}">{{$case->case_number}}</a></td>
                                    <td>{{$case->serial_number}}</td>
                                    <td>{!!App\Libraries\CaseLibrary::statusHtml($case->status)!!}</td>
                                    <td>{{$case->problem_summary}} - {{$case->problem_description}}</td>
                                </tr>
                                  @endforeach
                                  @else
                                    <tr><td colspan="4" class="text-center">Nothing to display.</td></tr>
                                  @endif
                                    
                                </tbody>
                            </table>
                        </div>
                        <!-- /.table-responsive -->
                    </div>
                    <!-- /.card-body -->
                    <div class="card-footer clearfix">
                        <a href="{{ route('customer.case.create') }}" class="btn btn-sm btn-info float-left">Create New Case</a>
                        <a href="{{ route('customer.case.active') }}" class="btn btn-sm btn-secondary float-right">View Active Cases</a>
                    </div>
                    <!-- /.card-footer -->
                </div>
            </div>
            
        </div>
    </section>
@endsection
