@extends('customer.auth.layout')
@section('content')
<div class="card">
  
  {{-- <div class="card-body login-card-body">
    
  </div> --}}

  <div class="card-body login-card-body">
    
    <p class="login-box-msg">Reset your password.</p>
    @if (\Session::has('success'))
      <p >An email has been sent to the supplied email address. </p>
      <p >If you do not receive the password reset mail within few moments, please check your spam folder. </p>
    @else
    {{-- @endif --}}
    @if(count( $errors ) > 0)
    <div class="text-center py-2">
      @foreach ($errors->all() as $error)
      <div class="text-danger">{{ $error }}</div>
      @endforeach
  </div>
  @endif

    <form action="{{ route('customer.auth.password.email') }}" method="post">
        @csrf
      <div class="input-group mb-3">
        <input type="email" class="form-control" placeholder="Email" name="email" value="{{ old('email') }}" required>
        <div class="input-group-append">
          <div class="input-group-text">
            <span class="fas fa-envelope"></span>
          </div>
        </div>
      </div>
      <div class="row">
        <!-- /.col -->
        <div class="col-8">
            
          </div>
        <div class="col-4">
          <button type="submit" class="btn btn-primary btn-block">Reset</button>
        </div>
        <!-- /.col -->
      </div>
    </form>



    <p class="mb-1">
      <a href="{{ route('customer.auth.login') }}">Login Here</a>
    </p>
    @endif
  </div>
  <!-- /.login-card-body -->
</div>
@endsection