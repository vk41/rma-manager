@extends('layouts.customer')
@section('title', 'Profile')
@section('content')
    <!-- Main content -->
    <section class="content">
        <form method="POST" action="{{ route('customer.user.update') }}">
        @csrf
        <div class="row">
            <div class="col-6">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Contact</h3>
                    </div>
                    <div class="card-body">
                        <div class="form-group">
                            <label for="contact_name">Contact Name</label>
                            <input type="text" id="contact_name" name="contact_name" class="form-control"
                                value="{{ old('contact_name', auth()->user()->contact_name) }}" required>
                        </div>
                        <div class="form-group">
                            <label for="contact_number">Contact Number</label>
                            <input type="text" id="contact_number" name="contact_number" class="form-control"
                                value="{{ old('contact_number', auth()->user()->contact_number) }}" required>
                        </div>
                        <div class="form-group">
                            <label for="contact_email">Contact Email</label>
                            <input type="text" id="contact_email" name="contact_email" class="form-control"
                                value="{{ old('contact_email', auth()->user()->contact_email) }}" required>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-6">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Password</h3>
                    </div>
                    <div class="card-body">
                        <small  class="form-text">Fill only if you want to change password.</small>
                        <div class="form-group">
                            <label for="password">Password</label>
                            <input type="text" id="password" name="password" class="form-control" >
                            <small  class="form-text">Minimum 8 characters</small>
                        </div>
                        <div class="form-group">
                            <label for="password_confirmation">Confirm Password</label>
                            <input type="text" id="password_confirmation" name="password_confirmation" class="form-control">
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
              <a href="{{ url('/') }}" class="btn btn-secondary">Cancel</a>
              <input type="submit" value="Save" class="btn btn-success float-right">
            </div>
          </div>
        </form>
    </section>
@endsection
