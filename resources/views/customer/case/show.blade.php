@extends('layouts.customer')
@section('title', 'Case Detail')
@section('content')
   

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-3">

                    <!-- Profile Image -->
                    <div class="card card-{{ App\Libraries\CaseLibrary::statusHtml($case->status, true) }} card-outline">
                        <div class="ribbon-wrapper ribbon-lg" title="{{$case->status}}">
                            <div class="ribbon bg-{{App\Libraries\CaseLibrary::statusHtml($case->status, true)}}">
                                {{$case->status}}
                            </div>
                          </div>
                        <div class="card-body box-profile">

                            @if ($case->rma_number)
                            <h3 class="profile-username text-center">RMA#: {{ $case->rma_number }}</h3>
                            <p class="text-muted text-center">Case#: {{ $case->case_number }}</p>
                            @else
                            <h3 class="profile-username text-center">Case#: {{ $case->case_number }}</h3>
                            @endif
                            <p class="text-muted text-center">                                
                                @if ($case->outcome)
                                    <div class="text-center"><b>Outcome:</b> {{ ucfirst($case->outcome) }}</div>
                                @endif
                            </p>

                            <ul class="list-group list-group-unbordered mb-3">
                                <li class="list-group-item">
                                    <b>Part Number</b> <span class="float-right">{{ $case->part_number }}</span>
                                </li>
                                <li class="list-group-item">
                                    <b>Serial Number</b> <span class="float-right">{{ $case->serial_number }}</span>
                                </li>
                                <li class="list-group-item">
                                    <b>Brand</b> <span class="float-right">{{ $case->manufacturer }}</span>
                                </li>
                            </ul>

                            {{-- <a href="#"
                                class="btn btn-primary btn-block"><b>Follow</b></a> --}}
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->

                    <!-- About Me Box -->
                    <div class="card card-{{ App\Libraries\CaseLibrary::statusHtml($case->status, true) }}">
                        <div class="card-header">
                            <h3 class="card-title">Detail</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <strong><i class="fas fa-book mr-1"></i> Problem</strong>

                            <p class="text-muted">
                                {{ $case->problem_summary }}
                                @if ($case->problem_description) -
                                    {{ $case->problem_description }} @endif
                            </p>

                            <hr>

                            <strong><i class="fas fa-map-marker-alt mr-1"></i> Invoice</strong>
                            <p class="text-muted">
                                {{ $case->invoice_number }}
                                @if ($case->invoice_date) - Date: {{ $case->invoice_date }}
                                @endif
                            </p>

                            <hr>
                            <strong><i class="fas fa-pencil-alt mr-1"></i>Original Accessories
                                @if ($case->checkLists->firstWhere('name', 'accessories'))
                                    @if (count($case->checkLists->firstWhere('name', 'accessories')->value) > 0)
                                        <div class="float-right">
                                            @foreach ($case->checkLists->firstWhere('name', 'accessories')->value as $item)
                                                <span class="badge badge-primary">{{ strtoupper($item) }}</span>
                                            @endforeach
                                        </div>
                                    @else
                                        @if (!$case->checkLists->firstWhere('name', 'accessories')->remarks)
                                            <div class="float-right">
                                                <span class="badge badge-danger">No Accessories</span>
                                            </div>
                                        @endif
                                    @endif
                                @endif


                            </strong>

                            <p class="text-muted">
                                @if ($case->checkLists->firstWhere('name', 'accessories'))
                                    <div>
                                        {{ $case->checkLists->firstWhere('name', 'accessories')->remarks }}
                                    </div>
                                @endif
                            </p>


                            <hr>

                            <strong><i class="far fa-file-alt mr-1"></i> Condition</strong>

                            <p class="text-muted">

                                @if ($case->checkLists->firstWhere('name', 'password_protected'))
                                    <div>Password Protected? <span
                                            class="float-right">{{ strtoupper($case->checkLists->firstWhere('name', 'password_protected')->value[0]) }}</span>
                                    </div>
                                @endif
                                @if ($case->checkLists->firstWhere('name', 'backup_done'))
                                    <div>Data Backup is done? <span
                                            class="float-right">{{ strtoupper($case->checkLists->firstWhere('name', 'backup_done')->value[0]) }}</span>
                                    </div>
                                @endif
                            </p>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
                <div class="col-md-9">
                    <div class="card">
                        <div class="card-header p-2">
                            <ul class="nav nav-pills">
                                {{-- <li class="nav-item"><a class="nav-link active"
                                        href="#activity" data-toggle="tab">Activity</a></li>
                                --}}
                                <li class="nav-item"><a class="nav-link active" href="#timeline"
                                        data-toggle="tab">Timeline</a></li>
                                <li class="nav-item"><a class="nav-link" href="#diagnosis" data-toggle="tab">Diagnosis</a>
                                <li class="nav-item"><a class="nav-link" href="#product" data-toggle="tab">Product</a>
                                @if(!in_array($case->status, ['pending', 'rejected']))
                                <li class="nav-item">
                                    <a href="{{ route('customer.case.print', ['case_number' => $case->case_number]) }}"
                                        target="_blank" class="btn btn-default"><i class="nav-icon fas fa-print"></i>
                                        Print</a>                                    
                                </li>
                                @endif
                            </ul>
                        </div><!-- /.card-header -->
                        <div class="card-body">
                            <div class="tab-content">
                                <!-- /.tab-pane -->
                                <div class="tab-pane active" id="timeline">
                                    <!-- The timeline -->
                                    <div class="timeline timeline-inverse">
                                        <!-- timeline time label -->
                                        <div class="time-label">
                                            <span class="bg-danger">
                                                {{ Carbon\Carbon::now()->format('j M, Y') }}
                                            </span>
                                        </div>
                                        <!-- /.timeline-label -->
                                        @foreach ($case->events->groupBy('date') as $date => $events)


                                            @foreach ($events as $event)
                                                <!-- timeline item -->
                                                <div>
                                                    <i class="fas fa-calendar-check bg-primary"></i>

                                                    <div class="timeline-item">
                                                        <span class="time"><i class="far fa-clock"></i>
                                                            {{ $event->created_at->format('g:i a') }}</span>

                                                        <h3 class="timeline-header">
                                                            <a href="#">
                                                                @if ($event->created_by == 0)
                                                                    {{ $case->customer->name }}
                                                                @else
                                                                    Support Team
                                                                @endif
                                                            </a>
                                                            {{ App\Libraries\Helper::unslug($event->name) }}
                                                        </h3>
                                                        @if ($event->remarks)
                                                            <div class="timeline-body">{{ $event->remarks }}</div>
                                                        @endif
                                                    </div>
                                                </div>
                                                <!-- END timeline item -->
                                            @endforeach
                                            <div class="time-label">
                                                <span class="bg-secondary">
                                                    {{ $date }}
                                                </span>
                                            </div>

                                        @endforeach


                                        <div>
                                            <i class="far fa-clock bg-gray"></i>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.tab-pane -->

                                <div class="tab-pane" id="diagnosis">
                                    {{-- <div class="text-right mb-3">
                                        
                                    </div> --}}
                                    <div class="card">
                                        <div class="card-header">
                                            Checklist
                                        </div>
                                        <div class="card-body">
                                            <ul class="list-group list-group-unbordered mb-3">
                                                @foreach ($case->checklists as $checklist)
                                                    <li class="list-group-item">
                                                        <b>{{ App\Libraries\CaseLibrary::getChecklistFriendlyName($checklist->name) }}</b>
                                                        @if ($checklist->remarks)
                                                            <small>{{ $checklist->remarks }}</small>
                                                        @endif
                                                        <span class="float-right">
                                                            @if (count($checklist->value) <= 0)
                                                                <span class="badge badge-warning">NA</span>
                                                            @endif
                                                            @foreach ($checklist->value as $item)
                                                                <span
                                                                    class="badge badge-secondary">{{ strtoupper(App\Libraries\Helper::unslug($item)) }}</span>
                                                            @endforeach

                                                        </span>
                                                    </li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    </div>

                                    <div class="card">
                                        <div class="card-header">
                                            Repair Summary
                                        </div>
                                        <div class="card-body">
                                            {{ $case->repair_summary }}
                                        </div>
                                    </div>
                                </div>
                                <!-- /.tab-pane -->
                                <div class="tab-pane" id="product">

                                    <div class="card">
                                        <div class="card-header">
                                            Product
                                        </div>
                                        <div class="card-body">
                                            <!-- Table row -->
                                            <div class="row">
                                                <div class="col-12 table-responsive">
                                                    <table class="table table-bordered table-sm">
                                                        <tbody>
                                                            <tr>
                                                                <th width="30%">Part Number</th>
                                                                <td>{{ $case->part_number }}</td>
                                                                <th width="30%">Serial Number</th>
                                                                <td>{{ $case->serial_number }}</td>
                                                            </tr>
                                                            <tr>
                                                                <th>Invoice Number</th>
                                                                <td>{{ $case->invoice_number }}</td>
                                                                <th>Invoice Date</th>
                                                                <td>{{ $case->invoice_date }}</td>
                                                            </tr>
                                                            <tr>
                                                                <th>Manufacturer</th>
                                                                <td>{{ $case->manufacturer }}</td>
                                                                <th>Category</th>
                                                                <td>{{ $case->category }}</td>
                                                            </tr>
                                                            <tr>
                                                                <th>Spec</th>
                                                                <td colspan="3">{{ $case->product_description }}</td>
                                                               
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <!-- /.col -->
                                            </div>
                                            <!-- /.row -->
                                        </div>
                                    </div>
                                </div>
                                <!-- /.tab-pane -->
                            </div>
                            <!-- /.tab-content -->
                        </div><!-- /.card-body -->
                    </div>
                    <!-- /.nav-tabs-custom -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </section>
@endsection
@push('styles')
    <link rel="stylesheet" href="{{ asset('plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}">
@endpush
@push('scripts')

@endpush
