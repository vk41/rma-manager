@extends('layouts.customer')
@section('title', 'New Case')
@section('content')
    <!-- Main content -->
    <section class="content mb-2">
      <form method="POST" action="{{ route('customer.case.store') }}">
        @csrf
      <div class="row">
        <div class="col-md-6">
          <div class="card card-primary" id="app">
            <div class="card-header">
              <h3 class="card-title">Product</h3>
            </div>
            <div class="card-body">
              <div class="form-group">
                <label for="serial_number">Serial Number</label>
                {{-- <input type="text" id="serial_number" name="serial_number" class="form-control"  value="{{ old('serial_number') }}"> --}}
                <div class="input-group">
                  <input type="text" autocomplete="off" class="form-control" id="serial_number" name="serial_number" value="{{ old('serial_number') }}">
                  <div class="input-group-append">
                    <button id="autofill_button" class="btn btn-outline-primary" type="button">Auto Fill</button>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <label for="part_number">Part Number</label>
                <input type="text" id="part_number" name="part_number" class="form-control" value="{{ old('part_number') }}" required>
              </div>
              
              <div class="form-group">
                <label for="invoice_number">Invoice Number</label>
                <input type="text" id="invoice_number" name="invoice_number" class="form-control" value="{{ old('invoice_number') }}" required>
              </div>
              <div class="form-group">
                <label for="inputStatus">Problem</label>
                <select class="form-control" name="problem_summary" required>
                  <option selected disabled>Select one</option>
                  <option @if( old('problem_summary') == 'DOA' ) selected @endif >DOA</option>
                  <option @if( old('problem_summary') == 'Display Problem' ) selected @endif>Display Problem</option>
                  <option @if( old('problem_summary') == 'System Slow' ) selected @endif>System Slow</option>
                  <option @if( old('problem_summary') == 'Others' ) selected @endif>Others</option>
                </select>
              </div>
              <div class="form-group">
                <label for="inputDescription">Problem Description</label>
                <textarea id="inputDescription" name="problem_description" class="form-control"  rows="2">{{ old('problem_description') }}</textarea>
              </div>
              
            </div>
            <!-- /.card-body -->
            <div class="overlay d-none">
              <i class="fas fa-2x fa-sync-alt fa-spin"></i>
          </div>
          </div>
          <!-- /.card -->
        </div>
        <div class="col-md-6">
          <div class="card card-secondary">
            <div class="card-header">
              <h3 class="card-title">Condition</h3>
            </div>
            <div class="card-body">
              <div class="form-group">
                <legend class="col-form-label  pt-0"><b>Password Protected?</b></legend>
                <div class="icheck-primary icheck-inline">
                  <input type="radio" id="password-protected-yes" value="yes" name="password_protected[value][]"  @if( old('password_protected.value.0') == 'yes' ) checked @endif required/>
                  <label for="password-protected-yes">Yes</label>
                </div>
                <div class="icheck-primary icheck-inline">
                    <input type="radio" id="password-protected-no" value="no" name="password_protected[value][]"  @if( old('password_protected.value.0') == 'no' ) checked @endif required/>
                    <label for="password-protected-no">No</label>
                </div>
                <small id="passwordHelpBlock" class="form-text text-danger">
                  Please provide password while droping the product, delays will occur otherwise.
                </small>
              </div>
              <div class="form-group">
                <legend class="col-form-label  pt-0"><b>Data Backup is done?</b></legend>
                <div class="icheck-primary icheck-inline">
                  <input type="radio" id="backup-done-yes" value="yes" name="backup_done[value][]" @if( old('backup_done.value.0') == 'yes' ) checked @endif required/>
                  <label for="backup-done-yes">Yes</label>
                </div>
                <div class="icheck-primary icheck-inline">
                    <input type="radio" id="backup-done-no" value="no" name="backup_done[value][]" @if( old('backup_done.value.0') == 'no' ) checked @endif required/>
                    <label for="backup-done-no">No</label>
                </div>
                <div class="icheck-primary icheck-inline">
                    <input type="radio" id="backup-done-na" value="na" name="backup_done[value][]" @if( old('backup_done.value.0') == 'na' ) checked @endif required/>
                    <label for="backup-done-na">NA</label>
                </div>
                <small id="passwordHelpBlock" class="form-text text-danger">
                  Data is not covered under warranty.
                </small>
              </div>
              <div class="form-group">
                <legend class="col-form-label  pt-0"><b>Accessories</b></legend>
                <div class="icheck-primary">
                  <input type="checkbox" id="acc-box" value="box" name="accessories[value][]"  @if( in_array('box', old('accessories.value', [])) ) checked @endif />
                  <label for="acc-box">Original Box</label>
                </div>
                <div class="icheck-primary">
                  <input type="checkbox" id="acc-adaptor" value="adaptor" name="accessories[value][]"  @if( in_array('adaptor', old('accessories.value', []))  ) checked @endif />
                  <label for="acc-adaptor">Original Power supply</label>
                </div>
                <div class="">
                  <label for="acc-adaptor">Other Accessories</label>
                  <input type="text" id="inputName" name="accessories[remarks]" class="form-control"  value="{{ old('accessories.remarks') }}" >
                </div>
                <small id="passwordHelpBlock" class="form-text text-danger">
                  Original accessories are mandatory. 
                </small>
              </div>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
      </div>
      <div class="row">
        <div class="col-12">
          <a href="{{ url('/') }}" class="btn btn-secondary">Cancel</a>
          <input type="submit" value="Create Case" class="btn btn-success float-right">
        </div>
      </div>
    </form>
    </section>
  @endsection
  @push('styles')
  <link rel="stylesheet" href="{{ asset('plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}">
  @endpush
  @push('scripts')
      <script>
        $(function() {
          $('#autofill_button').on('click', function(event) {                
                event.preventDefault();
                if($('#serial_number').val() == '') {
                $('#serial_number').addClass('is-invalid');
                  toastr.error('Serial number cannot be empty.');
                  return ;
                }else{
                  $('#serial_number').addClass('is-valid');
                }
                handleLoader();
                axios.post('{{route("customer.case.autofill")}}',  {
                    serial_number: $('#serial_number').val()
                })
                .then(function(response) {
                    handleAutoFill(response.data.data);
                    handleLoader(false);
                    toastr.success('Data filled automatically.');
                })
                .catch(function(err) {
                    handleLoader(false);
                    console.log(err, err.response);
                    toastr.error('Autofill failed.');
                });
            });
        });
        function handleAutoFill(data) {
                var fields = Object.keys(data);
                Object.keys(data).forEach(function(fieldName) {
                    var $element = $('#app').find('#'+fieldName);
                    var newValue = data[fieldName];
                    // console.log(newValue);
                    var originalValue = $element.val();
                    // console.log('originalValue', originalValue);
                    $element.val(newValue);
                    $element.closest('.form-group').addClass('value-changed');
                    console.log($element);
                });
            }
        function handleLoader(show = true) {
            if(show) {
                $('#app').find('.overlay').removeClass('d-none');
            }else {
                $('#app').find('.overlay').addClass('d-none');
            }
        }
      </script>
  @endpush