@extends('layouts.customer')
@section('title', 'Other Cases')
@section('content')
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-12">
                <div class="card">

                    <!-- /.card-header -->
                    <div class="card-body">
                        <table id="cases-table" class="table table-bordered table-hover">
                            <thead>

                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection


@push('styles')
    <link href="{{ asset('plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}" rel="stylesheet">
    <link href="{{ asset('plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}" rel="stylesheet">
@endpush

@push('scripts')
    <script src="{{ asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>

    <script>
        $(document).ready(function() {
            //     $("#example1").DataTable({
            //   "responsive": true,
            //   "autoWidth": false,
            // });
            var url = "{{ route('customer.case.data', ['status' => 'others']) }}";
            var packagesTable = $('#cases-table').DataTable({
                    processing: true,
                    serverSide: true,
                    autoWidth: false,
                    responsive: true,
                    pageLength: 20,
                    order: [
                        [1, 'desc']
                    ],
                    ajax: url,
                    lengthMenu: [
                        [10, 20, 50, -1],
                        [10, 20, 50, "All"]
                    ],
                    columns: [{
                            data: 'case_number',
                            width: '150px',
                            title: 'Case Number',
                            render: function(data, type, row) {
                                return `<a class="case-link" data-case="${row.case_number}" href="#">${data}</a>`
                            }
                        },
                        {
                            data: 'part_number',
                            width: '150px',
                            title: 'Part Number'
                        },
                        {
                            data: 'serial_number',
                            width: '150px',
                            title: 'Serial Number'
                        },
                        {
                            data: 'invoice_number',
                            width: '150px',
                            title: 'Invoice Number'
                        },
                        {
                            data: 'invoice_date',
                            width: '150px',
                            title: 'Invoice Date'
                        },
                        {
                            data: 'outcome',
                            width: '150px',
                            title: 'Outcome'
                        },
                        
                        {
                            data: 'status',
                            width: '150px',
                            title: 'Status',
                            render: function(data, type, row) {
                                return statusHtml(data);
                            }
                        },
                        {
                            data: 'case_created_on',
                            width: '150px',
                            title: 'Case Created On'
                        },
                    ],
                    drawCallback: function(settings) {
                        $(settings.nTable).find('.case-link').off().on('click', function(event) {
                          event.preventDefault();
                          var caseUrl = "{{route('customer.case.show', ['case_number' => ':caseno'])}}";
                          var caseNumber = $(event.currentTarget).data('case');
                          var targetUrl = caseUrl.replace(':caseno', caseNumber);
                          window.open(targetUrl);
                        });
                    }
                })
                .on('processing.dt', function(e, settings, processing) {
                    if (processing) {
                        $('#table-overlay').removeClass("d-none");
                    } else {
                        $('#table-overlay').addClass("d-none", true);
                    }
                });
        });

    </script>

@endpush
