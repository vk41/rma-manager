<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>@yield('title')</title>

        <!-- Fonts -->
        <link rel="dns-prefetch" href="//fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }
            .flex-column {
                flex-direction: column;
            }

            .code {
                border-right: 2px solid;
                font-size: 26px;
                padding: 0 15px 0 15px;
                text-align: center;
            }

            .message {
                font-size: 18px;
                text-align: center;
            }
            .back-button {
                color: #17a2b8;
                border-radius: .25rem;
                transition: color .15s ease-in-out,background-color .15s ease-in-out,border-color .15s ease-in-out,box-shadow .15s ease-in-out;
                border: 1px solid #17a2b8;
                padding: .375rem .75rem;
            }
            .back-button:hover {
                color: #fff;
                background-color: #17a2b8;
                border-color: #17a2b8;
            }
            
        </style>
    </head>
    <body>
        <div class="flex-center flex-column position-ref full-height">
            <div class="flex-center">
                <div class="code">
                    @yield('code')
                </div>
    
                <div class="message" style="padding: 10px;">
                    @yield('message')
                </div>
            </div>
            @if(!Route::is('open.*'))
            <div class="back-button-section" style="padding: 10px;">
                <a href="{{ url()->previous() }}" class="back-button">Go Back</a>
                @if(App\Libraries\Site::isAdminPage()) 
                <a href="{{ url('/admin') }}" class="back-button">Go Home</a>
                @else
                <a href="{{ url('/') }}" class="back-button">Go Home</a>
                @endif
            </div>
            @endif
        </div>
    </body>
</html>
