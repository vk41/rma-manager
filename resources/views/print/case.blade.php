@extends('layouts.print')
@section('title', 'RMA Request form')

@section('content')

    <!-- Table row -->
    @if ($case->rma_number)
        <h3 class="text-center">RMA Form</h3>
    @else
        <h3 class="text-center">RMA Request Form</h3>
    @endif
    <div class="row">
        <div class="col-12 table-responsive">
            @if ($case->rma_number)
                <table class="table table-striped">
                    <tbody>
                        <tr>
                            <th width="20%">RMA Number</th>
                            <td width="40%"><b>{{ $case->rma_number }}</b></td>
                            <td width="40%">{!! \App\Libraries\Barcode::getBarcode($case->rma_number) !!}</td>
                        </tr>
                    </tbody>
                </table>
            @else
                <h3 class="text-center mb-3">This is not a RMA. Your case# {{ $case->case_number }} is under review. Print
                    this once the RMA is issued.</h3>
            @endif
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->

    <!-- Table row -->
    <div class="row">
        <div class="col-6 table-responsive">
            <table class="table table-striped table-bordered table-sm">
                <tbody>
                    <tr>
                        <th width="30%">Comapny</th>
                        <td>{{ $customer->name }}</td>
                    </tr>
                    <tr>
                        <th>Contact name</th>
                        <td>{{ $customer->contact_name }}</td>
                    </tr>
                    <tr>
                        <th>Contact Number</th>
                        <td>{{ $customer->contact_number }}</td>
                    </tr>
                    <tr>
                        <th>Contact Email</th>
                        <td>{{ $customer->contact_email }}</td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="col-6 table-responsive">
            <table class="table table-striped table-bordered table-sm">
                <tbody>
                    <tr>
                        <th width="30%">Part Number</th>
                        <td>{{ $case->part_number }}</td>
                    </tr>
                    <tr>
                        <th>Serial Number</th>
                        <td>{{ $case->serial_number }}</td>
                    </tr>
                    <tr>
                        <th>Invoice</th>
                        <td>{{ $case->invoice_number }}
                            @if ($case->invoice_date) / Date: {{ $case->invoice_date }}
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <th>Manufacturer</th>
                        <td>{{ $case->manufacturer }}</td>
                    </tr>
                </tbody>
            </table>
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->

    <div class="row">
        {{-- <div class="col-12 table-responsive">
            <table class="table table-striped table-bordered">
                <tbody>
                    <tr>
                        <th width="30%"></th>
                        <td></td>
                    </tr>
                </tbody>
            </table>
        </div> --}}
        <div class="col-12">
            <div class="border mb-3 p-2"><b>Problem Description: </b>{{ $case->problem_summary }} -
                {{ $case->problem_description }}</div>
        </div>
    </div>

    <!-- Table row -->
    <div class="row">
        <div class="col-12 table-responsive">
            <table class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th width="35%">Specification</th>
                        <th width="25%">Status</th>
                        <th>Remarks</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($case->checklists as $checklist)
                        <tr>
                            <td><b>{{ App\Libraries\CaseLibrary::getChecklistFriendlyName($checklist->name) }}</b></td>
                            <td>
                                @if (count($checklist->value) <= 0)
                                    {{-- <span><i
                                            class="nav-icon fas fa-ban"></i>&nbsp;NA</span> --}}
                                @endif
                                @foreach ($checklist->value as $item)
                                    <span class="print-checkobx"> <i
                                            class="nav-icon fas fa-check"></i>&nbsp;{{ strtoupper($item) }}</span>
                                @endforeach
                            </td>
                            <td>{{ $checklist->remarks }}</td>
                        </tr>

                    @endforeach


                </tbody>
            </table>
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
    @if ($case->repair_summary)
        <div class="row">

            <div class="col-12">
                <div class="border mb-3 p-2"><b>Repair Summary: </b>{{ $case->repair_summary }} -
                    {{ $case->repair_summary }}</div>
            </div>
        </div>
    @endif
    <div class="row">
        <div class="col-12">
            <div class="mb-3 p-2">
                By printing this, you agree to all our terms and conditions mentioned on our website {{ $company->email }}
                <small class="float-right">Print Date: {{ date('Y-m-d') }}</small>
            </div>
        </div>
    </div>
    @if($case->rma_number)
    <!-- Table row -->
    <div class="row">
        <div class="col-12 table-responsive">
            <table class="table table-striped table-bordered">
                <tbody>
                    <tr>
                        <th width="30%">Customer Signature:</th>
                        <td width="50%"></td>
                        <td>Date:</td>
                    </tr>
                    <tr>
                        <th width="30%">Compusouk Rep Received: </th>
                        <td width="50%"></td>
                        <td>Date:</td>
                    </tr>
                    <tr>
                        <th width="30%">Device Returned on:</th>
                        <td width="50%"></td>
                        <td>Date:</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    @endif

@endsection
