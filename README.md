
# RMA Manager

## Features
- Customer can create RMA based on the serial number of the product.
- Customer can view status of the the RMA.
- Management can use /admin to login.
- Customer get notification about accept/complete/pickup product.

## Installation
- Clone env file.
- Setup database.
- Run below commands.

````
php artisan key:generate

php artisan migrate --seed

php artisan serve
````

