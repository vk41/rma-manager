<?php 
return [
    'visibility'=>[
        'events' => ['created', 'accepted', 'completed', 'work_in_progress',],
        'checklists'=> ['*']
    ],
];